package unittest;

import AST.Table;
import AbstractSyntaxTree.AbstractSyntaxTree;
import Parser.MMRLexer;
import Parser.MMRParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Stack;

import static org.junit.Assert.fail;

@RunWith(Parameterized.class)
public class SemanticCheckTest {
	private String filename;
	private boolean shouldPass;

	public SemanticCheckTest(String filename, boolean shouldPass) {
		this.filename = filename;
		this.shouldPass = shouldPass;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> data() {
		Collection<Object[]> parameters = new ArrayList<>();
		for (File file : new File("testfile/passed/").listFiles()) {
			if (file.isFile() && file.getName().endsWith(".mx")) {
				parameters.add(new Object[]{
						"testfile/passed/" + file.getName(),
						true
				});
			}
		}
		for (File file : new File("testfile/compile_error/").listFiles()) {
			if (file.isFile() && file.getName().endsWith(".mx")) {
				parameters.add(new Object[]{
						"testfile/compile_error/" + file.getName(),
						false
				});
			}
		}
		return parameters;
	}

	@Test
	public void testPass() throws IOException {
		System.out.println(filename);

		try {
			InputStream is = new FileInputStream(filename);

			ANTLRInputStream input = new ANTLRInputStream(is);

			MMRLexer lexer = new MMRLexer(input);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			MMRParser parser = new MMRParser(tokens);
			ParseTree tree = parser.program();

			ParseTreeWalker walker = new ParseTreeWalker();
			AbstractSyntaxTree ASTBuilder = new AbstractSyntaxTree(parser);
			walker.walk(ASTBuilder, tree);

			//AbstractSyntaxTree.tree.get(tree).print(-1);
			boolean ret = true;
			Table table = new Table();
			ret = ret & AbstractSyntaxTree.tree.get(tree).firstTraverse(table, 0);
			ret = ret & AbstractSyntaxTree.tree.get(tree).secondTraverse(table, 0);
			ret = ret & AbstractSyntaxTree.tree.get(tree).thirdTraverse(table, 0, new Stack<>(), null);
			//System.out.print("Accepted\n");

			if (shouldPass != ret) {
				fail("Failed!");
			}
		} catch (Exception e) {
			if (shouldPass) throw e;
		}
	}
}