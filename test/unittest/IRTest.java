package unittest;

import IR.*;
import AST.Program;
import AST.Table;
import AbstractSyntaxTree.AbstractSyntaxTree;
import Parser.MMRLexer;
import Parser.MMRParser;
import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import interpreter.LLIRInterpreter;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Stack;

import static jdk.nashorn.internal.parser.TokenKind.IR;

/**
 * Created by zhouyuhao on 2016/5/2.
 */
@RunWith(Parameterized.class)
public class IRTest {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Collection<Object[]> params = new ArrayList<>();
        for (File f : new File("testfile/ir/").listFiles()) {
            if (f.isFile() && f.getName().endsWith(".mx")) {
                params.add(new Object[] { "testfile/ir/" + f.getName() });
            }
        }
        return params;
    }
    private String filename;
    public IRTest(String filename) {
        this.filename = filename;
    }
    @Test
    public void testPass() throws IOException, InterruptedException {
        System.out.println(filename);

        ByteArrayOutputStream irTextOut = new ByteArrayOutputStream();
        OutputStream tee = irTextOut;
        PrintStream out = new PrintStream(tee);

        InputStream is = new FileInputStream(filename);
        ANTLRInputStream input = new ANTLRInputStream(is);
        MMRLexer lexer = new MMRLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MMRParser parser = new MMRParser(tokens);
        ParseTree tree = parser.program(); // parse; start at prog <label id="code.tour.main.6"/>

        ParseTreeWalker walker = new ParseTreeWalker();
        AbstractSyntaxTree astBuilder = new AbstractSyntaxTree(parser);
        walker.walk(astBuilder, tree);

        boolean ret = true;
        Table table = new Table();
        ret = ret & AbstractSyntaxTree.tree.get(tree).firstTraverse(table, 0);
        ret = ret & AbstractSyntaxTree.tree.get(tree).secondTraverse(table, 0);
        ret = ret & AbstractSyntaxTree.tree.get(tree).thirdTraverse(table, 0, new Stack<>(), null);

        //AbstractSyntaxTree.tree.get(tree).print(0);

        IR ir = ((Program) AbstractSyntaxTree.tree.get(tree)).IRtranslate(null, table, 0, new Stack<>());
        System.out.println(ir.toString());
        out.println(ir.toString());


        out.flush();
        irTextOut.close();

        // Run the virtual machine
        byte[] irText = irTextOut.toByteArray();
        ByteInputStream vmIn = new ByteInputStream(irText, irText.length);
        LLIRInterpreter vm = new LLIRInterpreter(vmIn, false);
        vm.run();

        // Assert result
        BufferedReader br = new BufferedReader(new FileReader(filename));
        String line;
        do {
            line = br.readLine();
        } while (!line.startsWith("/*! assert:"));
        String assertion = line.replace("/*! assert:", "").trim();
        if (assertion.equals("exitcode")) {
            int expected = Integer.valueOf(br.readLine().trim());
            if (vm.getExitcode() != expected)
                throw new RuntimeException("exitcode = " + vm.getExitcode() + ", expected: " + expected);
        } else if (assertion.equals("exception")) {
            if (!vm.exitException())
                throw new RuntimeException("exit successfully, expected an exception.");
        } else {
            throw new RuntimeException("unknown assertion.");
        }
        br.close();
    }
}

