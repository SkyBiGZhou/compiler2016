package interpreter;

import AST.Program;
import AST.Table;
import AbstractSyntaxTree.AbstractSyntaxTree;
import Parser.MMRLexer;
import Parser.MMRParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.*;
import java.util.Stack;

import IR.*;

/**
 * Created by zhouyuhao on 2016/5/7.
 */
public class MIPSTest {
    public static void main(String[] args) throws Exception {
        String inputFile = "C:/Users/zhouyuhao/Desktop/课程/compiler/Compiler/testfile/test.mx";
        InputStream is = new FileInputStream(inputFile);
        OutputStream os = new FileOutputStream("C:/Users/zhouyuhao/Desktop/课程/compiler/Compiler/testfile/test.s");
        ANTLRInputStream input = new ANTLRInputStream(is);

        MMRLexer lexer = new MMRLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MMRParser parser = new MMRParser(tokens);
        ParseTree tree = parser.program(); // parse;

        ParseTreeWalker walker = new ParseTreeWalker();
        AbstractSyntaxTree astBuilder = new AbstractSyntaxTree(parser);
        walker.walk(astBuilder, tree);

        boolean ret = true;
        Table table = new Table();
        ret = ret & (AbstractSyntaxTree.tree.get(tree).firstTraverse(table, 0));
        ret = ret & (AbstractSyntaxTree.tree.get(tree).secondTraverse(table, 0));
        ret = ret & (AbstractSyntaxTree.tree.get(tree).thirdTraverse(table, 0, new Stack<>(), null));

        IR ir = ((Program) AbstractSyntaxTree.tree.get(tree)).IRtranslate(null, table, 0, new Stack<>());

        String content = ir.MIPSForCISC();
        File file = new File("C:/Users/zhouyuhao/Desktop/课程/compiler/Compiler/testfile/test.s");
        if (!file.exists()) {
            file.createNewFile();
        }
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(content);
        bw.close();

    }

}
