grammar MMR;

program
    :   declaration* EOF
    ;

declaration
    :   class_declaration
    |   function_declaration
    |   global_var_declaration
    ;

class_declaration
    :   CLASS IDENTIFIER '{' var_declaration_statement* '}'
    ;

function_declaration
    :   type_name IDENTIFIER '(' (var_declaration (',' var_declaration)*)? ')' compound_statement
    |   VOID IDENTIFIER '(' (var_declaration (',' var_declaration)*)? ')' compound_statement
    ;

type_name
    :   type_name ('[' expression? ']')
    |   INT
    |   BOOL
    |   STRING
    |   IDENTIFIER
    ;

/*
func_parameter_decl
    :   var_declaration
    |   func_parameter_decl ',' var_declaration
    ;
*/
global_var_declaration
    :   var_declaration ';'
    ;

statement
    :   compound_statement
    |   if_statement
    |   var_declaration_statement
    |   expr_statement
    |   loop_statement
    |   jump_statement
    ;

compound_statement
    :   '{' statement* '}'
    ;

var_declaration_statement
    :   var_declaration ';'
    ;

var_declaration
    :   type_name IDENTIFIER ('=' expression)?
    ;

/*
identifier_list
    :   IDENTIFIER ('=' expression)?
    |   IDENTIFIER ('=' expression)? ',' identifier_list
    ;
*/

expr_statement
    :   expression? ';'
    ;

expression
    :   assignment_expression
    //|   expression ',' assignment_expression
    ;

assignment_expression
    :   unary_expression '=' assignment_expression
    |   logical_or_expression
    ;

logical_or_expression
    :   logical_or_expression '||' logical_and_expression
    |   logical_and_expression
    ;

logical_and_expression
    :   logical_and_expression '&&' or_expression
    |   or_expression
    ;

or_expression
    :   or_expression '|' xor_expression
    |   xor_expression
    ;

xor_expression
    :   xor_expression '^' and_expression
    |   and_expression
    ;

and_expression
    :   and_expression '&' equality_expression
    |   equality_expression
    ;

equality_expression
    :   equality_expression EQUAL relation_expression
    |   equality_expression NOTEQUAL relation_expression
    |   relation_expression
    ;

relation_expression
    :   relation_expression GREATER shift_expression
    |   relation_expression LESS shift_expression
    |   relation_expression GREATEREQUAL shift_expression
    |   relation_expression LESSEQUAL shift_expression
    |   shift_expression
    ;

shift_expression
    :   shift_expression RIGHTSHIFT add_expression
    |   shift_expression LEFTSHIFT add_expression
    |   add_expression
    ;

add_expression
    :   add_expression PLUS multi_expression
    |   add_expression MINUS multi_expression
    |   multi_expression
    ;

multi_expression
    :   multi_expression TIMES creation_expression
    |   multi_expression DIVISION creation_expression
    |   multi_expression MOD creation_expression
    |   creation_expression
    ;

creation_expression
    :   'new' type_name
    |   unary_expression
    ;

unary_expression
    :   postfix_expression
    |   INCREASE unary_expression
    |   DECREASE unary_expression
    |   PLUS unary_expression
    |   MINUS unary_expression
    |   TILDE unary_expression
    |   NOT unary_expression
    ;
/*
parameter_list
    :   assignment_expression
    |   parameter_list ',' assignment_expression
    ;
*/
postfix_expression
    :   primary_expression
    |   postfix_expression INCREASE
    |   postfix_expression DECREASE
    |   postfix_expression LEFTBRACKET expression RIGHTBRACKET
    |   postfix_expression DOT IDENTIFIER
    |   postfix_expression LEFTPA (assignment_expression (',' assignment_expression)*)? RIGHTPA
    //|   postfix_expression '.' 'size' '(' parameter_list? ')'
    ;

primary_expression
    :   IDENTIFIER
    |   constant
    |   LEFTPA expression RIGHTPA
    ;

constant
    :   NULL
    |   TRUE
    |   FALSE
    |   INT_LITERAL
    |   STRING_LITERAL
    ;

if_statement
    :   'if' '(' expression ')' statement
    |   'if' '(' expression ')' statement ELSE statement
    ;

loop_statement
    :   FOR '(' init? ';' boundary? ';' step? ')' statement
    |   WHILE '(' expression ')' statement
    ;

init
    :   expression
    ;

boundary
    :   expression
    ;

step
    :   expression
    ;

jump_statement
    :   RETURN expression? ';'
    |   BREAK ';'
    |   CONTINUE ';'
    ;

BOOL : 'bool' ;
INT : 'int' ;
STRING : 'string' ;
NULL : 'null' ;
VOID : 'void' ;
TRUE : 'true' ;
FALSE : 'false' ;
NEW : 'new' ;
CLASS : 'class' ;
IF : 'if' ;
ELSE : 'else' ;
FOR : 'for' ;
WHILE : 'while' ;
RETURN : 'return' ;
BREAK : 'break' ;
CONTINUE : 'continue' ;

LEFTPA : '(' ;
RIGHTPA : ')' ;
LEFTBRACKET : '[' ;
RIGHTBRACKET : ']' ;
LEFTBRACE : '{' ;
RIGHTBRACE : '}' ;
DOT : '.' ;
COMMA : ',' ;

PLUS : '+' ;
MINUS : '-' ;
TIMES : '*' ;
DIVISION : '/' ;
MOD : '%' ;
ASSIGN : '=' ;

LESS : '<' ;
GREATER : '>' ;
EQUAL : '==' ;
NOTEQUAL : '!=' ;
LESSEQUAL : '<=' ;
GREATEREQUAL : '>=' ;

ANDAND : '&&' ;
OROR : '||' ;
NOT : '!' ;

LEFTSHIFT : '<<' ;
RIGHTSHIFT : '>>' ;
TILDE : '~' ;
OR : '|' ;
XOR : '^' ;
AND : '&' ;

INCREASE : '++' ;
DECREASE : '--' ;

IDENTIFIER
    :   NONDIGIT ( DIGIT | NONDIGIT )*
    ;

fragment
NONDIGIT
    :   [a-zA-Z_]
    ;

fragment
DIGIT
    :   [0-9]
    ;


INT_LITERAL
    :   NON_ZERO_DIGIT DIGIT*
    |   '0'
    ;

fragment
NON_ZERO_DIGIT
    :   [1-9]
    ;

STRING_LITERAL
    :   '"' SCHAR_SEQUENCE '"'
    ;

fragment
SCHAR_SEQUENCE
    :   SCHAR*
    ;

fragment
SCHAR
    :   ~["\\\r\n]
    |   ESCAPESEQUENCE
    ;

fragment
ESCAPESEQUENCE
    :   '\\' ['"?abfnrtv\\]
    ;

WHITESPACE
    :   [ \t]+
        -> skip
    ;

NEWLINE
    :   ( '\r' '\n'? | '\n')
        -> skip
    ;

LINECOMMENT
    :   '//' ~[\r\n]*
        -> skip
    ;

BLOCKCOMMENT
    :   '/*' .*? '*/'
        -> skip
    ;