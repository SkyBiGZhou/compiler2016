package AST;

import IR.Address;
import IR.Function;
import IR.Temp;
import IR.Memory;
import IR.MemoryExpr;
import IR.MemoryOp;
import IR.Move;
import IR.IntConst;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public class Decrement extends Expression {
    public Expression expr;

    public Decrement() {
        expr = null;
    }

    public Decrement(Expression expr) {
        this.expr = expr;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("Decrement\n");
        expr.print(d+1);
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {

        if (!expr.thirdTraverse(table, scope, loopScope, func))
        {
            //outputError(this);
            return false;
        }

        if (!expr.lvalue) {
            outputError(this);
            return false;
        }

        if (!(expr.getType instanceof IntType)) {
            outputError(this);
            return false;
        }

        this.getType = expr.getType;
        this.lvalue = false;

        return true;
    }

    @Override
    public Address InReg(FunctionDecl func, Function translate,
                         Table table, int scope, Stack<AST> loopScope) {
        Address ope = expr.InReg(func, translate, table, scope, loopScope);
        Temp ret = new Temp();
        if (ope instanceof Memory) {
            MemoryExpr tmp = new MemoryExpr();
            tmp.op = MemoryOp.LOAD;
            tmp.dest = ret;
            tmp.ope = ((Memory) ope).pointer;
            tmp.offset = ((Memory) ope).offset;
            translate.body.add(tmp);

            IR.BinaryExpr dec = new IR.BinaryExpr();
            dec.dest = new Temp();
            dec.op = IR.BinaryOp.SUB;
            dec.ope1 = ret;
            dec.ope2 = new IntConst(1);
            translate.body.add(dec);

            MemoryExpr st = new MemoryExpr();
            st.op = MemoryOp.STORE;
            st.dest = ((Memory) ope).pointer;
            st.ope = dec.dest;
            st.offset = ((Memory) ope).offset;
            translate.body.add(st);

        } else {
            Move tmp = new Move();
            tmp.dest = ret;
            tmp.ope = ope;
            translate.body.add(tmp);

            IR.BinaryExpr dec = new IR.BinaryExpr();
            dec.dest = ope;
            dec.op = IR.BinaryOp.SUB;
            dec.ope1 = ret;
            dec.ope2 = new IntConst(1);
            translate.body.add(dec);

            //Move st = new Move(ope, inc.dest);
            //translate.body.add(st);
        }
        return ret;
    }
}
