package AST;

/**
 * Created by zhouyuhao on 2016/4/1.
 */
public abstract class Type extends AST {

    public Type getBaseType() {
        if (this instanceof ArrayType) {
            return ((ArrayType) this).basetype.getBaseType();
        } else {
            return this;
        }
    }

}
