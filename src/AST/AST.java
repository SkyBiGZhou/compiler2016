package AST;

import IR.Address;

import java.util.Stack;

public abstract class AST {
    public int scope;
    public int row;
    public Address reg;

    public void print(int d) {}

    public void indent(int d) {
        for (int i = 0; i < d; ++i) {
            System.out.print("\t");
        }
    }

    public boolean cmpType(Type left, Type right) {
        //检查是否同种类型
        if (!left.getClass().equals(right.getClass())) return false;
        //检查自定义类
        if (left instanceof ClassType && right instanceof ClassType) {
            if (!((ClassType) left).id.toString().equals(((ClassType) right).id.toString()))
                return false;
        }
        //检查数组类型
        else if (left instanceof ArrayType) {
            return cmpType(((ArrayType) left).basetype, ((ArrayType) right).basetype);
        }
        return true;
    }

    public boolean firstTraverse(Table table, int scope) {return true;}
    public boolean secondTraverse(Table table, int scope) {return true;}
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {return true;}

    public void outputError(AST node) {
        System.out.println(node.row);
        //System.out.println(node instanceof FunctionDecl);
    }

}
