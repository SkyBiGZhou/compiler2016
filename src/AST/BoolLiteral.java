package AST;

import IR.Address;
import IR.Function;
import IR.IntConst;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public class BoolLiteral extends Expression {
    public boolean value;

    public BoolLiteral() {
        value = false;
    }

    public BoolLiteral(boolean value) {
        this.value = value;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print(value);
        System.out.print("\n");
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        this.getType = new BoolType();
        this.lvalue = false;
        return true;
    }

    @Override
    public Address InReg(FunctionDecl func, Function translate,
                         Table table, int scope, Stack<AST> loopScope) {
        if (value == false) {
            IntConst tmp = new IntConst(0);
            return tmp;
        } else {
            IntConst tmp = new IntConst(1);
            return tmp;
        }
    }
}
