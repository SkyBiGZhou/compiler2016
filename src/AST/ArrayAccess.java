package AST;

import IR.Address;
import IR.Memory;
import IR.Function;
import IR.Temp;
import IR.MemoryOp;
import IR.MemoryExpr;
import IR.IntConst;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public class ArrayAccess extends Expression {
    public Expression expr;
    public Expression position;

    public ArrayAccess() {
        expr = null;
        position = null;
    }

    public ArrayAccess(Expression expr, Expression position) {
        this.expr = expr;
        this.position = position;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("ArrayAccess\n");
        expr.print(d+1);
        position.print(d+1);
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        if (!expr.thirdTraverse(table, scope, loopScope, func)) {
            //outputError(this);
            return false;
        }
        if (!position.thirdTraverse(table, scope, loopScope, func)) {
            //outputError(this);
            return false;
        }

        //System.out.println(expr.getType);

        if (!(expr.getType instanceof ArrayType)) {
            outputError(this);
            return false;
        }
        if (!(position.getType instanceof IntType)) {
            outputError(this);
            return false;
        }

        this.getType = ((ArrayType) expr.getType).basetype;
        this.lvalue = true;

        return true;
    }

    @Override
    public Address InReg(FunctionDecl func, Function translate,
                         Table table, int scope, Stack<AST> loopScope) {
        Memory res = (Memory) this.ForAddress(func, translate, table, scope, loopScope);
        Temp tmp = new Temp();
        IR.BinaryExpr addr = new IR.BinaryExpr();
        addr.dest = tmp;
        addr.op = IR.BinaryOp.ADD;
        addr.ope1 = res.pointer;
        addr.ope2 = res.offset;
        translate.body.add(addr);

        translate.body.add(new MemoryExpr(tmp, MemoryOp.LOAD, tmp, new IntConst(0)));
        return tmp;
        /*
        Temp reg = new Temp();
        MemoryExpr tmp = new MemoryExpr();
        tmp.dest = reg;
        tmp.op = MemoryOp.LOAD;
        tmp.ope = expr.InReg(func, translate, table, scope, loopScope);
        tmp.offset = position.InReg(func, translate, table, scope, loopScope);
        //System.out.print(tmp.offset.toString() + "\n");
        if (tmp.offset instanceof IntConst) {
            //System.out.print(((IntConst) tmp.offset).value+ "\n");
            tmp.offset = new IntConst((((IntConst) tmp.offset).value + 1) * 4);
        } else {
            IR.BinaryExpr cal1 = new IR.BinaryExpr();
            cal1.dest = new Temp();
            cal1.op = IR.BinaryOp.ADD;
            cal1.ope1 = tmp.offset;
            cal1.ope2 = new IntConst(1);
            translate.body.add(cal1);

            IR.BinaryExpr cal2 = new IR.BinaryExpr();
            cal2.dest = new Temp();
            cal2.op = IR.BinaryOp.MUL;
            cal2.ope1 = cal1.dest;
            cal2.ope2 = new IntConst(4);
            translate.body.add(cal2);

            IR.BinaryExpr cal3 = new IR.BinaryExpr();
            cal3.dest = new Temp();
            cal3.op = IR.BinaryOp.ADD;
            cal3.ope1 = tmp.ope;
            cal3.ope2 = cal2.dest;
            translate.body.add(cal3);

            Move move = new Move(new Temp(), cal3.dest);
            translate.body.add(move);

            tmp.ope = move.dest;
            tmp.offset = new IntConst(0);
        }
        translate.body.add(tmp);
        //System.out.print("#" + reg.toString() + "\n");
        return reg;
        */
    }

    @Override
    public Address ForAddress(FunctionDecl func, Function translate,
                              Table table, int scope, Stack<AST> loopScope) {
        Memory mem = new Memory();
        mem.pointer = expr.InReg(func, translate, table, scope, loopScope);
        mem.offset = position.InReg(func, translate, table, scope, loopScope);
        if (mem.offset instanceof IntConst) {

            mem.offset = new IntConst((((IntConst) mem.offset).value + 1) * 4);
        } else {
            Temp tmp = new Temp();
            translate.body.add(new IR.BinaryExpr(tmp, IR.BinaryOp.MUL, mem.offset, new IntConst(4)));
            translate.body.add(new IR.BinaryExpr(tmp, IR.BinaryOp.ADD, mem.pointer, tmp));

            mem.pointer = tmp;
            mem.offset = new IntConst(4);
        }

        return mem;
    }

}
