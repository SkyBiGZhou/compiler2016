package AST;

import IR.Function;
import IR.Label;
import IR.Temp;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public class FunctionDecl extends Declaration{
    public Type type;
    public Symbol id;
    public List<VarDecl> parameter;
    public CompoundStmt stmts;
    public Function map;

    public FunctionDecl() {
        type = null;
        id = null;
        parameter = new LinkedList<VarDecl>();
        stmts = null;
    }

    public FunctionDecl(Type type, Symbol id,
                        List<VarDecl> parameter, CompoundStmt stmts) {
        this.type = type;
        this.id = id;
        this.parameter = parameter;
        this.stmts = stmts;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("FunctionDecl\n");
        type.print(d + 1);
        id.print(d + 1);
        for (VarDecl eachvar : parameter) {
            if (eachvar != null) eachvar.print(d + 1);
        }
        stmts.print(d + 1);
    }

    @Override
    public boolean secondTraverse(Table table, int scope) {
        if (type instanceof ClassType) {
            if (table.get(((ClassType) type).id) == null) {
                outputError(this);
                return false;
            }
        }
        if (id.toString().equals("main")) {
            if (parameter.size() != 0) {
                outputError(this);
                return false;
            }
            if (!(type instanceof IntType)) {
                outputError(this);
                return false;
            }
        }
        //检查命名
        if (table.get(id) != null && table.get(id).scope == scope) {
            table.put(id, this, scope);
            outputError(this);
            return false;
        } else {
            table.put(id, this, scope);
            ++scope;
            table.beginScope();
            for (VarDecl p : parameter) {
                if (!p.secondTraverse(table, scope)) {
                    //outputError(this);
                    return false;
                }
            }
            //检查函数参数中的类名
            --scope;
            table.endScope();
        }
        return true;
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        table.beginScope();
        ++scope;
        for (VarDecl p : parameter) {
            p.secondTraverse(table, scope);
        }
        if (!stmts.thirdTraverse(table, scope, loopScope, this))
            return false;
        --scope;
        table.endScope();
        return true;
    }

    @Override
    public void IRtranslate(FunctionDecl func, Function translate,
                            Table table, int scope, Stack<AST> loopScope) {
        translate.name = id.toString();
        translate.entry = new Label();
        translate.exit = new Label();

        map = translate;
        table.beginScope();
        ++scope;
        for (VarDecl p : parameter) {
            table.put(p.id, p, scope);
            if (p.expr == null) {
                Temp reg = new Temp();
                translate.vars.add(reg);
                p.reg = reg;
            }
        }
        stmts.IRtranslate(this, translate, table, scope, loopScope);

        /*
        if (id.toString().equals("main")) {
            boolean flag = false;
            for (Statement stmt : stmts.stmt) {
                if (stmt instanceof ReturnStmt) {
                    flag = true;
                }
            }
            if (!flag) {
                translate.body.add(new Return(new IntConst(0)));
            }
        }

        if (type)
        */

        --scope;
        table.endScope();
        translate.size = Temp.tempCount;
    }

}
