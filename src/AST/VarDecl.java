package AST;

import IR.*;

import java.util.Stack;

import static IR.IR.initList;
import static IR.IR.varList;

/**
 * Created by zhouyuhao on 2016/4/1.
 */
public class VarDecl extends Declaration {
    public Type type;
    public Symbol id;
    public Expression expr;

    public VarDecl() {
        type = null;
        id = null;
        expr = null;
    }

    public VarDecl(Type type, Symbol id, Expression expr) {
        this.type = type;
        this.id = id;
        this.expr = expr;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("VarDecl\n");
        if (type != null) type.print(d+1);
        id.print(d+1);
        if (expr != null) expr.print(d+1);
    }

    @Override
    public boolean secondTraverse(Table table, int scope) {
        if (type instanceof ClassType) {
            if (table.get(((ClassType) type).id) == null) {
                outputError(this);
                return false;
            }
        } else if (type instanceof ArrayType) {
            if (type.getBaseType() instanceof ClassType) {
                if (table.get(((ClassType) type.getBaseType()).id) == null) {
                    outputError(this);
                    return false;
                }
            }
        }
        if (table.get(id) != null && table.get(id).scope == scope) {
            table.put(id, this, scope);
            outputError(this);
            return false;
        }
        table.put(id, this, scope);
        //System.out.println(id.toString());
        return true;
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        if (expr != null) {
            if (!expr.thirdTraverse(table, scope, loopScope, func)) {
                outputError(this);
                return false;
            }
        }

        if ((expr instanceof Null) && (type instanceof StringType)) {
            return false;
        }

        //自定义
        if (type instanceof ClassType) {
            AST node = table.get(((ClassType) type).id);
            if (node == null) {
                outputError(this);
                return false;
            }
            if (!(node instanceof ClassDecl)) {
                outputError(this);
                return false;
            }
        }
        //数组
        if (type instanceof ArrayType) {
            if (type.getBaseType() instanceof ClassType) {
                AST node = table.get(((ClassType) type.getBaseType()).id);
                if (node == null) {
                    outputError(this);
                    return false;
                }
                if (!(node instanceof ClassDecl)) {
                    outputError(this);
                    return false;
                }
            }
            Type tmp = type;
            while (tmp instanceof ArrayType) {
                if (((ArrayType) tmp).arraysize != null)
                {
                    outputError(this);
                    return false;
                }
                tmp = ((ArrayType) tmp).basetype;
            }
        }
        //是否重定义
        if (table.get(id) != null && table.get(id).scope == scope) {
            /*
            if (table.get(id) != null) {
                System.out.println("AAA"+id);
                System.out.println(id.getClass().toString());
            }
            */
            table.put(id, this, scope);
            {
                outputError(this);
                return false;
            }
        }
        table.put(id, this, scope);
        return true;
    }

    @Override
    public void IRtranslate(FunctionDecl func, Function translate, Table table, int scope, Stack<AST> loopScope) {

        table.put(id, this, scope);
        //System.out.print("Entry\n");
        //非全局变量的情况
        if (func != null) {
            //System.out.print("Entry\n");
            if (expr == null) {
                Temp reg = new Temp();
                table.get(id).reg = reg;
                return;
            }
            Address ope = expr.InReg(func, translate, table, scope, loopScope);

            Temp reg = new Temp();
            table.get(id).reg = reg;

            Move tmp = new Move();
            tmp.dest = reg;
            tmp.ope = ope;
            translate.body.add(tmp);
        } else {//全局变量
            Temp reg = new Temp();
            reg.name = id.id;
            varList.add(reg);
            table.get(id).reg = reg;
            //initialize
            if (expr != null) {
                Function initial = new Function();
                Move move = new Move();
                move.dest = reg;
                move.ope = expr.InReg(func, initial, table, scope, loopScope);
                for (Quadruple init: initial.body) {
                    initList.add(init);
                }
                initList.add(move);
            }

        }
        return;
    }
}
