package AST;

import IR.*;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/1.
 */
public class IfStmt extends Statement {
    public Expression condition;
    public Statement branch_true;
    public Statement branch_false;

    public IfStmt() {
        condition = null;
        branch_true = null;
        branch_false = null;
    }

    public IfStmt(Expression condition,
                  Statement branch_true, Statement branch_false) {
        this.condition = condition;
        this.branch_true = branch_true;
        this.branch_false = branch_false;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("IfStmt\n");
        condition.print(d+1);
        branch_true.print(d+1);
        if (branch_false != null) branch_false.print(d+1);
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        if (!condition.thirdTraverse(table, scope, loopScope, func))
            return false;
        if (!(condition.getType instanceof BoolType)) {
            return false;
        }

        table.beginScope();
        ++scope;
        if (!branch_true.thirdTraverse(table, scope, loopScope, func))
            return false;
        --scope;
        table.endScope();

        if (branch_false != null) {
            table.beginScope();
            ++scope;
            if (!branch_false.thirdTraverse(table, scope, loopScope, func))
                return false;
            --scope;
            table.endScope();
        }

        return true;
    }

    @Override
    public void IRtranslate(FunctionDecl func, Function translate,
                            Table table, int scope, Stack<AST> loopScope) {
        Temp reg = (Temp) condition.InReg(func, translate, table, scope, loopScope);
        Branch br = new Branch();
        br.ope = reg;
        br.labelForTrue = new Label();
        br.labelForFalse = new Label();
        Label end = new Label();
        Goto gotoEnd = new Goto(end);
        translate.body.add(br);
        translate.body.add(br.labelForTrue);
        table.beginScope();
        ++scope;
        branch_true.IRtranslate(func, translate, table, scope, loopScope);
        --scope;
        table.endScope();
        translate.body.add(gotoEnd);
        translate.body.add(br.labelForFalse);
        if (branch_false != null) {
            table.beginScope();
            ++scope;
            branch_false.IRtranslate(func, translate, table, scope, loopScope);
            --scope;
            table.endScope();
            translate.body.add(gotoEnd);
        }
        translate.body.add(end);
    }
}
