package AST;

import IR.Function;
import IR.Goto;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/1.
 */
public class BreakStmt extends Statement {
    public void print(int d) {
        indent(d);
        System.out.print("BreakStmt\n");
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        if (loopScope.empty()) {
            outputError(this);
            return false;
        }
        return true;
    }

    @Override
    public void IRtranslate(FunctionDecl func, Function translate,
                            Table table, int scope, Stack<AST> loopScope) {
        Goto gotoBreak = new Goto();
        if (loopScope.peek() instanceof ForStmt) {
            gotoBreak.label = ((ForStmt) loopScope.peek()).labelForBre;
        }
        if (loopScope.peek() instanceof WhileStmt) {
            gotoBreak.label = ((WhileStmt) loopScope.peek()).labelForBre;
        }
        translate.body.add(gotoBreak);
        return;
    }
}

