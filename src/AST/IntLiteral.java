package AST;

import IR.Address;
import IR.Function;
import IR.IntConst;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public class IntLiteral extends Expression {
    public int value;

    public IntLiteral() {
        value = 0;
    }

    public IntLiteral(int value) {
        this.value = value;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print(value);
        System.out.print("\n");
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        this.getType = new IntType();
        this.lvalue = false;
        return true;
    }

    @Override
    public Address InReg(FunctionDecl func, Function translate,
                         Table table, int scope, Stack<AST> loopScope) {
        IntConst tmp = new IntConst(value);
        return tmp;
    }
}

