package AST;

import IR.Address;
import IR.Function;
import IR.IntConst;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/5.
 */
public class Null extends Expression {
    @Override
    public void print(int d) {
        indent(d);
        System.out.print("NULL\n");
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        this.lvalue = true;
        //null has no type for it.
        return true;
    }

    @Override
    public Address InReg(FunctionDecl func, Function translate,
                         Table table, int scope, Stack<AST> loopScope) {
        IntConst tmp = new IntConst(0);
        return tmp;
    }
}
