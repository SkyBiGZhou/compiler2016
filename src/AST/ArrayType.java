package AST;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public class ArrayType extends Type {
    public Type basetype;
    public Expression arraysize;

    public ArrayType() {
        basetype = null;
        arraysize = null;
    }

    public ArrayType(Type basetype, Expression arraysize) {
        this.basetype = basetype;
        this.arraysize = arraysize;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("ArrayType\n");
        basetype.print(d+1);
        if (arraysize != null) arraysize.print(d+1);
    }

}
