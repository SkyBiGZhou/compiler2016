package AST;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public class BoolType extends BasicType {
    @Override
    public void print(int d) {
        indent(d);
        System.out.print("BoolType\n");
    }
}
