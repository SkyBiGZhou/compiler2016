package AST;

import IR.Address;
import IR.Function;
import IR.IntConst;
import IR.MemoryExpr;
import IR.MemoryOp;
import IR.Allocation;
import IR.Temp;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public class CreationExpr extends Expression {
    public Type type;

    public CreationExpr() {
        type = null;
    }

    public CreationExpr(Type type) {
        this.type = type;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("CreationExpr\n");
        type.print(d+1);
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {

        //是否存在类

        if (!(type instanceof ArrayType)) {
            if (type instanceof ClassType)
                if (table.get(((ClassType) type).id) == null) {
                    //outputError(this);
                    return false;
                }
        } else {
            if (type.getBaseType() instanceof ClassType)
                if (table.get(((ClassType) type.getBaseType()).id) == null) {
                    //outputError(this);
                    return false;
                }

            //数组维数初始化检查

            Type tmp = type;
            while (((ArrayType) tmp).basetype instanceof ArrayType) {
                if (((ArrayType) tmp).arraysize != null) {
                    //outputError(this);
                    return false;
                }
                tmp = ((ArrayType) tmp).basetype;
            }

            if (((ArrayType) tmp).arraysize != null) {
                if (!((ArrayType) tmp).arraysize.thirdTraverse(table, scope, loopScope, func)) {
                    //outputError(this);
                    return false;
                }
            }

        }

        this.getType = type;
        this.lvalue = false;

        return true;
    }


    @Override
    public Address InReg(FunctionDecl func, Function translate,
                         Table table, int scope, Stack<AST> loopScope) {

        Allocation tmp = new Allocation();
        tmp.pointer = new Temp();
        if (type instanceof ArrayType) {
            Expression sizeExpr = null;
            Type cntType = type;
            while (sizeExpr == null) {
                sizeExpr = ((ArrayType) cntType).arraysize;
                cntType = ((ArrayType) cntType).basetype;
            }
            Address size = sizeExpr.InReg(func, translate, table, scope, loopScope);

            if (size instanceof IntConst) {
                tmp.size = new IntConst((((IntConst) size).value + 1) * 4);
            } else {
                IR.BinaryExpr calSize1 = new IR.BinaryExpr();
                calSize1.dest = new Temp();
                calSize1.op = IR.BinaryOp.ADD;
                calSize1.ope1 = size;
                calSize1.ope2 = new IntConst(1);
                translate.body.add(calSize1);

                IR.BinaryExpr calSize2 = new IR.BinaryExpr();
                calSize2.dest = new Temp();
                calSize2.op = IR.BinaryOp.MUL;
                calSize2.ope1 = calSize1.dest;
                calSize2.ope2 = new IntConst(4);
                translate.body.add(calSize2);

                tmp.size = calSize2.dest;
            }

            translate.body.add(tmp);
            MemoryExpr memExpr = new MemoryExpr();
            memExpr.op = MemoryOp.STORE;
            memExpr.dest = tmp.pointer;
            memExpr.ope = size;
            memExpr.offset = new IntConst(0);
            translate.body.add(memExpr);
        }
        if (type instanceof ClassType) {
            ClassDecl className = (ClassDecl) table.get(((ClassType) type).id);
            tmp.size = new IntConst(className.getSize(table));
            translate.body.add(tmp);
        }
        return tmp.pointer;
    }


}
