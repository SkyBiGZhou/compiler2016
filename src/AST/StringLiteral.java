package AST;

import IR.*;

import java.util.Stack;

import static IR.IR.stringList;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public class StringLiteral extends Expression {
    public String value;

    public StringLiteral() {
        value = null;
    }

    public StringLiteral(String value) {
        this.value = value;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print(value + "\n");
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        this.getType = new StringType();
        this.lvalue = false;
        return true;
    }


    @Override
    public Address InReg(FunctionDecl func, Function translate,
                         Table table, int scope, Stack<AST> loopScope) {
        StringConst str = new StringConst(value);
        Temp reg = new Temp();
        Move move = new Move(reg, str);
        translate.body.add(move);
        stringList.add(str);
        return reg;
    }

}
