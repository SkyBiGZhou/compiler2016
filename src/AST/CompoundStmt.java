package AST;

import IR.Function;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class CompoundStmt extends Statement {
    public List<Statement> stmt;

    public CompoundStmt() {
        stmt = new LinkedList<Statement>();
    }

    public CompoundStmt(List<Statement> stmt) {
        this.stmt = stmt;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("CompoundStmt\n");
        for (Statement eachstmt : stmt) {
            if (eachstmt != null) eachstmt.print(d+1);
        }
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        /*
        if (func == null) {
            table.beginScope();
            ++scope;
        }
        */
        //System.out.println(scope);

        for (Statement eachStmt : stmt) {
            if (eachStmt != null)
                if (!eachStmt.thirdTraverse(table, scope, loopScope, func)) {
                    //outputError(this);
                    return false;
                }
        }
        /*
        if (func == null) {
            --scope;
            table.endScope();
        }
        */
        return true;
    }

    @Override
    public void IRtranslate(FunctionDecl func, Function translate,
                            Table table, int scope, Stack<AST> loopScope) {
        for (Statement eachStmt: stmt) {
            if (eachStmt != null) {
                eachStmt.IRtranslate(func, translate, table, scope, loopScope);
            }
        }
    }

}
