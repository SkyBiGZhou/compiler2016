package AST;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public class StringType extends BasicType {
    @Override
    public void print(int d) {
        indent(d);
        System.out.print("String\n");
    }
}
