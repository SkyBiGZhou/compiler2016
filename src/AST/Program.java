package AST;

import IR.Function;
import IR.IR;
import IR.Temp;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class Program extends AST {
    public List<Declaration> decl;

    public Program() {
        decl = new LinkedList<Declaration>();
    }

    public Program(List<Declaration> decl) {
        this.decl = decl;
    }

    @Override
    public void print(int d) {
        for (Declaration eachDecl : decl) {
            eachDecl.print(d + 1);
        }
    }

    @Override
    public boolean firstTraverse(Table table, int scope) {
        for (Declaration eachDecl : decl) {
            if (!eachDecl.firstTraverse(table, scope))
                return false;
        }
        return true;
    }

    @Override
    public boolean secondTraverse(Table table, int scope) {
        for (Declaration eachDecl : decl) {
            if (eachDecl instanceof FunctionDecl) {
                if (!eachDecl.secondTraverse(table, scope))
                    return false;
            }
        }
        return true;
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        for (Declaration eachDecl : decl) {
            if (!eachDecl.thirdTraverse(table, scope, loopScope, func))
                return false;
        }
        return true;
    }

    public IR IRtranslate(FunctionDecl func, Table table, int scope, Stack<AST> loopScope) {
        IR threeAddressCode = new IR();
        for (Declaration eachDecl : decl) {
            Temp.setCount();
            if (eachDecl instanceof FunctionDecl) {
                Function translate = new Function();
                eachDecl.IRtranslate(func, translate, table, scope, loopScope);
                threeAddressCode.functionList.add(translate);
            } else if (eachDecl instanceof VarDecl) {
                eachDecl.IRtranslate(func, null, table, scope, loopScope);
            }

        }
        return threeAddressCode;
    }

}

