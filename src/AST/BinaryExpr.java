package AST;

import IR.Address;
import IR.Temp;
import IR.Memory;
import IR.IntConst;
import IR.Move;
import IR.MemoryExpr;
import IR.MemoryOp;
import IR.Branch;
import IR.Goto;
import IR.Label;
import IR.Function;
import IR.Call;
import IR.RelationExpr;
import IR.RelationOp;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/1.
 */
public class BinaryExpr extends Expression {
    public Expression left;
    public BinaryOp op;
    public Expression right;

    public BinaryExpr() {
        left = null;
        op = null;
        right = null;
    }

    public BinaryExpr(Expression left, BinaryOp op,
                      Expression right) {
        this.left = left;
        this.op = op;
        this.right = right;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("Binary" + op.toString() + "\n");
        left.print(d+1);
        right.print(d+1);
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        if (!left.thirdTraverse(table, scope, loopScope, func)) {
            //outputError(this);
            return false;
        }
        if (!right.thirdTraverse(table, scope, loopScope, func)) {
            //outputError(this);
            return false;
        }

        if (!(left instanceof Null) && !(right instanceof Null)) {
            Type leftTp = left.getType;
            Type rightTp = right.getType;
            if (!cmpType(leftTp, rightTp)) {
                outputError(this);
                return false;
            }

            if (op == BinaryOp.ASSIGN) {
                if (!left.lvalue) {
                    outputError(this);
                    return false;
                }
                this.getType = leftTp;
            }

            if (op == BinaryOp.ANDAND || op == BinaryOp.OROR) {
                if (!(leftTp instanceof BoolType)) {
                    outputError(this);
                    return false;
                }
                this.getType = new BoolType();
            }

            if (op == BinaryOp.OR || op == BinaryOp.AND || op == BinaryOp.XOR) {
                if (!(leftTp instanceof BoolType)) {
                    outputError(this);
                    return false;
                }
                this.getType = leftTp;
            }

            if (op == BinaryOp.EQUAL) {
                if (leftTp instanceof VoidType) {
                    outputError(this);
                    return false;
                }
                this.getType = new BoolType();
            }

            if (op == BinaryOp.NOTEQUAL) {
                if (leftTp instanceof VoidType) {
                    outputError(this);
                    return false;
                }

                if (leftTp instanceof StringType) {
                    outputError(this);
                    return false;
                }
                this.getType = new BoolType();
            }

            if (op == BinaryOp.LESS || op == BinaryOp.GREATER
                || op == BinaryOp.LESSEQUAL || op == BinaryOp.GREATEREQUAL) {
                if (leftTp instanceof VoidType) {
                    outputError(this);
                    return false;
                }
                if (leftTp instanceof BoolType) {
                    outputError(this);
                    return false;
                }
                if (leftTp instanceof ClassType) {
                    outputError(this);
                    return false;
                }
                if (leftTp instanceof ArrayType) {
                    outputError(this);
                    return false;
                }
                this.getType = new BoolType();
            }

            if (op == BinaryOp.LEFTSHIFT || op == BinaryOp.RIGHTSHIFT) {
                if (!(leftTp instanceof IntType)) {
                    outputError(this);
                    return false;
                }
                this.getType = leftTp;
            }

            if (op == BinaryOp.ADD) {
                if (!(leftTp instanceof IntType) && !(leftTp instanceof StringType)) {
                    outputError(this);
                    return false;
                }

                this.getType = leftTp;
            }

            if (op == BinaryOp.SUB || op == BinaryOp.TIMES || op == BinaryOp.DIVISION || op == BinaryOp.MOD) {
                if (!(leftTp instanceof IntType)) {
                    outputError(this);
                    return false;
                }
                this.getType = leftTp;
            }

            this.lvalue = false;

        } else {

            if ((op != BinaryOp.ASSIGN) && (op != BinaryOp.EQUAL) && (op != BinaryOp.NOTEQUAL)) {
                outputError(this);
                return false;
            }

            if (op == BinaryOp.ASSIGN) {
                if (right instanceof Null && !(left instanceof Null)) {
                    Type leftTp = left.getType;
                    if (!(leftTp instanceof ClassType) && !(leftTp instanceof ArrayType))
                    {
                        outputError(this);
                        return false;
                    }
                    this.getType = leftTp;
                    this.lvalue = false;
                } else {
                    outputError(this);
                    return false;
                }
            }

            if (op == BinaryOp.EQUAL || op == BinaryOp.NOTEQUAL) {
                if (left instanceof Null && right instanceof Null) {
                    this.getType = new BoolType();
                    this.lvalue = false;
                } else {
                    if (right instanceof Null) {
                        Type leftTp = left.getType;
                        if (!(leftTp instanceof ClassType) && !(leftTp instanceof ArrayType))
                        {
                            outputError(this);
                            return false;
                        }
                    }
                    if (left instanceof Null) {
                        Type rightTp = right.getType;
                        if (!(rightTp instanceof ClassType) && !(rightTp instanceof ArrayType))
                        {
                            outputError(this);
                            return false;
                        }
                    }
                    this.getType = new BoolType();
                    this.lvalue = false;
                }
            }

        }

        return true;
    }

    @Override
    public Address InReg(FunctionDecl func, Function translate,
                               Table table, int scope, Stack<AST> loopScope) {

        //赋值运算符
        if (op == BinaryOp.ASSIGN) {
            if (!(left instanceof ArrayAccess)
                    && !(left instanceof ClassMem)) {
                //System.out.print(left.getType.toString() + "\n");
                Move tmp = new Move();
                tmp.dest = left.ForAddress(func, translate, table, scope, loopScope);
                tmp.ope = right.InReg(func, translate, table, scope, loopScope);
                translate.body.add(tmp);
                return tmp.dest;
            } else {
                //System.out.print("Entry!\n");
                MemoryExpr mem = new MemoryExpr();
                mem.op = MemoryOp.STORE;
                Memory fromMem = (Memory) left.ForAddress(func, translate, table, scope, loopScope);
                mem.ope = right.InReg(func, translate, table, scope, loopScope);
                mem.dest = fromMem.pointer;
                mem.offset = fromMem.offset;
                //mem.print();
                translate.body.add(mem);
                return mem.ope;
            }
        }

        //短路求值 For &&
        if (op == BinaryOp.ANDAND) {
            Address l = left.InReg(func, translate, table, scope, loopScope);
            if (l instanceof IntConst) {
                if (((IntConst) l).value == 0) {
                    return new IntConst(0);
                } else {
                    return right.InReg(func, translate, table, scope, loopScope);
                }
            } else {
                Temp ret = new Temp();
                Label labelForCon = new Label();
                Label labelForTrue = new Label();
                Label labelForFalse = new Label();
                Label labelForEnd = new Label();
                //System.out.print(labelForCon.toString() + "\n");
                Branch brForLeft = new Branch(l, labelForCon, labelForFalse);
                translate.body.add(brForLeft);
                translate.body.add(labelForCon);
                Address r = right.InReg(func, translate, table, scope, loopScope);
                Branch brForRight = new Branch(r, labelForTrue, labelForFalse);
                translate.body.add(brForRight);
                translate.body.add(labelForTrue);
                Move move1 = new Move(ret, new IntConst(1));
                translate.body.add(move1);
                Goto gotoEnd1 = new Goto(labelForEnd);
                translate.body.add(gotoEnd1);
                translate.body.add(labelForFalse);
                Move move2 = new Move(ret, new IntConst(0));
                translate.body.add(move2);
                Goto gotoEnd2 = new Goto(labelForEnd);
                translate.body.add(gotoEnd2);
                translate.body.add(labelForEnd);
                return ret;
            }
        }

        //短路求值 For ||
        if (op == BinaryOp.OROR) {
            Address l = left.InReg(func, translate, table, scope, loopScope);
            if (l instanceof IntConst) {
                if (((IntConst) l).value == 1) {
                    return new IntConst(1);
                } else {
                    return right.InReg(func, translate, table, scope, loopScope);
                }
            } else {
                Temp ret = new Temp();
                Label labelForCon = new Label();
                Label labelForTrue = new Label();
                Label labelForFalse = new Label();
                Label labelForEnd = new Label();
                Branch brForLeft = new Branch(l, labelForTrue, labelForCon);
                translate.body.add(brForLeft);
                translate.body.add(labelForCon);
                Address r = right.InReg(func, translate, table, scope, loopScope);
                Branch brForRight = new Branch(r, labelForTrue, labelForFalse);
                translate.body.add(brForRight);
                translate.body.add(labelForTrue);
                Move move1 = new Move(ret, new IntConst(1));
                translate.body.add(move1);
                Goto gotoEnd1 = new Goto(labelForEnd);
                translate.body.add(gotoEnd1);
                translate.body.add(labelForFalse);
                Move move2 = new Move(ret, new IntConst(0));
                translate.body.add(move2);
                Goto gotoEnd2 = new Goto(labelForEnd);
                translate.body.add(gotoEnd2);
                translate.body.add(labelForEnd);
                return ret;
            }
        }

        Address ope1 = left.InReg(func, translate, table, scope, loopScope);
        Address ope2 = right.InReg(func, translate, table, scope, loopScope);

        //两个常数直接返回常数
        if ((ope1 instanceof IntConst)
                && (ope2 instanceof IntConst)) {
            switch (op) {
                case ADD: return new IntConst(((IntConst) ope1).value
                        + ((IntConst) ope2).value);
                case SUB: return new IntConst(((IntConst) ope1).value
                        - ((IntConst) ope2).value);
                case TIMES: return new IntConst(((IntConst) ope1).value
                        * ((IntConst) ope2).value);
                case DIVISION: return new IntConst(((IntConst) ope1).value
                        / ((IntConst) ope2).value);
                case MOD: return new IntConst(((IntConst) ope1).value
                        % ((IntConst) ope2).value);
                case LEFTSHIFT: return new IntConst(((IntConst) ope1).value
                        << ((IntConst) ope2).value);
                case RIGHTSHIFT: return new IntConst(((IntConst) ope1).value
                        >> ((IntConst) ope2).value);
                case OR: return new IntConst(((IntConst) ope1).value
                        | ((IntConst) ope2).value);
                case XOR: return new IntConst(((IntConst) ope1).value
                        ^ ((IntConst) ope2).value);
                case AND: return new IntConst(((IntConst) ope1).value
                        & ((IntConst) ope2).value);
                case EQUAL: if (((IntConst) ope1).value == ((IntConst) ope2).value) return new IntConst(1);
                    else return new IntConst(0);
                case NOTEQUAL: if (((IntConst) ope1).value != ((IntConst) ope2).value) return new IntConst(1);
                    else return new IntConst(0);
                case LESS: if (((IntConst) ope1).value < ((IntConst) ope2).value) return new IntConst(1);
                    else return new IntConst(0);
                case GREATER: if (((IntConst) ope1).value > ((IntConst) ope2).value) return new IntConst(1);
                    else return new IntConst(0);
                case LESSEQUAL: if (((IntConst) ope1).value <= ((IntConst) ope2).value) return new IntConst(1);
                    else return new IntConst(0);
                case GREATEREQUAL: if (((IntConst) ope1).value >= ((IntConst) ope2).value) return new IntConst(1);
                    else return new IntConst(0);
                case OROR: if ((((IntConst) ope1).value | ((IntConst) ope2).value) == 1) return new IntConst(1);
                    else return new IntConst(0);
                case ANDAND: if ((((IntConst) ope1).value & ((IntConst) ope2).value) == 1) return new IntConst(1);
                    else return new IntConst(0);
            }
        }

        /*
        if ((ope1 instanceof StringConst)
                && (ope2 instanceof StringConst)) {
            if (op == BinaryOp.ADD) {
                return new StringConst(((StringConst) ope1).str
                        + ((StringConst) ope2).str);
            }
        }
        */
        //字符串操作符重载转成内建函数
        if (left.getType instanceof StringType
                && right.getType instanceof StringType) {
            Call stringOp = new Call();
            stringOp.dest = new Temp();
            switch (op) {
                case ADD: stringOp.name = "stringAdd";
                    stringOp.param.add(ope1);
                    stringOp.param.add(ope2);
                    break;
                case EQUAL: stringOp.name = "stringEquals";
                    stringOp.param.add(ope1);
                    stringOp.param.add(ope2);
                    break;
                case LESS: stringOp.name = "stringLessThan";
                    stringOp.param.add(ope1);
                    stringOp.param.add(ope2);
                    break;
                case LESSEQUAL: stringOp.name = "stringLessThanOrEquals";
                    stringOp.param.add(ope1);
                    stringOp.param.add(ope2);
                    break;
                case GREATER: stringOp.name = "stringLessThan";
                    stringOp.param.add(ope2);
                    stringOp.param.add(ope1);
                    break;
                case GREATEREQUAL: stringOp.name = "stringLessThanOrEquals";
                    stringOp.param.add(ope2);
                    stringOp.param.add(ope1);
                    break;
            }
            stringOp.callee = new Function();
            stringOp.callee.name = stringOp.name;

            translate.body.add(stringOp);
            return stringOp.dest;
        }


        //逻辑运算符
        if (op == BinaryOp.EQUAL
                || op == BinaryOp.NOTEQUAL
                || op == BinaryOp.LESS
                || op == BinaryOp.LESSEQUAL
                || op == BinaryOp.GREATER
                || op == BinaryOp.GREATEREQUAL) {

            Temp reg = new Temp();
            RelationExpr tmp = new RelationExpr();
            tmp.dest = reg;
            tmp.ope1 = ope1;
            //if (tmp.ope1 == null) System.out.print("Entry\n");
            tmp.ope2 = ope2;
            switch (op) {
                case EQUAL: tmp.op = RelationOp.EQ; break;
                case NOTEQUAL: tmp.op = RelationOp.NE; break;
                case LESS: tmp.op = RelationOp.LT; break;
                case GREATER: tmp.op = RelationOp.GT; break;
                case LESSEQUAL: tmp.op = RelationOp.LE; break;
                case GREATEREQUAL: tmp.op = RelationOp.GE; break;
                //case OROR: tmp.op = IR.BinaryOp.OR; break;
                //case ANDAND: tmp.op = IR.BinaryOp.AND; break;
            }
            translate.body.add(tmp);
            return reg;
        }

        //算术运算符
        Temp reg = new Temp();
        IR.BinaryExpr tmp = new IR.BinaryExpr();
        tmp.dest = reg;
        tmp.ope1 = ope1;
        tmp.ope2 = ope2;
        switch (op) {
            case ADD: tmp.op = IR.BinaryOp.ADD; break;
            case SUB: tmp.op = IR.BinaryOp.SUB; break;
            case TIMES: tmp.op = IR.BinaryOp.MUL; break;
            case DIVISION: tmp.op = IR.BinaryOp.DIV; break;
            case MOD: tmp.op = IR.BinaryOp.MOD; break;
            case LEFTSHIFT: tmp.op = IR.BinaryOp.SHL; break;
            case RIGHTSHIFT: tmp.op = IR.BinaryOp.SHR; break;
            case AND: tmp.op = IR.BinaryOp.AND; break;
            case OR: tmp.op = IR.BinaryOp.OR; break;
            case XOR: tmp.op = IR.BinaryOp.XOR; break;
            case OROR: tmp.op = IR.BinaryOp.OR; break;
            case ANDAND: tmp.op = IR.BinaryOp.AND; break;
        }
        translate.body.add(tmp);
        return reg;
    }

}
