package AST;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public enum UnaryOp {
    INCREASE, DECREASE,
    POS, NEG,
    TILDE, NOT, DOT
}
