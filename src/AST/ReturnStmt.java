package AST;

import IR.Address;
import IR.Function;
import IR.Return;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/1.
 */
public class ReturnStmt extends Statement {
    public Expression expr;

    public ReturnStmt() {
        expr = null;
    }

    public ReturnStmt(Expression expr) {
        this.expr = expr;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("ReturnStmt\n");
        if (expr != null) expr.print(d+1);
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        if (func == null) {
            outputError(this);
            return false;
        }
        if (expr == null) {
            if (func.type instanceof VoidType) return true;
            else {
                outputError(this);
                return false;
            }
        } else {
            if (!expr.thirdTraverse(table, scope, loopScope, func)) return false;
            if (!cmpType(func.type, expr.getType)) {
                outputError(this);
                return false;
            }
        }
        return true;
    }

    public void IRtranslate(FunctionDecl func, Function translate,
                            Table table, int scope, Stack<AST> loopScope) {
        Return tmp = new Return();
        if (expr != null) {
            Address ret = expr.InReg(func, translate, table, scope, loopScope);
            tmp.value = ret;
        }
        tmp.exit = translate.exit;
        translate.body.add(tmp);
        return;
    }
}
