package AST;

import IR.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public class FunctionCall extends Expression {
    public Symbol id;
    public List<Expression> parameter;

    public FunctionCall() {
        id = null;
        parameter = new LinkedList<Expression>();
    }

    public FunctionCall(Symbol id, List<Expression> parameter) {
        this.id = id;
        this.parameter = parameter;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("FunctionCall\n");
        id.print(d+1);
        for (Expression eachexpr : parameter) {
            eachexpr.print(d+1);
        }
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        if (id == null) {
            outputError(this);
            return false;
        }
        for (Expression eachExpr : parameter) {
            if (!eachExpr.thirdTraverse(table, scope, loopScope, func))
            {
                //outputError(this);
                return false;
            }
        }

        if (table.get(id) == null) {

            if (id.id == "print") {
                if (parameter.size() != 1) {
                    outputError(this);
                    return false;
                }
                Expression str = parameter.get(0);
                if (!(str.getType instanceof StringType)) {
                    outputError(this);
                    return false;
                }
                this.getType = new VoidType();
                this.lvalue = false;
                return true;
            }

            if (id.id == "println") {
                if (parameter.size() != 1) {
                    outputError(this);
                    return false;
                }
                Expression str = parameter.get(0);
                if (!(str.getType instanceof StringType)) {
                    outputError(this);
                    return false;
                }
                this.getType = new VoidType();
                this.lvalue = false;
                return true;
            }

            if (id.id == "getString") {
                if (parameter.size() != 0) {
                    outputError(this);
                    return false;
                }
                this.getType = new StringType();
                this.lvalue = false;
                return true;
            }

            if (id.id == "getInt") {
                if (parameter.size() != 0) {
                    outputError(this);
                    return false;
                }
                this.getType = new IntType();
                this.lvalue = false;
                return true;
            }

            if (id.id == "toString") {
                if (parameter.size() != 1) {
                    outputError(this);
                    return false;
                }
                Expression i = parameter.get(0);
                if (!(i.getType instanceof IntType))
                {
                    outputError(this);
                    return false;
                }
                this.getType = new StringType();
                this.lvalue = false;
                return true;
            }
            outputError(this);
            return false;
        }

        //除了内建函数以外的判断
        if (!(table.get(id) instanceof FunctionDecl)) {
            outputError(this);
            return false;
        }
        //对比调用时的参数和声明时的参数
        FunctionDecl function = (FunctionDecl) table.get(id);

        List<VarDecl> decl = function.parameter;
        List<Expression> call = parameter;

        if (decl.size() != call.size()) {
            outputError(this);
            return false;
        }
        for (int i = 0; i < decl.size(); ++i) {
            Type declTp = decl.get(i).type;
            Type callTp = call.get(i).getType;
            if (!cmpType(declTp, callTp)) {
                outputError(this);
                return false;
            }
        }

        this.getType = function.type;
        this.lvalue = false;

        return true;
    }

    @Override
    public Address InReg(FunctionDecl func, Function translate,
                         Table table, int scope, Stack<AST> loopScope) {

        if ((id.id == "print")
                && parameter.get(0) instanceof FunctionCall
                && ((FunctionCall) parameter.get(0)).id.id == "toString") {
            //id.id = "printInt";
            FunctionCall printInt = new FunctionCall();
            printInt.id = new Symbol("printInt");
            Expression param = ((FunctionCall) parameter.get(0)).parameter.get(0);
            printInt.parameter.add(param);
            //parameter = new LinkedList<>();
            //parameter.add(param);
            printInt.InReg(func, translate, table, scope, loopScope);
            return null;
        }

        if ((id.id == "println")
                && parameter.get(0) instanceof FunctionCall
                && ((FunctionCall) parameter.get(0)).id.id == "toString") {
            //id.id = "printlnInt";
            FunctionCall printlnInt = new FunctionCall();
            printlnInt.id = new Symbol("printlnInt");
            Expression param = ((FunctionCall) parameter.get(0)).parameter.get(0);
            printlnInt.parameter.add(param);
            //parameter = new LinkedList<>();
            //parameter.add(param);
            printlnInt.InReg(func, translate, table, scope, loopScope);
            return null;
        }

        // 优化print（toStrong）和println（toString）
        if ((id.id == "print")
                && (parameter.get(0) instanceof BinaryExpr)
                && ((BinaryExpr) parameter.get(0)).op == BinaryOp.ADD) {
            //System.out.print("Entry\n");
            BinaryExpr str = (BinaryExpr) parameter.get(0);
            List<Expression> paramLeft = new LinkedList<>();
            paramLeft.add(str.left);
            List<Expression> paramRight = new LinkedList<>();
            paramRight.add(str.right);
            FunctionCall printLeft = new FunctionCall(new Symbol("print"), paramLeft);
            FunctionCall printRight = new FunctionCall(new Symbol("print"), paramRight);
            printLeft.InReg(func, translate, table, scope, loopScope);
            printRight.InReg(func, translate, table, scope, loopScope);
            return null;
        }

        if ((id.id == "println")
                && parameter.get(0) instanceof BinaryExpr
                && ((BinaryExpr) parameter.get(0)).op == BinaryOp.ADD) {
            BinaryExpr str = (BinaryExpr) parameter.get(0);
            List<Expression> paramLeft = new LinkedList<>();
            paramLeft.add(str.left);
            List<Expression> paramRight = new LinkedList<>();
            paramRight.add(str.right);
            FunctionCall printLeft = new FunctionCall(new Symbol("print"), paramLeft);
            FunctionCall printRight = new FunctionCall(new Symbol("println"), paramRight);
            printLeft.InReg(func, translate, table, scope, loopScope);
            printRight.InReg(func, translate, table, scope, loopScope);
            return null;
        }

        Call tmp = new Call();

        for (Expression param: parameter) {
            if (param != null) {
                Address var = param.InReg(func, translate, table, scope, loopScope);
                if (var instanceof Temp) tmp.param.add(var);
                else {
                    Temp newReg = new Temp();
                    Move move = new Move();
                    move.dest = newReg;
                    move.ope = var;
                    translate.body.add(move);
                    tmp.param.add(newReg);
                }
            }
        }

        if (table.get(id) != null) {
            FunctionDecl function = (FunctionDecl) table.get(id);
            if (!(function.type instanceof VoidType)) {
                tmp.dest = new Temp();
            }
            tmp.name = id.id;
            tmp.callee = function.map;
        } else {
            tmp.name = id.id;
            switch (id.id) {
                case "print": tmp.dest = null; break;
                case "println": tmp.dest = null; break;
                case "printInt": tmp.dest = null; break;
                case "printlnInt": tmp.dest = null; break;
                case "getString": tmp.dest = new Temp(); break;
                case "getInt": tmp.dest = new Temp(); break;
                case "toString":
                    tmp.dest = new Temp(); break;
            }
            tmp.callee = new Function();
            tmp.callee.name = id.id;
        }

        translate.body.add(tmp);

        return tmp.dest;
    }
}
