package AST;

/**
 * Created by zhouyuhao on 2016/4/1.
 */
public enum BinaryOp {
    //COMMA,
    ASSIGN,
    OROR, ANDAND,
    OR, AND, XOR,
    EQUAL, NOTEQUAL, LESS, GREATER, LESSEQUAL, GREATEREQUAL,
    LEFTSHIFT, RIGHTSHIFT,
    ADD, SUB, TIMES, DIVISION, MOD
}
