package AST;

import IR.Address;
import IR.Function;
import IR.Memory;
import IR.Temp;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/1.
 */
public abstract class Expression extends Statement {
    public Type getType;
    public boolean lvalue;

    public Address InReg(FunctionDecl func, Function translate,
                               Table table, int scope, Stack<AST> loopScope) {
        Temp reg = new Temp();
        return reg;
    }


    public Address ForAddress(FunctionDecl func, Function translate,
                              Table table, int scope, Stack<AST> loopScope) {
        Address mem = new Memory();
        return mem;
    }

    @Override
    public void IRtranslate(FunctionDecl func, Function translate,
                            Table table, int scope, Stack<AST> loopScope) {
        this.InReg(func, translate, table, scope, loopScope);
    }

}
