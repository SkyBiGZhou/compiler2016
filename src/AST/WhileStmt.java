package AST;

import IR.Branch;
import IR.Function;
import IR.Goto;
import IR.Label;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/1.
 */
public class WhileStmt extends Statement {
    public Expression condition;
    public Statement stmt;
    public Label labelForBre;
    public Label labelForCon;

    public WhileStmt() {
        condition = null;
        stmt = null;
    }

    public WhileStmt(Expression condition, Statement stmt) {
        this.condition = condition;
        this.stmt = stmt;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("WhileStmt\n");
        condition.print(d+1);
        if (stmt != null) stmt.print(d+1);
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        if (condition == null) {
            outputError(this);
            return false;
        }
        if (!condition.thirdTraverse(table, scope, loopScope, func))
            return false;
        if (!(condition.getType instanceof BoolType))
            return false;

        table.beginScope();
        ++scope;
        loopScope.push(this);
        if (!stmt.thirdTraverse(table, scope, loopScope, func))
            return false;
        loopScope.pop();
        --scope;
        table.endScope();
        return true;
    }

    @Override
    public void IRtranslate(FunctionDecl func, Function translate,
                            Table table, int scope, Stack<AST> loopScope) {
        labelForCon = new Label();
        if (loopScope.empty() || this != loopScope.peek()) {
            Goto gotoWhile = new Goto(labelForCon);
            translate.body.add(gotoWhile);
        }
        loopScope.push(this);
        translate.body.add(labelForCon);
        Branch br = new Branch();
        br.ope = condition.InReg(func, translate, table, scope, loopScope);
        Label labelForTrue = new Label();
        br.labelForTrue = labelForTrue;
        labelForBre = new Label();
        br.labelForFalse = labelForBre;
        translate.body.add(br);
        translate.body.add(labelForTrue);
        table.beginScope();
        ++scope;
        stmt.IRtranslate(func, translate, table, scope, loopScope);
        --scope;
        table.endScope();
        Goto gotoCon = new Goto(labelForCon);
        translate.body.add(gotoCon);
        translate.body.add(labelForBre);
        loopScope.pop();
    }
}
