package AST;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public class ClassDecl extends Declaration {
    public Symbol id;
    public List<VarDecl> decls;

    public ClassDecl() {
        id = null;
        decls = new LinkedList<VarDecl>();
    }

    public ClassDecl(Symbol id, List<VarDecl> decls) {
        this.id = id;
        this.decls = decls;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("ClassDecl\n");
        id.print(d+1);
        for (VarDecl vardecl : decls) {
            vardecl.print(d+1);
        }
    }

    @Override
    public boolean firstTraverse(Table table, int scope) {

        if (table.get(id) != null && table.get(id).scope == scope) {
            table.put(id, this, scope);
            outputError(this); //类重定义
            return false;
        }
        table.put(id, this, scope);
        return true;
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        table.beginScope();
        ++scope;
        for (VarDecl eachDecl : decls) {
            if (!eachDecl.thirdTraverse(table, scope, loopScope, func))
            {
                //outputError(this);
                return false;
            }
        }
        --scope;
        table.endScope();
        return true;
    }

    public int getSize(Table table) {
        int num = 0;
        for (VarDecl decl : decls) {
            /*
            Type type = decl.expr.getType;
            if (type instanceof ArrayType) {
                num += ;
                continue;
            }
            if (type instanceof ClassType) {
                num += ((ClassDecl)table.get(((ClassType) type).id)).getSize(table);
                continue;
            }
            */
            num += 4;
        }
        return num;
    }

}
