package AST;

import IR.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/7.
 */
public class EmbeddedCall extends Expression {
    public Expression expr;
    public Symbol id;
    public List<Expression> parameter;

    public EmbeddedCall() {
        expr = null;
        id = null;
        parameter = new LinkedList<>();
    }

    public EmbeddedCall(Expression expr, Symbol id, List<Expression> parameter) {
        this.expr = expr;
        this.id = id;
        this.parameter = parameter;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("FunctionCall\n");
        for (Expression eachExpr : parameter) {
            eachExpr.print(d+1);
        }
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        if (!expr.thirdTraverse(table, scope, loopScope, func))
        {
            //outputError(this);
            return false;
        }

        String s = id.toString();

        if (s == "size") {
            //是否为数组类型
            if (!(expr.getType instanceof ArrayType)) {
                outputError(this);
                return false;
            }
            //参数个数为0
            if (parameter.size() != 0) {
                outputError(this);
                return false;
            }
            this.getType = new IntType();
            this.lvalue = false;
            return true;
        }
        if (s == "length") {
            if (!(expr.getType instanceof StringType)) {
                outputError(this);
                return false;
            }
            if (parameter.size() != 0) {
                outputError(this);
                return false;
            }
            this.getType = new IntType();
            this.lvalue = false;
            return true;
        }
        if (s == "substring") {
            if (!(expr.getType instanceof StringType)) {
                outputError(this);
                return false;
            }
            if (parameter.size() != 2) {
                outputError(this);
                return false;
            }
            //检查两个参数
            Expression left = parameter.get(0);
            Expression right = parameter.get(1);
            if (!left.thirdTraverse(table, scope, loopScope, func))
            {
                //outputError(this);
                return false;
            }
            if (!right.thirdTraverse(table, scope, loopScope, func))
            {
                //outputError(this);
                return false;
            }
            if (!(left.getType instanceof IntType))
            {
                outputError(this);
                return false;
            }
            if (!(right.getType instanceof IntType))
            {
                outputError(this);
                return false;
            }
            this.getType = new StringType();
            this.lvalue = false;
            return true;
        }
        if (s == "parseInt") {
            if (!(expr.getType instanceof StringType))
            {
                outputError(this);
                return false;
            }
            if (parameter.size() != 0) {
                outputError(this);
                return false;
            }

            this.getType = new IntType();
            this.lvalue = false;
            return true;
        }
        if (s == "ord") {
            if (!(expr.getType instanceof StringType))
            {
                outputError(this);
                return false;
            }
            if (parameter.size() != 1) {
                outputError(this);
                return false;
            }

            Expression pos = parameter.get(0);
            if (!pos.thirdTraverse(table, scope, loopScope, func))
            {
                //outputError(this);
                return false;
            }
            if (!(pos.getType instanceof IntType))
            {
                outputError(this);
                return false;
            }
            this.getType = new IntType();
            this.lvalue = false;
            return true;
        }

        return false;
    }

    @Override
    public Address InReg(FunctionDecl func, Function translate,
                         Table table, int scope, Stack<AST> loopScope) {

        Call tmp = new Call();

        tmp.name = id.id;
        tmp.param.add(expr.InReg(func, translate, table, scope, loopScope));
        switch (id.id) {
            case "size": tmp.dest = new Temp(); break;
            case "length": tmp.dest = new Temp(); break;
            case "substring": tmp.dest = new Temp(); break;
            case "parseInt": tmp.dest = new Temp(); break;
            case "ord": tmp.dest = new Temp(); break;
        }

        tmp.callee = new Function();
        tmp.callee.name = id.id;
        tmp.callee.vars.add(new Temp());

        for (Expression var : parameter) {
            Address vars = var.InReg(func, translate, table, scope, loopScope);
            if (vars instanceof Temp) {
                tmp.param.add(vars);
            } else {
                Temp reg = new Temp();
                Move move = new Move(reg, vars);
                translate.body.add(move);
                tmp.param.add(reg);
            }
        }
        translate.body.add(tmp);

        return tmp.dest;
    }

}
