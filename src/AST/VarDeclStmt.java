package AST;

import IR.Function;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public class VarDeclStmt extends Statement {
    public VarDecl vardecl;

    public VarDeclStmt() {
        vardecl = null;
    }

    public VarDeclStmt(VarDecl vardecl) {
        this.vardecl = vardecl;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("VarDeclStmt\n");
        vardecl.print(d+1);
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        if (!vardecl.thirdTraverse(table, scope, loopScope, func))
            return false;
        return true;
    }

    public void IRtranslate(FunctionDecl func, Function translate,
                            Table table, int scope, Stack<AST> loopScope) {
        vardecl.IRtranslate(func, translate, table, scope, loopScope);
        return;
    }
}
