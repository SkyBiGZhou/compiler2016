package AST;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public class ClassType extends Type {
    public Symbol id;

    public ClassType() {
        id = null;
    }

    public ClassType(Symbol id) {
        this.id = id;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("ClassType\n");
        id.print(d+1);
    }
}
