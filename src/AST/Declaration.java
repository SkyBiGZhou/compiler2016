package AST;

import IR.Function;

import java.util.Stack;

public abstract class Declaration extends AST {
    public void IRtranslate(FunctionDecl func, Function translate,
                            Table table, int scope, Stack<AST> loopScope) {}
}
