package AST;

import IR.Function;
import IR.Goto;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/1.
 */
public class ContinueStmt extends Statement {
    @Override
    public void print(int d) {
        indent(d);
        System.out.print("Continue\n");
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        if (loopScope.empty()) {
            outputError(this);
            return false;
        }
        return true;
    }

    @Override
    public void IRtranslate(FunctionDecl func, Function translate,
                            Table table, int scope, Stack<AST> loopScope) {
        Goto gotoCon = new Goto();
        if (loopScope.peek() instanceof ForStmt) {
            gotoCon.label = ((ForStmt) loopScope.peek()).labelForCon;
        }
        if (loopScope.peek() instanceof WhileStmt) {
            gotoCon.label = ((WhileStmt) loopScope.peek()).labelForCon;
        }
        translate.body.add(gotoCon);
        return;
    }

}
