package AST;

import IR.Address;
import IR.Temp;
import IR.Memory;
import IR.MemoryExpr;
import IR.MemoryOp;
import IR.Move;
import IR.Function;
import IR.IntConst;


import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/2.
 */
public class Increment extends Expression {
    public Expression expr;

    public Increment() {
        expr = null;
    }

    public Increment(Expression expr) {
        this.expr = expr;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("Increment\n");
        expr.print(d+1);
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        if (!expr.thirdTraverse(table, scope, loopScope, func))
            return false;

        if (!expr.lvalue) {
            outputError(this);
            return false;
        }

        if (!(expr.getType instanceof IntType)) {
            outputError(this);
            return false;
        }

        this.getType = expr.getType;
        this.lvalue = false;

        return true;
    }

    @Override
    public Address InReg(FunctionDecl func, Function translate,
                         Table table, int scope, Stack<AST> loopScope) {
        Address ope = expr.ForAddress(func, translate, table, scope, loopScope);



        if (ope instanceof Memory) {
            Temp ret = new Temp();
            Temp temp = new Temp();
            MemoryExpr memoryExpr = new MemoryExpr(temp, MemoryOp.LOAD, ((Memory) ope).pointer,((Memory) ope).offset);
            translate.body.add(memoryExpr);
            translate.body.add(new Move(ret, temp));
            translate.body.add(new IR.BinaryExpr(temp, IR.BinaryOp.ADD, temp, new IntConst(1)));
            translate.body.add(new MemoryExpr(((Memory) ope).pointer, MemoryOp.STORE, temp, ((Memory) ope).offset));
            return ret;
        } else {
            Temp ret = new Temp();
            Move tmp = new Move();
            tmp.dest = ret;
            tmp.ope = ope;
            translate.body.add(tmp);

            IR.BinaryExpr inc = new IR.BinaryExpr();
            inc.dest = ope;
            inc.op = IR.BinaryOp.ADD;
            inc.ope1 = ret;
            inc.ope2 = new IntConst(1);
            translate.body.add(inc);
            return ret;
        }

    }
}
