package AST;

import IR.Address;
import IR.Temp;
import IR.Function;
import IR.IntConst;
import IR.MemoryExpr;
import IR.MemoryOp;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/1.
 */
public class UnaryExpr extends Expression {
    public UnaryOp op;
    public Expression unary;

    public UnaryExpr() {
        op = null;
        unary = null;
    }

    public UnaryExpr(UnaryOp op, Expression unary) {
        this.op = op;
        this.unary = unary;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("Unary" + op + "\n");
        unary.print(d+1);
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        if (!unary.thirdTraverse(table, scope, loopScope, func))
            return false;

        Type unaryTp = unary.getType;

        if (op == UnaryOp.INCREASE || op == UnaryOp.DECREASE) {
            if (!(unaryTp instanceof IntType)) {
                outputError(this);
                return false;
            }
            if (!unary.lvalue) {
                outputError(this);
                return false;
            }
        }

        if (op == UnaryOp.POS || op == UnaryOp.NEG) {
            if (!(unaryTp instanceof IntType)) {
                outputError(this);
                return false;
            }
        }

        if (op == UnaryOp.TILDE) {
            if (!(unaryTp instanceof IntType)) {
                outputError(this);
                return false;
            }
        }

        if (op == UnaryOp.NOT) {
            if (!(unaryTp instanceof BoolType)) {
                outputError(this);
                return false;
            }
        }

        this.getType = unaryTp;
        this.lvalue = false;

        return true;
    }

    @Override
    public Address InReg(FunctionDecl func, Function translate,
                         Table table, int scope, Stack<AST> loopScope) {
        Address ope = unary.InReg(func, translate, table, scope, loopScope);

        Temp reg = new Temp();

        if (op == UnaryOp.INCREASE
                || op == UnaryOp.DECREASE) {
            IR.BinaryExpr tmp = new IR.BinaryExpr();
            tmp.dest = ope;
            tmp.ope1 = ope;
            tmp.ope2 = new IntConst(1);
            switch (op) {
                case INCREASE: tmp.op = IR.BinaryOp.ADD; break;
                case DECREASE: tmp.op = IR.BinaryOp.SUB; break;
            }
            translate.body.add(tmp);

            /*
            Move cnt = new Move();
            cnt.dest = ope;
            cnt.ope = reg;
            translate.body.add(cnt);
            return ope;
            */
            if (unary instanceof ArrayAccess) {
                MemoryExpr memExpr = new MemoryExpr();
                memExpr.op = MemoryOp.STORE;
                memExpr.dest = unary.ForAddress(func, translate, table, scope, loopScope);
                memExpr.ope = ope;
                memExpr.offset = new IntConst(0);
                translate.body.add(memExpr);
            }
            return ope;
        }

        if (op == UnaryOp.POS) {
            return ope;
        }

        if (op == UnaryOp.NEG) {
            IR.UnaryExpr tmp = new IR.UnaryExpr();
            tmp.dest = reg;
            tmp.ope = ope;
            tmp.op = IR.UnaryOp.MINUS;
            translate.body.add(tmp);
            return reg;
        }

        if (op == UnaryOp.TILDE) {
            IR.UnaryExpr tmp = new IR.UnaryExpr();
            tmp.dest = reg;
            tmp.ope = ope;
            tmp.op = IR.UnaryOp.TILDE;
            translate.body.add(tmp);
            return reg;
        }

        if (op == UnaryOp.NOT) {
            IR.BinaryExpr tmp = new IR.BinaryExpr();
            tmp.dest = reg;
            tmp.ope1 = ope;
            tmp.ope2 = new IntConst(1);
            tmp.op = IR.BinaryOp.XOR;
            translate.body.add(tmp);
            return reg;
        }

        return reg;
    }

}
