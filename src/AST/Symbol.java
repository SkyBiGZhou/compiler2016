package AST;

import IR.Address;
import IR.Function;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/1.
 */
public class Symbol extends Expression {
    public String id;

    public Symbol(String text) {
        this.id = text;
    }

    private static Dictionary<String, Symbol> dict = new Hashtable();

    public static Symbol get(String text) {
        String unique = text.intern();
        Symbol cnt = dict.get(unique);
        if (cnt == null) {
            cnt = new Symbol(unique);
            dict.put(unique, cnt);
        }
        return cnt;
    }

    public String toString() {
        return id;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print(this.toString() + "\n");
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        if (table.get(this) == null) {
            outputError(this);
            return false;
        }
        AST value = table.get(this);
        if (value instanceof ClassDecl) {
            this.getType = new ClassType(((ClassDecl) value).id);
            this.lvalue = false;
        }
        if (value instanceof FunctionDecl) {
            this.getType = ((FunctionDecl) value).type;
            this.lvalue = false;
        }
        if (value instanceof VarDecl) {
            this.getType = ((VarDecl) value).type;
            this.lvalue = true;
        }
        return true;
    }


    @Override
    public Address InReg(FunctionDecl func, Function translate,
                         Table table, int scope, Stack<AST> loopScope) {
        return table.get(this).reg;
    }

    @Override
    public Address ForAddress(FunctionDecl func, Function translate,
                             Table table, int scope, Stack<AST> loopScope) {
        return table.get(this).reg;
    }

}
