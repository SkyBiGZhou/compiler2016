package AST;

import IR.Function;

import java.util.Stack;

public abstract class Statement extends AST{
    public void IRtranslate(FunctionDecl func, Function translate,
                            Table table, int scope, Stack<AST> loopScope) {}
}
