package AST;

import IR.Branch;
import IR.Function;
import IR.Goto;
import IR.Label;
import com.sun.javafx.fxml.expression.UnaryExpression;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/1.
 */
public class ForStmt extends Statement {
    public Expression init;
    public Expression boundary;
    public Expression step;
    public Statement operation;
    public Label labelForCon;
    public Label labelForBre;

    public ForStmt() {
        init = null;
        boundary = null;
        step = null;
        operation = null;
    }

    public ForStmt(Expression init, Expression boundary,
                   Expression step, Statement operation) {
        this.init = init;
        this.boundary = boundary;
        this.step = step;
        this.operation = operation;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("ForStmt\n");
        if (init != null) init.print(d+1);
        if (boundary != null) boundary.print(d+1);
        if (step != null) step.print(d+1);
        if (operation != null) operation.print(d+1);
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        loopScope.push(this);
        if (init != null)
            if (!init.thirdTraverse(table, scope, loopScope, func))
            {
                //outputError(this);
                return false;
            }
        if (boundary != null) {
            if (!boundary.thirdTraverse(table, scope, loopScope, func)) {
                //outputError(this);
                return false;
            }
            if (!(boundary.getType instanceof BoolType))
                return false;
        }
        if (step != null)
            if (!step.thirdTraverse(table, scope, loopScope, func))
            {
                //outputError(this);
                return false;
            }
        if (operation != null) {
            table.beginScope();
            ++scope;
            if (!operation.thirdTraverse(table, scope, loopScope, func))
            {
                //outputError(this);
                return false;
            }
            --scope;
            table.endScope();
        }
        loopScope.pop();
        return true;
    }

    public void IRtranslate(FunctionDecl func, Function translate,
                            Table table, int scope, Stack<AST> loopScope) {
        loopScope.push(this);
        if (init != null) {
            init.IRtranslate(func, translate, table, scope, loopScope);
        }
        Label labelForBoundary = new Label();
        Label labelForTrue = new Label();
        labelForCon = new Label();
        labelForBre = new Label();
        Goto gotoNext1 = new Goto();
        gotoNext1.label = labelForBoundary;
        translate.body.add(gotoNext1);
        translate.body.add(labelForBoundary);
        if (boundary != null) {
            Branch br = new Branch();
            br.ope = boundary.InReg(func, translate, table, scope, loopScope);
            br.labelForTrue = labelForTrue;
            br.labelForFalse = labelForBre;
            translate.body.add(br);
        }
        translate.body.add(labelForTrue);
        table.beginScope();
        ++scope;
        if (operation != null) {
            operation.IRtranslate(func, translate, table, scope, loopScope);
        }
        --scope;
        table.endScope();
        Goto gotoCon = new Goto();
        gotoCon.label = labelForCon;
        translate.body.add(gotoCon);
        translate.body.add(labelForCon);
        if (step != null) {
            if (step instanceof Increment) {
                Expression pro = ((Increment) step).expr;
                UnaryExpr renew = new UnaryExpr(UnaryOp.INCREASE, pro);
                renew.InReg(func, translate, table, scope, loopScope);
            } else step.IRtranslate(func, translate, table, scope, loopScope);
        }
        Goto loop = new Goto(labelForBoundary);
        translate.body.add(loop);
        translate.body.add(labelForBre);
        loopScope.pop();
    }
}
