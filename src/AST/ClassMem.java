package AST;

import IR.*;

import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/5.
 */
public class ClassMem extends Expression {
    public Expression classname;
    public Symbol id;

    public ClassMem() {
        classname = null;
        id = null;
    }

    public ClassMem(Expression classname, Symbol id) {
        this.classname = classname;
        this.id = id;
    }

    @Override
    public void print(int d) {
        indent(d);
        System.out.print("ClassMem\n");
        classname.print(d+1);
        id.print(d+1);
    }

    @Override
    public boolean thirdTraverse(Table table, int scope, Stack<AST> loopScope, FunctionDecl func) {
        if (!(classname.thirdTraverse(table, scope, loopScope, func)))
        {
            //outputError(this);
            return false;
        }

        if (!(classname.getType instanceof ClassType)) {
            outputError(this);
            return false;
        }
        //检查类以及ID是否在参数列表中出现

        if (table.get(((ClassType) classname.getType).id) == null) {
            outputError(this);
            return false;
        }
        ClassDecl name = (ClassDecl) table.get(((ClassType) classname.getType).id);
        for (VarDecl parameter : name.decls) {
            if (parameter.id.toString().equals(id.toString())) {
                this.getType = parameter.type;
                this.lvalue = true;
                return true;
            }
        }
        outputError(this);
        return false;
    }

    @Override
    public Address InReg(FunctionDecl func, Function translate,
                         Table table, int scope, Stack<AST> loopScope) {
        Temp reg = new Temp();
        MemoryExpr tmp = new MemoryExpr();
        tmp.dest = reg;
        tmp.op = MemoryOp.LOAD;
        tmp.ope = classname.InReg(func, translate, table, scope, loopScope);
        //tmp.offset = id.InReg(func, translate, table, scope, loopScope);
        ClassDecl classDecl = (ClassDecl) table.get(((ClassType) classname.getType).id);
        int size = 0;
        for (VarDecl vars : classDecl.decls) {
            if (this.id.toString().equals(vars.id.toString())) break;
            size++;
        }
        tmp.offset = new IntConst(size * 4);
        translate.body.add(tmp);
        return reg;
    }

    @Override
    public Address ForAddress(FunctionDecl func, Function translate,
                              Table table, int scope, Stack<AST> loopScope) {
        Memory mem = new Memory();
        mem.pointer = classname.InReg(func, translate, table, scope, loopScope);
        //mem.offset = ;
        ClassDecl classDecl = (ClassDecl) table.get(((ClassType) classname.getType).id);
        int size = 0;
        for (VarDecl vars : classDecl.decls) {
            if (this.id.toString().equals(vars.id.toString())) break;
            size++;
        }
        mem.offset = new IntConst(size * 4);
        return mem;
    }

}
