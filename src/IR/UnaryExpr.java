package IR;

/**
 * Created by zhouyuhao on 2016/4/29.
 */
public class UnaryExpr extends Quadruple {
    public Address dest;
    public UnaryOp op;
    public Address ope;

    public UnaryExpr() {
        dest = null;
        op = null;
        ope = null;
    }

    public UnaryExpr(Address dest, UnaryOp op, Address ope) {
        this.dest = dest;
        this.op = op;
        this.ope = ope;
    }

    @Override
    public void print() {
        dest.print();
        System.out.print(" = ");
        switch (op) {
            case MINUS: System.out.print("neg"); break;
            case TILDE: System.out.print("not"); break;
        }
        System.out.print(" ");
        ope.print();
        System.out.print("\n");
    }

    @Override
    public String toString() {
        String str = dest.toString() + " = ";
        switch (op) {
            case MINUS: str = str + "neg"; break;
            case TILDE: str = str + "not"; break;
        }
        str = str + " " + ope.toString() + "\n";
        return str;
    }

    @Override
    public String MIPSForCISC() {
        String str = "";
        // $t4 -> 1, $t5 -> 4
        if (ope instanceof IntConst && ((IntConst) ope).value == 1) {
            switch (op) {
                case MINUS:
                    str = str + "neg $t0, $t4\n";
                    break;
                case TILDE:
                    str = str + "not $t0, $t4\n";
                    break;
            }
            str = str + "sw $t0, " + ((Temp) dest).num * 4 + "($sp)\n";

        } else if (ope instanceof IntConst && ((IntConst) ope).value == 4) {
            switch (op) {
                case MINUS:
                    str = str + "neg $t0, $t5\n";
                    break;
                case TILDE:
                    str = str + "not $t0, $t5\n";
                    break;
            }
            str = str + "sw $t0, " + ((Temp) dest).num * 4 + "($sp)\n";

        } else if (ope instanceof IntConst && ((IntConst) ope).value == 2) {

            switch (op) {
                case MINUS:
                    str = str + "neg $t0, $t6\n";
                    break;
                case TILDE:
                    str = str + "not $t0, $t6\n";
                    break;
            }
            str = str + "sw $t0, " + ((Temp) dest).num * 4 + "($sp)\n";

        } else {
            if (ope instanceof IntConst) {
                str = str + "li $t1, " + ((IntConst) ope).value + "\n";
            } else if (ope instanceof Temp) {
                str = str + "lw $t1, " + ((Temp) ope).num * 4 + "($sp)\n";
            }
            switch (op) {
                case MINUS:
                    str = str + "neg $t0, $t1\n";
                    break;
                case TILDE:
                    str = str + "not $t0, $t1\n";
                    break;
            }
            str = str + "sw $t0, " + ((Temp) dest).num * 4 + "($sp)\n";
        }
        return str;
    }

}
