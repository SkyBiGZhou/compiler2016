package IR;

/**
 * Created by zhouyuhao on 2016/4/29.
 */
public class Temp extends Address {
    public static int tempCount = 0;

    public int num;
    public String name;

    public static void setCount() {
        tempCount = 0;
    }
    public Temp() {
        name = null;
        num = tempCount++;
    }

    @Override
    public void print() {
        System.out.print("$" + num);
    }

    @Override
    public String toString() {
        return "$Reg" + num;
    }

    @Override
    public String MIPSForCISC() {
        return name;
    }
}
