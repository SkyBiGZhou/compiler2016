package IR;

/**
 * Created by zhouyuhao on 2016/4/29.
 * It might be replaced by RelationExpr & Branch
 */
public class IfFalseGoto extends Quadruple {
    public Address ope1;
    public RelationOp op;
    public Address ope2;
    public Label label;

    public IfFalseGoto() {
        ope1 = null;
        op = null;
        ope2 = null;
        label = null;
    }

    public IfFalseGoto(Address ope1, RelationOp op,
                       Address ope2, Label label) {
        this.ope1 = ope1;
        this.op = op;
        this.ope2 = ope2;
        this.label = label;
    }
}
