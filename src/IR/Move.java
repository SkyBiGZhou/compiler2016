package IR;

/**
 * Created by zhouyuhao on 2016/4/29.
 */
public class Move extends Quadruple {
    public Address dest;
    public Address ope;

    public Move() {
        dest = null;
        ope = null;
    }

    public Move(Address dest, Address ope) {
        this.dest = dest;
        this.ope = ope;
    }

    @Override
    public void print() {
        dest.print();
        System.out.print(" = move ");
        ope.print();
        System.out.print("\n");
    }

    @Override
    public String toString() {
        return dest.toString() + " = move " +
                ope.toString() + "\n";
    }

    @Override
    public String MIPSForCISC() {
        String str = "";
        // $t4 -> 1, $t5 -> 4
        if (ope instanceof IntConst && ((IntConst) ope).value == 1) {
            if (((Temp) dest).name == null) {
                str = str + "sw $t4, " + ((Temp) dest).num * 4 + "($sp)\n";
            } else {
                str = str + "sw $t4, var" + ((Temp) dest).name + "\n";
            }
        } else if (ope instanceof IntConst && ((IntConst) ope).value == 4) {
            if (((Temp) dest).name == null) {
                str = str + "sw $t5, " + ((Temp) dest).num * 4 + "($sp)\n";
            } else {
                str = str + "sw $t5, var" + ((Temp) dest).name + "\n";
            }
        } else if (ope instanceof IntConst && ((IntConst) ope).value == 2) {
            if (((Temp) dest).name == null) {
                str = str + "sw $t6, " + ((Temp) dest).num * 4 + "($sp)\n";
            } else {
                str = str + "sw $t6, var" + ((Temp) dest).name + "\n";
            }
        } else {
            if (ope instanceof IntConst) {
                str = str + "li $t0, " + ((IntConst) ope).value + "\n";
            } else if (ope instanceof Temp) {
                if (((Temp) ope).name == null) {
                    str = str + "lw $t0, " + ((Temp) ope).num * 4 + "($sp)\n";
                } else {
                    str = str + "lw $t0, var" + ((Temp) ope).name + "\n";
                }
            } else if (ope instanceof StringConst) {
                str = str + "la $t0, str" + ((StringConst) ope).num + "\n";
            }
            if (((Temp) dest).name == null) {
                str = str + "sw $t0, " + ((Temp) dest).num * 4 + "($sp)\n";
            } else {
                str = str + "sw $t0, var" + ((Temp) dest).name + "\n";
            }
        }
        return str;
    }
}
