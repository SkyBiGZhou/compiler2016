package IR;

/**
 * Created by zhouyuhao on 2016/4/29.
 */
public class BinaryExpr extends Quadruple {
    public Address dest;
    public BinaryOp op;
    public Address ope1;
    public Address ope2;

    public BinaryExpr() {
        dest = null;
        op = null;
        ope1 = null;
        ope2 = null;
    }

    public BinaryExpr(Address dest, BinaryOp op,
                      Address ope1, Address ope2) {
        this.dest = dest;
        this.op = op;
        this.ope1 = ope1;
        this.ope2 = ope2;
    }

    @Override
    public void print() {
        dest.print();
        System.out.print(" = ");
        switch (op) {
            case ADD: System.out.print("add"); break;
            case SUB: System.out.print("sub"); break;
            case MUL: System.out.print("mul"); break;
            case DIV: System.out.print("div"); break;
            case MOD: System.out.print("rem"); break;
            case SHL: System.out.print("shl"); break;
            case SHR: System.out.print("shr"); break;
            case AND: System.out.print("and"); break;
            case OR: System.out.print("or"); break;
            case XOR: System.out.print("xor"); break;
        }
        System.out.print(" ");
        ope1.print();
        System.out.print(" ");
        ope2.print();
        System.out.print("\n");
    }

    @Override
    public String toString() {
        String str = dest.toString() + " = ";
        switch (op) {
            case ADD: str = str + "add"; break;
            case SUB: str = str + "sub"; break;
            case MUL: str = str + "mul"; break;
            case DIV: str = str + "div"; break;
            case MOD: str = str + "rem"; break;
            case SHL: str = str + "shl"; break;
            case SHR: str = str + "shr"; break;
            case AND: str = str + "and"; break;
            case OR: str = str + "or"; break;
            case XOR: str = str + "xor"; break;
        }
        str = str + " " + ope1.toString() + " " + ope2.toString() + "\n";
        return str;
    }

    @Override
    public String MIPSForCISC() {
        String str = "";
        //$t4 -> 1, $t5 -> 4, $t6 -> 2
        if (ope2 instanceof IntConst && ((IntConst) ope2).value == 1) {
            if (ope1 instanceof IntConst) {
                str = str + "li $t1, " + ((IntConst) ope1).value + "\n";
            } else if (ope1 instanceof Temp) {
                if (((Temp) ope1).name == null) {
                    str = str + "lw $t1, " + ((Temp) ope1).num * 4 + "($sp)\n";
                } else {
                    str = str + "lw $t1, var" + ((Temp) ope1).name + "\n";
                }
            }
            switch (op) {
                case ADD:
                    str = str + "add $t0, $t1, $t4\n";
                    break;
                case SUB:
                    str = str + "sub $t0, $t1, $t4\n";
                    break;
                case MUL:
                    str = str + "mul $t0, $t1, $t4\n";
                    break;
                case DIV:
                    str = str + "div $t0, $t1, $t4\n";
                    break;
                case MOD:
                    str = str + "rem $t0, $t1, $t4\n";
                    break;
                case SHL:
                    str = str + "sll $t0, $t1, $t4\n";
                    break;
                case SHR:
                    str = str + "srl $t0, $t1, $t4\n";
                    break;
                case AND:
                    str = str + "and $t0, $t1, $t4\n";
                    break;
                case OR:
                    str = str + "or $t0, $t1, $t4\n";
                    break;
                case XOR:
                    str = str + "xor $t0, $t1, $t4\n";
                    break;
            }
            if (((Temp) dest).name == null) {
                str = str + "sw $t0, " + ((Temp) dest).num * 4 + "($sp)\n";
            } else {
                str = str + "sw $t0, var" + ((Temp) dest).name + "\n";
            }

        } else if (ope2 instanceof IntConst
                && ((IntConst) ope2).value == 4) {
            if (ope1 instanceof IntConst) {
                str = str + "li $t1, " + ((IntConst) ope1).value + "\n";
            } else if (ope1 instanceof Temp) {
                if (((Temp) ope1).name == null) {
                    str = str + "lw $t1, " + ((Temp) ope1).num * 4 + "($sp)\n";
                } else {
                    str = str + "lw $t1, var" + ((Temp) ope1).name + "\n";
                }
            }
            switch (op) {
                case ADD:
                    str = str + "add $t0, $t1, $t5\n";
                    break;
                case SUB:
                    str = str + "sub $t0, $t1, $t5\n";
                    break;
                case MUL:
                    str = str + "mul $t0, $t1, $t5\n";
                    break;
                case DIV:
                    str = str + "div $t0, $t1, $t5\n";
                    break;
                case MOD:
                    str = str + "rem $t0, $t1, $t5\n";
                    break;
                case SHL:
                    str = str + "sll $t0, $t1, $t5\n";
                    break;
                case SHR:
                    str = str + "srl $t0, $t1, $t5\n";
                    break;
                case AND:
                    str = str + "and $t0, $t1, $t5\n";
                    break;
                case OR:
                    str = str + "or $t0, $t1, $t5\n";
                    break;
                case XOR:
                    str = str + "xor $t0, $t1, $t5\n";
                    break;
            }
            if (((Temp) dest).name == null) {
                str = str + "sw $t0, " + ((Temp) dest).num * 4 + "($sp)\n";
            } else {
                str = str + "sw $t0, var" + ((Temp) dest).name + "\n";
            }

        } else if (ope2 instanceof IntConst
                && ((IntConst) ope2).value == 2) {

            if (ope1 instanceof IntConst) {
                str = str + "li $t1, " + ((IntConst) ope1).value + "\n";
            } else if (ope1 instanceof Temp) {
                if (((Temp) ope1).name == null) {
                    str = str + "lw $t1, " + ((Temp) ope1).num * 4 + "($sp)\n";
                } else {
                    str = str + "lw $t1, var" + ((Temp) ope1).name + "\n";
                }
            }
            switch (op) {
                case ADD:
                    str = str + "add $t0, $t1, $t6\n";
                    break;
                case SUB:
                    str = str + "sub $t0, $t1, $t6\n";
                    break;
                case MUL:
                    str = str + "mul $t0, $t1, $t6\n";
                    break;
                case DIV:
                    str = str + "div $t0, $t1, $t6\n";
                    break;
                case MOD:
                    str = str + "rem $t0, $t1, $t6\n";
                    break;
                case SHL:
                    str = str + "sll $t0, $t1, $t6\n";
                    break;
                case SHR:
                    str = str + "srl $t0, $t1, $t6\n";
                    break;
                case AND:
                    str = str + "and $t0, $t1, $t6\n";
                    break;
                case OR:
                    str = str + "or $t0, $t1, $t6\n";
                    break;
                case XOR:
                    str = str + "xor $t0, $t1, $t6\n";
                    break;
            }
            if (((Temp) dest).name == null) {
                str = str + "sw $t0, " + ((Temp) dest).num * 4 + "($sp)\n";
            } else {
                str = str + "sw $t0, var" + ((Temp) dest).name + "\n";
            }

        } else {

            if (ope1 instanceof IntConst) {
                str = str + "li $t1, " + ((IntConst) ope1).value + "\n";
            } else if (ope1 instanceof Temp) {
                if (((Temp) ope1).name == null) {
                    str = str + "lw $t1, " + ((Temp) ope1).num * 4 + "($sp)\n";
                } else {
                    str = str + "lw $t1, var" + ((Temp) ope1).name + "\n";
                }
            }
            if (ope2 instanceof IntConst) {
                str = str + "li $t2, " + ((IntConst) ope2).value + "\n";
            } else if (ope2 instanceof Temp) {
                if (((Temp) ope2).name == null) {
                    str = str + "lw $t2, " + ((Temp) ope2).num * 4 + "($sp)\n";
                } else {
                    str = str + "lw $t2, var" + ((Temp) ope2).name + "\n";
                }
            }
            switch (op) {
                case ADD:
                    str = str + "add $t0, $t1, $t2\n";
                    break;
                case SUB:
                    str = str + "sub $t0, $t1, $t2\n";
                    break;
                case MUL:
                    str = str + "mul $t0, $t1, $t2\n";
                    break;
                case DIV:
                    str = str + "div $t0, $t1, $t2\n";
                    break;
                case MOD:
                    str = str + "rem $t0, $t1, $t2\n";
                    break;
                case SHL:
                    str = str + "sll $t0, $t1, $t2\n";
                    break;
                case SHR:
                    str = str + "srl $t0, $t1, $t2\n";
                    break;
                case AND:
                    str = str + "and $t0, $t1, $t2\n";
                    break;
                case OR:
                    str = str + "or $t0, $t1, $t2\n";
                    break;
                case XOR:
                    str = str + "xor $t0, $t1, $t2\n";
                    break;
            }
            if (((Temp) dest).name == null) {
                str = str + "sw $t0, " + ((Temp) dest).num * 4 + "($sp)\n";
            } else {
                str = str + "sw $t0, var" + ((Temp) dest).name + "\n";
            }
        }
        return str;
    }
}
