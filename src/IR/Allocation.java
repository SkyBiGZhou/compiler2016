package IR;

/**
 * Created by zhouyuhao on 2016/5/2.
 */
public class Allocation extends Quadruple {
    public Address pointer;
    public Address size;

    public Allocation() {
        pointer = null;
        size = null;
    }

    public Allocation(Address pointer, Address size) {
        this.pointer = pointer;
        this.size = size;
    }

    @Override
    public void print() {
        pointer.print();
        System.out.print(" = alloc ");
        size.print();
        System.out.print("\n");
    }

    @Override
    public String toString() {
        return pointer.toString() + " = alloc " + size.toString() + "\n";
    }

    @Override
    public String MIPSForCISC() {
        String str = "";
        //system call $v0 = 9(sbrk), $a0 = size, return $v0
        if (size instanceof IntConst) {
            str = str + "li $a0, " + ((IntConst) size).value + "\n";
        } else {
            str = str + "lw $a0, " + ((Temp) size).num * 4 + "($sp)\n";
        }
        str = str + "li $v0, 9\n"
                + "syscall\n";
        str = str + "sw $v0, " + ((Temp)pointer).num * 4 + "($sp)\n";
        return str;
    }
}
