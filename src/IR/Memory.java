package IR;

/**
 * Created by zhouyuhao on 2016/4/30.
 */
public class Memory extends Address {

    public Address pointer;
    public Address offset;

    public Memory() {
        pointer = null;
        offset = null;
    }

    public Memory(Address pointer, Address offset) {
        this.pointer = pointer;
        this.offset = offset;
    }

    @Override
    public void print() {
        pointer.print();
        System.out.print(" ");
        offset.print();
    }

    @Override
    public String toString() {
        return pointer.toString() + " " + offset.toString();
    }

}
