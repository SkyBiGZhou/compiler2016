package IR;

/**
 * Created by zhouyuhao on 2016/4/29.
 */
public class MemoryExpr extends Quadruple {
    public Address dest;
    public MemoryOp op;
    public Address ope;
    public Address offset;

    public MemoryExpr() {
        dest = null;
        op = null;
        ope = null;
        offset = null;
    }

    public MemoryExpr(Address dest, MemoryOp op, Address ope, Address offset) {
        this.dest = dest;
        this.op = op;
        this.ope = ope;
        this.offset = offset;
    }

    @Override
    public void print() {
        if (op == MemoryOp.STORE) {
            System.out.print("store ");
            dest.print();
            System.out.print(" ");
            ope.print();
            System.out.print(" ");
            offset.print();
            System.out.print("\n");
        } else {
            dest.print();
            System.out.print(" = load ");
            ope.print();
            System.out.print(" ");
            offset.print();
            System.out.print("\n");
        }
        return;
    }

    @Override
    public String toString() {
        if (op == MemoryOp.STORE) {
            return "store 4 " + dest.toString() + " " +
                    ope.toString() + " " + offset.toString() + "\n";
        } else {
            return dest.toString() + " = load 4 " +
                    ope.toString() + " " + offset.toString() + "\n";
        }
    }

    @Override
    public String MIPSForCISC() {
        String str = "";
        //offset must be IntConst
        if (op == MemoryOp.STORE) {
            if (ope instanceof Temp) {
                if (((Temp) ope).name == null) {
                    str = str + "lw $t0, "
                            + ((Temp) ope).num * 4
                            + "($sp)\n";
                } else {
                    str = str + "lw $t0, var" + ((Temp) ope).name + "\n";
                }
            } else if (ope instanceof IntConst) {
                str = str + "li $t0, "
                        + ((IntConst) ope).value + "\n";
            }
            if (dest instanceof Memory) {
                //System.out.print(((Memory) dest).pointer.toString());
                if (((Temp)((Memory) dest).pointer).name == null) {
                    str = str + "lw $t1, " + (((Temp) ((Memory) dest).pointer).num * 4) + "($sp)\n";
                    str = str + "sw $t0, " + (((IntConst) offset).value + ((IntConst)((Memory) dest).offset).value) + "($t1)\n";
                } else {
                    str = str + "la $t1, var" + ((Temp) ((Memory) dest).pointer).name + "\n";
                    str = str + "lw $t0, " + (((IntConst) offset).value + ((IntConst)((Memory) dest).offset).value) + "($t1)\n";
                }
            } else {
                if (((Temp) dest).name == null) {
                    str = str + "lw $t1, "
                            + (((Temp) dest).num * 4)
                            + "($sp)\n";
                } else {
                    str = str + "lw $t1, var" + ((Temp) dest).name + "\n";
                }
                str = str + "sw $t0, " + ((IntConst) offset).value + "($t1)\n";
            }

        } else if (op == MemoryOp.LOAD) {
            if (((Temp) ope).name == null) {
                str = str + "lw $t0, "
                        + (((Temp) ope).num * 4)
                        + "($sp)\n";
            } else {
                str = str + "lw $t0, var" + ((Temp) ope).name + "\n";
            }
            str = str + "lw $t1, " + ((IntConst) offset).value +"($t0)\n";

            if (((Temp) dest).name == null) {
                str = str + "sw $t1, "
                        + ((Temp) dest).num * 4
                        + "($sp)\n";
            } else {
                str = str + "sw $t1, var" + ((Temp) ope).name + "\n";
            }

        }
        return str;
    }
}
