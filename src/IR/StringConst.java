package IR;

/**
 * Created by zhouyuhao on 2016/4/29.
 */
public class StringConst extends Const {
    public String str;
    public static int stringNum = 0;
    public int num;
    public int length;
    //public int size;

    public StringConst() {
        str = null;
        num = stringNum++;
        //size = 0;
        length = 0;
    }

    public StringConst(String str) {
        this.str = str;
        this.num = stringNum++;
        //size = 0;
        this.length = 0;
    }

    public int CalLength() {
        for (int i = 0; i < str.length(); ++i) {
            if (str.charAt(i) == '\\') ++i;
            length++;
        }
        length = length -2;
        return length;
    }

    @Override
    public void print() {
        System.out.print(str);
    }

    @Override
    public String toString() {
        return str;
    }
}
