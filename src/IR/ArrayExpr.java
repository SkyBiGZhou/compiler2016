package IR;

/**
 * Created by zhouyuhao on 2016/4/29.
 * It might be replaced by MemoryExpr
 */
public class ArrayExpr extends Quadruple {
    public Address dest;
    public MemoryOp op;
    public Address ope;
    public Address offset;

    public ArrayExpr() {
        dest = null;
        op = null;
        ope = null;
        offset = null;
    }

    public ArrayExpr(Address dest, MemoryOp op, Address ope, Address offset) {
        this.dest = dest;
        this.op = op;
        this.ope = ope;
        this.offset = offset;
    }

    @Override
    public void print() {
        if (op == MemoryOp.STORE) {
            System.out.print("store ");
            dest.print();
            System.out.print(" ");
            ope.print();
            System.out.print(" ");
            offset.print();
            System.out.print("\n");
        } else {
            dest.print();
            System.out.print(" = load ");
            ope.print();
            System.out.print(" ");
            offset.print();
            System.out.print("\n");
        }
        return;
    }

    @Override
    public String toString() {
        if (op == MemoryOp.STORE) {
            return "store " + dest.toString() + " " +
                    ope.toString() + " " + offset.toString() + "\n";
        } else {
            return dest.toString() + " = load " +
                    ope.toString() + " " + offset.toString() + "\n";
        }
    }
}
