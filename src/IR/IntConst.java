package IR;

/**
 * Created by zhouyuhao on 2016/4/29.
 */
public class IntConst extends Const {
    public int value;

    public IntConst() {
        value = 0;
    }

    public IntConst(int value) {
        this.value = value;
    }

    @Override
    public void print() {
        System.out.print(value);
    }

    @Override
    public String toString() {
        return "" + value;
    }

}
