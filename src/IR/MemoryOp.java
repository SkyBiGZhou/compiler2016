package IR;

/**
 * Created by zhouyuhao on 2016/4/29.
 */
public enum MemoryOp {
    LOAD, STORE
}
