package IR;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by zhouyuhao on 2016/4/29.
 */
public class Call extends Quadruple {
    public Address dest;
    public List<Address> param;
    public String name;
    //public int calleeSize;
    public Function callee;
    //public Function caller;
    //callee被调用者, caller调用者

    public Call() {
        dest = null;
        param = new LinkedList<Address>();
        name = null;
        callee = null;
        //calleeSize = 0;
    }

    public Call(Address dest, List<Address> param,
                String name) {
        this.dest = dest;
        this.param = param;
        this.name = name;
    }

    @Override
    public void print() {
        if (dest != null) {
            dest.print();
            System.out.print(" = ");
        }
        System.out.print("call " + name);
        for (Address var : param) {
            System.out.print(" ");
            var.print();
        }
        System.out.print("\n");
    }

    @Override
    public String toString() {
        if (dest != null) {
            String str = dest.toString() + " = call " + name;
            for (Address var : param) {
                if (var != null)
                    str = str + " " + var.toString();
            }
            str = str + "\n";
            return str;
        } else {
            String str = "call " + name;
            for (Address var : param) {
                str = str + " " + var.toString();
            }
            str = str + "\n";
            return str;
        }
    }

    @Override
    public String MIPSForCISC() {
        String str = "";
        int size;
        /*
        if (callee == null)
            System.out.print(name + "\n");
        */
        if (callee == null)
            size = 32;
        else
            size = callee.size + 32;


        //$sp 压栈
        str = str + "subu $sp, $sp, " + size * 4 + "\n";

        //parameter
        //callee.entry != null <-> callee is not embedded
        if (callee.entry != null) {
            for (int i = 0; i < param.size(); ++i) {
                int forCaller = ((Temp) param.get(i)).num;
                int forCallee = ((Temp) callee.vars.get(i)).num;
                if (((Temp) param.get(i)).name != null) {
                    ((Temp) callee.vars.get(i)).name = ((Temp) param.get(i)).name;
                    //System.out.print(((Temp) callee.vars.get(i)).name+"\n");
                    str = str + "lw $t0, var" + ((Temp) param.get(i)).name + "\n";
                    str = str + "sw $t0, " + forCallee * 4 + "($sp)\n";
                }
                if (((Temp) param.get(i)).name == null) {
                    str = str + "lw $t0, " + (forCaller + size) * 4 + "($sp)\n";
                    str = str + "sw $t0, " + forCallee * 4 + "($sp)\n";
                }
            }
        } else {
            for (int i = 0; i < param.size(); ++i) {
                int forCaller = ((Temp) param.get(i)).num;
                if (((Temp) param.get(i)).name == null) {
                    str = str + "lw $t0, " + (forCaller + size) * 4 + "($sp)\n";
                } else {
                    str = str + "lw $t0, var" + ((Temp) param.get(i)).name + "\n";
                }
                str = str + "move $a" + i + ", $t0" + "\n";
            }
        }

        //jal & ra
        //str = str + "sw $ra, " + (31 + size) * 4 + "($sp)\n";
        if (callee.entry != null) {
            str = str + "jal Label" + callee.entry.num + "\n";
        } else {
            str = str + "jal " + callee.name + "\n";
        }
        if (dest != null) {
            str = str + "sw $v0, " + (((Temp) dest).num + size) * 4 + "($sp)\n";
        }
        //str = str + "lw $ra, " + (31 + size) * 4 + "($sp)\n";

        //$sp 弹出
        str = str + "addu $sp, $sp, " + size * 4 + "\n";

        return str;
    }
}
