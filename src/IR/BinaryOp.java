package IR;

/**
 * Created by zhouyuhao on 2016/4/29.
 */
public enum  BinaryOp {
    ADD, SUB, MUL, DIV, MOD,
    SHL, SHR, AND, OR, XOR
}
