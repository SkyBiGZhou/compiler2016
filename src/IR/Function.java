package IR;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by zhouyuhao on 2016/4/29.
 */
public class Function {
    public String name;
    public Label entry, exit;
    public int size;
    public List<Address> vars;
    public List<Quadruple> body;

    public Function() {
        this.name = null;
        this.entry = null;
        this.exit = null;
        this.size = 32;
        this.vars = new LinkedList<Address>();
        this.body = new LinkedList<Quadruple>();
    }

    public Function(String name, List<Address> vars, List<Quadruple> body, Label entry, int size, Label exit) {
        this.name = name;
        this.size = size;
        this.entry = entry;
        this.exit = exit;
        this.vars = vars;
        this.body = body;
    }

    public void print() {
        System.out.print("func " + name);
        for (Address var : vars) {
            System.out.print(" ");
            var.print();
        }
        System.out.print(" { \n");
        for (Quadruple stmt : body) {
            stmt.print();
        }
        System.out.print("} \n");
        System.out.print("\n");
    }

    public String toString() {
        String str = "func " + name;
        for (Address var : vars) {
            str = str + " " + var.toString();
        }
        str = str + " { \n";
        str = str + "%" + name + ".entry:" + "\n";
        for (Quadruple stmt : body) {
            str = str + stmt.toString();
        }
        str = str + "} \n \n";
        return str;
    }

    public String MIPSForCISC() {
        String str = "";
        //Need a entry label!
        str = str + entry.MIPSForCISC();
        str += "sw $ra, " + (size + 31) * 4 + "($sp)\n";
        for (Quadruple stmt : body) {
            str = str + stmt.MIPSForCISC();
        }
        str = str + exit.MIPSForCISC();
        str += "lw $ra, " + (size + 31) * 4 + "($sp)\n";
        str = str + "jr $ra\n";
        return str;
    }

}
