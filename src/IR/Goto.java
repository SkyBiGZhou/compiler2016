package IR;

/**
 * Created by zhouyuhao on 2016/4/29.
 */
public class Goto extends Quadruple {
    public Label label;

    public Goto() {
        label = null;
    }

    public Goto(Label label) {
        this.label = label;
    }

    @Override
    public void print() {
        System.out.print("jump ");
        label.print();
        System.out.print("\n");
    }

    @Override
    public String toString() {
        return "jump " + "%Label" + label.num + "\n";
    }

    @Override
    public String MIPSForCISC() {
        String str = "";
        str = str + "j Label" + label.num + "\n";
        return str;
    }
}
