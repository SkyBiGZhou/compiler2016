package IR;

/**
 * Created by zhouyuhao on 2016/4/30.
 */
public class RelationExpr extends Quadruple {
    public Address dest;
    public RelationOp op;
    public Address ope1;
    public Address ope2;

    public RelationExpr() {
        dest = null;
        op = null;
        ope1 = null;
        ope2 = null;
    }

    public RelationExpr(Address dest, RelationOp op, Address ope1, Address ope2) {
        this.dest = dest;
        this.op = op;
        this.ope1 = ope1;
        this.ope2 = ope2;
    }

    @Override
    public void print() {
        dest.print();
        System.out.print(" = ");
        switch (op) {
            case EQ: System.out.print("seq"); break;
            case NE: System.out.print("sne"); break;
            case LE: System.out.print("sle"); break;
            case LT: System.out.print("slt"); break;
            case GE: System.out.print("sge"); break;
            case GT: System.out.print("sgt"); break;
        }
        System.out.print(" ");
        ope1.print();
        System.out.print(" ");
        ope2.print();
        System.out.print("\n");
    }

    @Override
    public String toString() {
        String str = dest.toString() + " = ";
        switch (op) {
            case EQ: str = str + "seq"; break;
            case NE: str = str + "sne"; break;
            case LE: str = str + "sle"; break;
            case LT: str = str + "slt"; break;
            case GE: str = str + "sge"; break;
            case GT: str = str + "sgt"; break;
        }
        str = str + " " + ope1.toString() + " "
                + ope2.toString() + "\n";
        return str;
    }

    @Override
    public String MIPSForCISC() {
        String str = "";
        // $t4 -> 1, $t5 -> 4
        if (ope2 instanceof IntConst && ((IntConst) ope2).value == 1) {
            if (ope1 instanceof IntConst) {
                str = str + "li $t1, " + ((IntConst) ope1).value + "\n";
            } else if (ope1 instanceof Temp) {
                if (((Temp) ope1).name == null) {
                    str = str + "lw $t1, " + ((Temp) ope1).num * 4 + "($sp)\n";
                } else {
                    str = str + "lw $t1, var" + ((Temp) ope1).name + "\n";
                }
            }
            switch (op) {
                case EQ:
                    str = str + "seq $t0, $t1, $t4\n";
                    break;
                case NE:
                    str = str + "sne $t0, $t1, $t4\n";
                    break;
                case LE:
                    str = str + "sle $t0, $t1, $t4\n";
                    break;
                case LT:
                    str = str + "slt $t0, $t1, $t4\n";
                    break;
                case GE:
                    str = str + "sge $t0, $t1, $t4\n";
                    break;
                case GT:
                    str = str + "sgt $t0, $t1, $t4\n";
                    break;
            }
            str = str + "sw $t0, " + ((Temp) dest).num * 4 + "($sp)\n";
            return str;

        } else if (ope2 instanceof IntConst
                && ((IntConst) ope2).value == 4) {

            if (ope1 instanceof IntConst) {
                str = str + "li $t1, " + ((IntConst) ope1).value + "\n";
            } else if (ope1 instanceof Temp) {
                if (((Temp) ope1).name == null) {
                    str = str + "lw $t1, " + ((Temp) ope1).num * 4 + "($sp)\n";
                } else {
                    str = str + "lw $t1, var" + ((Temp) ope1).name + "\n";
                }
            }
            switch (op) {
                case EQ:
                    str = str + "seq $t0, $t1, $t5\n";
                    break;
                case NE:
                    str = str + "sne $t0, $t1, $t5\n";
                    break;
                case LE:
                    str = str + "sle $t0, $t1, $t5\n";
                    break;
                case LT:
                    str = str + "slt $t0, $t1, $t5\n";
                    break;
                case GE:
                    str = str + "sge $t0, $t1, $t5\n";
                    break;
                case GT:
                    str = str + "sgt $t0, $t1, $t5\n";
                    break;
            }
            str = str + "sw $t0, " + ((Temp) dest).num * 4 + "($sp)\n";
            return str;

        } else if (ope2 instanceof IntConst && ((IntConst) ope2).value == 2) {

            if (ope1 instanceof IntConst) {
                str = str + "li $t1, " + ((IntConst) ope1).value + "\n";
            } else if (ope1 instanceof Temp) {
                if (((Temp) ope1).name == null) {
                    str = str + "lw $t1, " + ((Temp) ope1).num * 4 + "($sp)\n";
                } else {
                    str = str + "lw $t1, var" + ((Temp) ope1).name + "\n";
                }
            }
            switch (op) {
                case EQ:
                    str = str + "seq $t0, $t1, $t6\n";
                    break;
                case NE:
                    str = str + "sne $t0, $t1, $t6\n";
                    break;
                case LE:
                    str = str + "sle $t0, $t1, $t6\n";
                    break;
                case LT:
                    str = str + "slt $t0, $t1, $t6\n";
                    break;
                case GE:
                    str = str + "sge $t0, $t1, $t6\n";
                    break;
                case GT:
                    str = str + "sgt $t0, $t1, $t6\n";
                    break;
            }
            str = str + "sw $t0, " + ((Temp) dest).num * 4 + "($sp)\n";
            return str;

        } else {
            if (ope1 instanceof IntConst) {
                str = str + "li $t1, " + ((IntConst) ope1).value + "\n";
            } else if (ope1 instanceof Temp) {
                if (((Temp) ope1).name == null) {
                    str = str + "lw $t1, " + ((Temp) ope1).num * 4 + "($sp)\n";
                } else {
                    str = str + "lw $t1, var" + ((Temp) ope1).name + "\n";
                }
            }
            if (ope2 instanceof IntConst) {
                str = str + "li $t2, " + ((IntConst) ope2).value + "\n";
            } else if (ope2 instanceof Temp) {
                if (((Temp) ope2).name == null) {
                    str = str + "lw $t2, " + ((Temp) ope2).num * 4 + "($sp)\n";
                } else {
                    str = str + "lw $t2, var" + ((Temp) ope2).name + "\n";
                }
            }
            switch (op) {
                case EQ:
                    str = str + "seq $t0, $t1, $t2\n";
                    break;
                case NE:
                    str = str + "sne $t0, $t1, $t2\n";
                    break;
                case LE:
                    str = str + "sle $t0, $t1, $t2\n";
                    break;
                case LT:
                    str = str + "slt $t0, $t1, $t2\n";
                    break;
                case GE:
                    str = str + "sge $t0, $t1, $t2\n";
                    break;
                case GT:
                    str = str + "sgt $t0, $t1, $t2\n";
                    break;
            }
            str = str + "sw $t0, " + ((Temp) dest).num * 4 + "($sp)\n";
            return str;
        }
    }
}
