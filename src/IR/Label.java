package IR;

/**
 * Created by zhouyuhao on 2016/4/29.
 */
public class Label extends Quadruple {
    private static int labelCount = 0;

    public int num;

    public Label() {
        num = labelCount++;
    }

    @Override
    public void print() {
        System.out.print("\n" + "%" + num + "\n");
    }

    @Override
    public String toString() {
        return "\n" + "%Label" + num + ":\n";
    }

    @Override
    public String MIPSForCISC() {
        String str = "";
        str = str + "Label" + num + ":\n";
        return str;
    }
}
