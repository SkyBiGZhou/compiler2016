package IR;

/**
 * Created by zhouyuhao on 2016/4/29.
 */
public class Return extends Quadruple {
    public Address value;
    public Label exit;

    public Return() {
        value = null;
        exit = null;
    }

    public Return(Address value) {
        this.value = value;
    }

    @Override
    public void print() {
        System.out.print("ret ");
        value.print();
        System.out.print("\n");
    }

    @Override
    public String toString() {
        return "ret " + value.toString() + "\n";
    }

    @Override
    public String MIPSForCISC() {
        String str = "";
        if (value instanceof IntConst) {
            str = str + "li $v0, " + ((IntConst) value).value + "\n";
        } else {
            if (((Temp) value).name == null) {
                str = str + "lw $v0, " + ((Temp) value).num * 4 + "($sp)\n";
            } else {
                str = str + "lw $v0, var" + ((Temp) value).name + "\n";
            }
        }
        /*
        if (exit == null) {
            System.out.print(value.toString());
        }*/

        str = str + "j Label" + exit.num + "\n";
        return str;
    }
}
