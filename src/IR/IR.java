package IR;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by zhouyuhao on 2016/4/28.
 */
public class IR {
    public List<Function> functionList;
    public static List<Temp> varList;
    public static List<Quadruple> initList;
    public static List<StringConst> stringList;

    public IR() {
        varList = new LinkedList<Temp>();
        functionList = new LinkedList<Function>();
        stringList = new LinkedList<StringConst>();
        initList = new LinkedList<Quadruple>();
    }

    public IR(List<Function> functionList, List<Temp> varList,
              List<StringConst> stringList) {
        this.functionList = functionList;
        this.varList = varList;
        //this.stringList = stringList;
    }

    public void print() {
        for (Function func : functionList) {
            func.print();
            System.out.print("\n");
        }
    }

    public String toString() {
        String str = "";
        for (Function func : functionList) {
            str = str + func.toString() + "\n";
        }
        return str;
    }

    public String MIPSForCISC() {
        String str = "";
        //var && string

        str = str + "\t.data\n";
        for (Temp reg: varList) {
            str = str + "var" + reg.MIPSForCISC() + ":\n\t.word 0\n";
        }

        for (StringConst string: stringList) {
            str = str + "str" + string.num + ":\n\t.word "
                    + string.CalLength()
                    + "\n\t.align 2\n" + "\t.asciiz " + string.str + "\n";
        }

        //For Function "println"
        str = str + "strnewline" + ":\n\t.asciiz \"\\n\"\n";

        str = str + "\t.text\n";
        for (Function function: functionList) {
            if (function.name == "main") {
                str = str + "main:\n";
                //CISC $t4 = IntConst(1), $t5 = IntConst(4), $t6 = IntConst(2);
                str = str + "li $t4, 1\n";
                str = str + "li $t5, 4\n";
                str = str + "li $t6, 2\n";

                str = str + "sw $ra, " + 31 * 4 + "($sp)\n";
                str = str + "subu $sp, $sp, " + 32 * 4 + "\n";
                //deal with global variable
                for (Quadruple var: initList) {
                    str = str + var.MIPSForCISC();
                }
                //function.size *= 10;
                // $sp 压栈
                str = str + "subu $sp, $sp, " + (32 + function.size) * 4 + "\n";

                //jal

                str = str + "jal Label" + function.entry.num + "\n";
                // $sp 弹回
                str = str + "addu $sp, $sp, " + (32 + function.size) * 4 + "\n";
                str = str + "addu $sp, $sp, " + 32 * 4 + "\n";
                str = str + "lw $ra, " + 31 * 4 + "($sp)\n";
                str = str + "jr $ra\n";

                str = str + function.MIPSForCISC();
            } else {
                str = str + function.MIPSForCISC();
            }
        }

        //EmbeddedFunctionCall
        //service 0(new)
        str = str + "new:\n" +
                "sw\t$ra, 124($sp)\n" +
                "li\t$v0, 9\n" +
                "syscall\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //size
        str = str + "size:\n" +
                "sw\t$ra, 124($sp)\n" +
                "lw\t$v0, 0($a0)\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 1(print)
        str = str + "print:\n" +
                "sw\t$ra, 124($sp)\n" +
                "li\t$v0, 4\n" +
                "addu\t$a0, $a0, 4\n" +
                "syscall\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //println
        str = str + "println:\n" +
                "sw\t$ra, 124($sp)\n" +
                "li\t$v0, 4\n" +
                "addu\t$a0, $a0, 4\n" +
                "syscall\n" +
                "la\t$a0, strnewline\n" +
                "syscall\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //printInt
        str = str + "printInt:\n" +
                "sw\t$ra, 124($sp)\n" +
                "li\t$v0, 1\n" +
                "syscall\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //printlnInt
        str = str + "printlnInt:\n" +
                "sw\t$ra, 124($sp)\n" +
                "li\t$v0, 1\n" +
                "syscall\n" +
                "li\t$v0, 4\n" +
                "la\t$a0, strnewline\n" +
                "syscall\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 2(getString)
        str = str + "getString:\n" +
                "sw\t$ra, 124($sp)\n" +
                "li\t$v0, 9\n" +
                "li\t$a0, 8192\n" +
                "syscall\n" +
                "addu\t$a0, $v0, 4\n" +
                "li\t$a1, 8188\n" +
                "li\t$v0, 8\n" +
                "syscall\n" +
                "subu\t$v0, $a0, 4\n" +
                "getString_label:\n" +
                "lb\t$a1, 0($a0)\n" +
                "addu\t$a0, $a0, 1\n" +
                "bnez\t$a1, getString_label\n" +
                "subu\t$a0, $a0, $v0\n" +
                "subu\t$a0, $a0, 5\n" +
                "sw\t$a0, 0($v0)\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 3
        str = str + "getInt:\n" +
                "sw\t$ra, 124($sp)\n" +
                "li\t$a1, 0\n" +
                "getInt_label1:\n" +
                "li\t$v0, 12\n" +
                "syscall\n" +
                "beq\t$v0, 45, getInt_label2\n" +
                "bge\t$v0, 48, getInt_label3\n" +
                "j\tgetInt_label1\n" +
                "getInt_label2:\n" +
                "li\t$v0, 12\n" +
                "syscall\n" +
                "li\t$a1, 1\n" +
                "getInt_label3:\n" +
                "sub\t$a0, $v0, 48\n" +
                "getInt_label6:\n" +
                "li\t$v0, 12\n" +
                "syscall\n" +
                "blt\t$v0, 48, getInt_label4\n" +
                "sub\t$v0, $v0, 48\n" +
                "mul\t$a0, $a0, 10\n" +
                "add\t$a0, $a0, $v0\n" +
                "j getInt_label6\n" +
                "getInt_label4:\n" +
                "move\t$v0, $a0\n" +
                "beq\t$a1, 0, getInt_label5\n" +
                "neg\t$v0, $v0\n" +
                "getInt_label5:\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 4
        str = str + "toString:\n" +
                "sw\t$ra, 124($sp)\n" +
                "move\t$a1, $a0\n" +
                "li\t$a2, 0\n" +
                "toString_label1:\n" +
                "div\t$a1, $a1, 10\n" +
                "addu\t$a2, $a2, 1\n" +
                "bnez\t$a1, toString_label1\n" +
                "move\t$a1, $a0\n" +
                "move\t$a0, $a2\n" +
                "addu\t$a0, $a0, 9\n" +
                "divu\t$a0, $a0, 4\n" +
                "mulou\t$a0, $a0, 4\n" +
                "li\t$v0, 9\n" +
                "syscall\n" +
                "bltz\t$a1, toString_label2\n" +
                "sw\t$a2, 0($v0)\n" +
                "addu\t$a0, $v0, 4\n" +
                "addu\t$a0, $a0, $a2\n" +
                "j\ttoString_label3\n" +
                "toString_label2:\n" +
                "abs\t$a1, $a1\n" +
                "addu\t$a2, $a2, 1\n" +
                "sw\t$a2, 0($v0)\n" +
                "addu\t$a0, $v0, 4\n" +
                "li\t$a3, 45\n" +
                "sb\t$a3, 0($a0)\n" +
                "addu\t$a0, $a0, $a2\n" +
                "toString_label3:\n" +
                "li\t$a2, 0\n" +
                "sb\t$a2, 0($a0)\n" +
                "toString_label4:\n" +
                "subu\t$a0, $a0, 1\n" +
                "rem\t$a3, $a1, 10\n" +
                "addu $a3, $a3, 48\n" +
                "sb\t$a3, 0($a0)\n" +
                "div\t$a1, $a1, 10\n" +
                "bnez\t$a1, toString_label4\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 5
        str = str + "substring:\n" +
                "sw\t$ra, 124($sp)\n" +
                "addu\t$a1, $a1, 4\n" +
                "addu\t$a1, $a1, $a0\n" +
                "addu\t$a2, $a2, 4\n" +
                "addu\t$a2, $a2, $a0\n" +
                "li\t$v0, 9\n" +
                "subu\t$a0, $a2, $a1\n" +
                "addu\t$a0, $a0, 9\n" +
                "divu\t$a0, $a0, 4\n" +
                "mulou\t$a0, $a0, 4\n" +
                "syscall\n" +
                "subu\t$a3, $a2, $a1\n" +
                "addu\t$a3, $a3, 1\n" +
                "sw\t$a3, 0($v0)\n" +
                "addu\t$a0, $v0, 4\n" +
                "substring_label1:\n" +
                "bgt\t$a1, $a2, substring_label2\n" +
                "lb\t$a3, 0($a1)\n" +
                "sb\t$a3, 0($a0)\n" +
                "addu\t$a1, $a1, 1\n" +
                "addu\t$a0, $a0, 1\n" +
                "j substring_label1\n" +
                "substring_label2:\n" +
                "li\t$a3, 0\n" +
                "sb\t$a3, 0($a0)\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 6
        str = str + "parseInt:\n" +
                "sw\t$ra, 124($sp)\n" +
                "li\t$a1, 0\n" +
                "addu\t$a2, $a0, 4\n" +
                "lb\t$a3, 0($a2)\n" +
                "bge\t$a3, 48, parseInt_label1\n" +
                "addu\t$a2, $a2, 1\n" +
                "lb\t$a3, 0($a2)\n" +
                "li\t$a1, 1\n" +
                "parseInt_label1:\n" +
                "sub\t$a0, $a3, 48\n" +
                "parseInt_label2:\n" +
                "addu\t$a2, $a2, 1\n" +
                "lb\t$a3, 0($a2)\n" +
                "blt\t$a3, 48, parseInt_label3\n" +
                "bgt\t$a3, 57, parseInt_label3\n" +
                "sub\t$a3, $a3, 48\n" +
                "mul\t$a0, $a0, 10\n" +
                "add\t$a0, $a0, $a3\n" +
                "j parseInt_label2\n" +
                "parseInt_label3:\n" +
                "move\t$v0, $a0\n" +
                "beq\t$a1, 0, parseInt_label4\n" +
                "neg\t$v0, $v0\n" +
                "parseInt_label4:\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 7
        str = str + "stringAdd:\n" +
                "sw\t$ra, 124($sp)\n" +
                "lw\t$a2, 0($a0)\n" +
                "lw\t$a3, 0($a1)\n" +
                "add\t$a3, $a2, $a3\n" +
                "move\t$a2, $a0\n" +
                "move\t$a0, $a3\n" +
                "add\t$a0, $a0, 8\n" +
                "div\t$a0, $a0, 4\n" +
                "mul\t$a0, $a0, 4\n" +
                "li\t$v0, 9\n" +
                "syscall\n" +
                "sw\t$a3, 0($v0)\n" +
                "addu\t$a0, $v0, 4\n" +
                "addu\t$a2, $a2, 4\n" +
                "stringAdd_label1:\n" +
                "lb\t$a3, 0($a2)\n" +
                "beqz\t$a3, stringAdd_label2\n" +
                "sb\t$a3, 0($a0)\n" +
                "addu\t$a2, $a2, 1\n" +
                "addu\t$a0, $a0, 1\n" +
                "j\tstringAdd_label1\n" +
                "stringAdd_label2:\n" +
                "addu\t$a1, $a1, 4\n" +
                "stringAdd_label3:\n" +
                "lb\t$a3, 0($a1)\n" +
                "beqz\t$a3, stringAdd_label4\n" +
                "sb\t$a3, 0($a0)\n" +
                "addu\t$a1, $a1, 1\n" +
                "addu\t$a0, $a0, 1\n" +
                "j\tstringAdd_label3\n" +
                "stringAdd_label4:\n" +
                "li\t$a3, 0\n" +
                "sb\t$a3, 0($a0)\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 8
        str = str + "stringEquals:\n" +
                "sw\t$ra, 124($sp)\n" +
                "lw\t$a2, 0($a0)\n" +
                "lw\t$a3, 0($a1)\n" +
                "bne\t$a2, $a3, stringEquals_neq\n" +
                "addu\t$a0, $a0, 4\n" +
                "addu\t$a1, $a1, 4\n" +
                "stringEquals_start:\n" +
                "lb\t$a2, 0($a0)\n" +
                "lb\t$a3, 0($a1)\n" +
                "addu\t$a0, $a0, 1\n" +
                "addu\t$a1, $a1, 1\n" +
                "bne\t$a2, $a3, stringEquals_neq\n" +
                "beq\t$a2, 0, stringEquals_eq\n" +
                "j\tstringEquals_start\n" +
                "stringEquals_eq:\n" +
                "li\t$v0, 1\n" +
                "j stringEquals_end\n" +
                "stringEquals_neq:\n" +
                "li\t$v0, 0\n" +
                "stringEquals_end:\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 9
        str = str + "stringLessThan:\n" +
                "sw\t$ra, 124($sp)\n" +
                "addu\t$a0, $a0, 4\n" +
                "addu\t$a1, $a1, 4\n" +
                "stringLessThan_start:\n" +
                "lb\t$a2, 0($a0)\n" +
                "lb\t$a3, 0($a1)\n" +
                "addu\t$a0, $a0, 1\n" +
                "addu\t$a1, $a1, 1\n" +
                "add\t$v0, $a2, $a3\n" +
                "beq\t$v0, 0, stringLessThan_no\n" +
                "beq\t$a2, 0, stringLessThan_yes\n" +
                "beq\t$a3, 0, stringLessThan_no\n" +
                "blt\t$a2, $a3, stringLessThan_yes\n" +
                "bgt\t$a2, $a3, stringLessThan_no\n" +
                "j\tstringLessThan_start\n" +
                "stringLessThan_yes:\n" +
                "li\t$v0, 1\n" +
                "j stringLessThan_end\n" +
                "stringLessThan_no:\n" +
                "li\t$v0, 0\n" +
                "stringLessThan_end:\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //service 10
        str = str + "stringLessThanOrEquals:\n" +
                "sw\t$ra, 124($sp)\n" +
                "addu\t$a0, $a0, 4\n" +
                "addu\t$a1, $a1, 4\n" +
                "stringLessThanOrEquals_start:\n" +
                "lb\t$a2, 0($a0)\n" +
                "lb\t$a3, 0($a1)\n" +
                "addu\t$a0, $a0, 1\n" +
                "addu\t$a1, $a1, 1\n" +
                "add\t$v0, $a2, $a3\n" +
                "beq\t$v0, 0, stringLessThanOrEquals_yes\n" +
                "beq\t$a2, 0, stringLessThanOrEquals_yes\n" +
                "beq\t$a3, 0, stringLessThanOrEquals_no\n" +
                "blt\t$a2, $a3, stringLessThanOrEquals_yes\n" +
                "bgt\t$a2, $a3, stringLessThanOrEquals_no\n" +
                "j\tstringLessThanOrEquals_start\n" +
                "stringLessThanOrEquals_yes:\n" +
                "li\t$v0, 1\n" +
                "j stringLessThanOrEquals_end\n" +
                "stringLessThanOrEquals_no:\n" +
                "li\t$v0, 0\n" +
                "stringLessThanOrEquals_end:\n" +
                "lw\t$ra, 124($sp)\n" +
                "jr\t$ra\n";

        //length:
        str = str + "length:\n"
                + "lw $v0, 0($a0)\n"
                + "jr $ra\n";

        //ord:
        str = str + "ord:\n"
                + "sw $ra, 124($sp)\n"
                + "addu $a1, $a1, 4\n"
                + "addu $a1, $a1, $a0\n"
                + "move $v0, $a1\n"
                + "lw $ra, 124($sp)\n"
                + "jr $ra\n";

        return str;
    }
}
