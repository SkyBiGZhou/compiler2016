package IR;

/**
 * Created by zhouyuhao on 2016/4/30.
 */
public class Branch extends Quadruple {
    public Address ope;
    public Label labelForTrue;
    public Label labelForFalse;

    public Branch() {
        ope = null;
        labelForTrue = null;
        labelForFalse = null;
    }

    public Branch(Address ope, Label labelForTrue, Label labelForFalse) {
        this.ope = ope;
        this.labelForTrue = labelForTrue;
        this.labelForFalse = labelForFalse;
    }

    @Override
    public void print() {
        System.out.print("br ");
        ope.print();
        System.out.print(" ");
        labelForTrue.print();
        System.out.print(" ");
        labelForFalse.print();
        System.out.print("\n");
    }

    @Override
    public String toString() {
        return "br " + ope.toString() + " " +
                "%Label" + labelForTrue.num + " " +
                "%Label" + labelForFalse.num + "\n";
    }

    @Override
    public String MIPSForCISC() {
        String str = "";
        // jump without load
        if (ope instanceof IntConst && ((IntConst) ope).value == 1) {
            str = str + "j Label" + labelForTrue.num + "\n";
        }
        if (ope instanceof IntConst && ((IntConst) ope).value == 0) {
            str = str + "j Label" + labelForFalse.num + "\n";
        }
        if (ope instanceof IntConst) {
            str = str + "li $t0, " + ((IntConst) ope).value + "\n";
        } else {
            if (((Temp) ope).name == null) {
                str = str + "lw $t0, " + ((Temp) ope).num * 4 + "($sp)\n";
            } else {
                str = str + "lw $t0, var" + ((Temp) ope).name + "\n";
            }
        }
        str = str + "beq $t0, 1, Label" + labelForTrue.num + "\n"
                + "beq $t0, 0, Label" + labelForFalse.num + "\n";
        return str;
    }
}
