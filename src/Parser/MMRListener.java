package Parser;
// Generated from MMR.g4 by ANTLR 4.5.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MMRParser}.
 */
public interface MMRListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MMRParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(MMRParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(MMRParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclaration(MMRParser.DeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclaration(MMRParser.DeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#class_declaration}.
	 * @param ctx the parse tree
	 */
	void enterClass_declaration(MMRParser.Class_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#class_declaration}.
	 * @param ctx the parse tree
	 */
	void exitClass_declaration(MMRParser.Class_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#function_declaration}.
	 * @param ctx the parse tree
	 */
	void enterFunction_declaration(MMRParser.Function_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#function_declaration}.
	 * @param ctx the parse tree
	 */
	void exitFunction_declaration(MMRParser.Function_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#type_name}.
	 * @param ctx the parse tree
	 */
	void enterType_name(MMRParser.Type_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#type_name}.
	 * @param ctx the parse tree
	 */
	void exitType_name(MMRParser.Type_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#global_var_declaration}.
	 * @param ctx the parse tree
	 */
	void enterGlobal_var_declaration(MMRParser.Global_var_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#global_var_declaration}.
	 * @param ctx the parse tree
	 */
	void exitGlobal_var_declaration(MMRParser.Global_var_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(MMRParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(MMRParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#compound_statement}.
	 * @param ctx the parse tree
	 */
	void enterCompound_statement(MMRParser.Compound_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#compound_statement}.
	 * @param ctx the parse tree
	 */
	void exitCompound_statement(MMRParser.Compound_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#var_declaration_statement}.
	 * @param ctx the parse tree
	 */
	void enterVar_declaration_statement(MMRParser.Var_declaration_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#var_declaration_statement}.
	 * @param ctx the parse tree
	 */
	void exitVar_declaration_statement(MMRParser.Var_declaration_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#var_declaration}.
	 * @param ctx the parse tree
	 */
	void enterVar_declaration(MMRParser.Var_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#var_declaration}.
	 * @param ctx the parse tree
	 */
	void exitVar_declaration(MMRParser.Var_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#expr_statement}.
	 * @param ctx the parse tree
	 */
	void enterExpr_statement(MMRParser.Expr_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#expr_statement}.
	 * @param ctx the parse tree
	 */
	void exitExpr_statement(MMRParser.Expr_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(MMRParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(MMRParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#assignment_expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_expression(MMRParser.Assignment_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#assignment_expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_expression(MMRParser.Assignment_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#logical_or_expression}.
	 * @param ctx the parse tree
	 */
	void enterLogical_or_expression(MMRParser.Logical_or_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#logical_or_expression}.
	 * @param ctx the parse tree
	 */
	void exitLogical_or_expression(MMRParser.Logical_or_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#logical_and_expression}.
	 * @param ctx the parse tree
	 */
	void enterLogical_and_expression(MMRParser.Logical_and_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#logical_and_expression}.
	 * @param ctx the parse tree
	 */
	void exitLogical_and_expression(MMRParser.Logical_and_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#or_expression}.
	 * @param ctx the parse tree
	 */
	void enterOr_expression(MMRParser.Or_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#or_expression}.
	 * @param ctx the parse tree
	 */
	void exitOr_expression(MMRParser.Or_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#xor_expression}.
	 * @param ctx the parse tree
	 */
	void enterXor_expression(MMRParser.Xor_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#xor_expression}.
	 * @param ctx the parse tree
	 */
	void exitXor_expression(MMRParser.Xor_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#and_expression}.
	 * @param ctx the parse tree
	 */
	void enterAnd_expression(MMRParser.And_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#and_expression}.
	 * @param ctx the parse tree
	 */
	void exitAnd_expression(MMRParser.And_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#equality_expression}.
	 * @param ctx the parse tree
	 */
	void enterEquality_expression(MMRParser.Equality_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#equality_expression}.
	 * @param ctx the parse tree
	 */
	void exitEquality_expression(MMRParser.Equality_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#relation_expression}.
	 * @param ctx the parse tree
	 */
	void enterRelation_expression(MMRParser.Relation_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#relation_expression}.
	 * @param ctx the parse tree
	 */
	void exitRelation_expression(MMRParser.Relation_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#shift_expression}.
	 * @param ctx the parse tree
	 */
	void enterShift_expression(MMRParser.Shift_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#shift_expression}.
	 * @param ctx the parse tree
	 */
	void exitShift_expression(MMRParser.Shift_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#add_expression}.
	 * @param ctx the parse tree
	 */
	void enterAdd_expression(MMRParser.Add_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#add_expression}.
	 * @param ctx the parse tree
	 */
	void exitAdd_expression(MMRParser.Add_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#multi_expression}.
	 * @param ctx the parse tree
	 */
	void enterMulti_expression(MMRParser.Multi_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#multi_expression}.
	 * @param ctx the parse tree
	 */
	void exitMulti_expression(MMRParser.Multi_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#creation_expression}.
	 * @param ctx the parse tree
	 */
	void enterCreation_expression(MMRParser.Creation_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#creation_expression}.
	 * @param ctx the parse tree
	 */
	void exitCreation_expression(MMRParser.Creation_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#unary_expression}.
	 * @param ctx the parse tree
	 */
	void enterUnary_expression(MMRParser.Unary_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#unary_expression}.
	 * @param ctx the parse tree
	 */
	void exitUnary_expression(MMRParser.Unary_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#postfix_expression}.
	 * @param ctx the parse tree
	 */
	void enterPostfix_expression(MMRParser.Postfix_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#postfix_expression}.
	 * @param ctx the parse tree
	 */
	void exitPostfix_expression(MMRParser.Postfix_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#primary_expression}.
	 * @param ctx the parse tree
	 */
	void enterPrimary_expression(MMRParser.Primary_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#primary_expression}.
	 * @param ctx the parse tree
	 */
	void exitPrimary_expression(MMRParser.Primary_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(MMRParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(MMRParser.ConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void enterIf_statement(MMRParser.If_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void exitIf_statement(MMRParser.If_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#loop_statement}.
	 * @param ctx the parse tree
	 */
	void enterLoop_statement(MMRParser.Loop_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#loop_statement}.
	 * @param ctx the parse tree
	 */
	void exitLoop_statement(MMRParser.Loop_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#init}.
	 * @param ctx the parse tree
	 */
	void enterInit(MMRParser.InitContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#init}.
	 * @param ctx the parse tree
	 */
	void exitInit(MMRParser.InitContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#boundary}.
	 * @param ctx the parse tree
	 */
	void enterBoundary(MMRParser.BoundaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#boundary}.
	 * @param ctx the parse tree
	 */
	void exitBoundary(MMRParser.BoundaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#step}.
	 * @param ctx the parse tree
	 */
	void enterStep(MMRParser.StepContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#step}.
	 * @param ctx the parse tree
	 */
	void exitStep(MMRParser.StepContext ctx);
	/**
	 * Enter a parse tree produced by {@link MMRParser#jump_statement}.
	 * @param ctx the parse tree
	 */
	void enterJump_statement(MMRParser.Jump_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MMRParser#jump_statement}.
	 * @param ctx the parse tree
	 */
	void exitJump_statement(MMRParser.Jump_statementContext ctx);
}