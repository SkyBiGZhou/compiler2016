package Parser;
// Generated from MMR.g4 by ANTLR 4.5.2

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.LexerATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MMRLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, BOOL=2, INT=3, STRING=4, NULL=5, VOID=6, TRUE=7, FALSE=8, NEW=9, 
		CLASS=10, IF=11, ELSE=12, FOR=13, WHILE=14, RETURN=15, BREAK=16, CONTINUE=17, 
		LEFTPA=18, RIGHTPA=19, LEFTBRACKET=20, RIGHTBRACKET=21, LEFTBRACE=22, 
		RIGHTBRACE=23, DOT=24, COMMA=25, PLUS=26, MINUS=27, TIMES=28, DIVISION=29, 
		MOD=30, ASSIGN=31, LESS=32, GREATER=33, EQUAL=34, NOTEQUAL=35, LESSEQUAL=36, 
		GREATEREQUAL=37, ANDAND=38, OROR=39, NOT=40, LEFTSHIFT=41, RIGHTSHIFT=42, 
		TILDE=43, OR=44, XOR=45, AND=46, INCREASE=47, DECREASE=48, IDENTIFIER=49, 
		INT_LITERAL=50, STRING_LITERAL=51, WHITESPACE=52, NEWLINE=53, LINECOMMENT=54, 
		BLOCKCOMMENT=55;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "BOOL", "INT", "STRING", "NULL", "VOID", "TRUE", "FALSE", "NEW", 
		"CLASS", "IF", "ELSE", "FOR", "WHILE", "RETURN", "BREAK", "CONTINUE", 
		"LEFTPA", "RIGHTPA", "LEFTBRACKET", "RIGHTBRACKET", "LEFTBRACE", "RIGHTBRACE", 
		"DOT", "COMMA", "PLUS", "MINUS", "TIMES", "DIVISION", "MOD", "ASSIGN", 
		"LESS", "GREATER", "EQUAL", "NOTEQUAL", "LESSEQUAL", "GREATEREQUAL", "ANDAND", 
		"OROR", "NOT", "LEFTSHIFT", "RIGHTSHIFT", "TILDE", "OR", "XOR", "AND", 
		"INCREASE", "DECREASE", "IDENTIFIER", "NONDIGIT", "DIGIT", "INT_LITERAL", 
		"NON_ZERO_DIGIT", "STRING_LITERAL", "SCHAR_SEQUENCE", "SCHAR", "ESCAPESEQUENCE", 
		"WHITESPACE", "NEWLINE", "LINECOMMENT", "BLOCKCOMMENT"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "';'", "'bool'", "'int'", "'string'", "'null'", "'void'", "'true'", 
		"'false'", "'new'", "'class'", "'if'", "'else'", "'for'", "'while'", "'return'", 
		"'break'", "'continue'", "'('", "')'", "'['", "']'", "'{'", "'}'", "'.'", 
		"','", "'+'", "'-'", "'*'", "'/'", "'%'", "'='", "'<'", "'>'", "'=='", 
		"'!='", "'<='", "'>='", "'&&'", "'||'", "'!'", "'<<'", "'>>'", "'~'", 
		"'|'", "'^'", "'&'", "'++'", "'--'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, "BOOL", "INT", "STRING", "NULL", "VOID", "TRUE", "FALSE", 
		"NEW", "CLASS", "IF", "ELSE", "FOR", "WHILE", "RETURN", "BREAK", "CONTINUE", 
		"LEFTPA", "RIGHTPA", "LEFTBRACKET", "RIGHTBRACKET", "LEFTBRACE", "RIGHTBRACE", 
		"DOT", "COMMA", "PLUS", "MINUS", "TIMES", "DIVISION", "MOD", "ASSIGN", 
		"LESS", "GREATER", "EQUAL", "NOTEQUAL", "LESSEQUAL", "GREATEREQUAL", "ANDAND", 
		"OROR", "NOT", "LEFTSHIFT", "RIGHTSHIFT", "TILDE", "OR", "XOR", "AND", 
		"INCREASE", "DECREASE", "IDENTIFIER", "INT_LITERAL", "STRING_LITERAL", 
		"WHITESPACE", "NEWLINE", "LINECOMMENT", "BLOCKCOMMENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public MMRLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "MMR.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\29\u0170\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3"+
		"\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3"+
		"\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21"+
		"\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\24\3\24"+
		"\3\25\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3\31\3\31\3\32\3\32\3\33\3\33"+
		"\3\34\3\34\3\35\3\35\3\36\3\36\3\37\3\37\3 \3 \3!\3!\3\"\3\"\3#\3#\3#"+
		"\3$\3$\3$\3%\3%\3%\3&\3&\3&\3\'\3\'\3\'\3(\3(\3(\3)\3)\3*\3*\3*\3+\3+"+
		"\3+\3,\3,\3-\3-\3.\3.\3/\3/\3\60\3\60\3\60\3\61\3\61\3\61\3\62\3\62\3"+
		"\62\7\62\u0122\n\62\f\62\16\62\u0125\13\62\3\63\3\63\3\64\3\64\3\65\3"+
		"\65\7\65\u012d\n\65\f\65\16\65\u0130\13\65\3\65\5\65\u0133\n\65\3\66\3"+
		"\66\3\67\3\67\3\67\3\67\38\78\u013c\n8\f8\168\u013f\138\39\39\59\u0143"+
		"\n9\3:\3:\3:\3;\6;\u0149\n;\r;\16;\u014a\3;\3;\3<\3<\5<\u0151\n<\3<\5"+
		"<\u0154\n<\3<\3<\3=\3=\3=\3=\7=\u015c\n=\f=\16=\u015f\13=\3=\3=\3>\3>"+
		"\3>\3>\7>\u0167\n>\f>\16>\u016a\13>\3>\3>\3>\3>\3>\3\u0168\2?\3\3\5\4"+
		"\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22"+
		"#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37= ?!A\"C"+
		"#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63e\2g\2i\64k\2m\65o\2q\2s\2u"+
		"\66w\67y8{9\3\2\t\5\2C\\aac|\3\2\62;\3\2\63;\6\2\f\f\17\17$$^^\f\2$$)"+
		")AA^^cdhhppttvvxx\4\2\13\13\"\"\4\2\f\f\17\17\u0174\2\3\3\2\2\2\2\5\3"+
		"\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2"+
		"\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3"+
		"\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'"+
		"\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63"+
		"\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2"+
		"?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3"+
		"\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2"+
		"\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2"+
		"i\3\2\2\2\2m\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\2{\3\2\2\2\3}\3"+
		"\2\2\2\5\177\3\2\2\2\7\u0084\3\2\2\2\t\u0088\3\2\2\2\13\u008f\3\2\2\2"+
		"\r\u0094\3\2\2\2\17\u0099\3\2\2\2\21\u009e\3\2\2\2\23\u00a4\3\2\2\2\25"+
		"\u00a8\3\2\2\2\27\u00ae\3\2\2\2\31\u00b1\3\2\2\2\33\u00b6\3\2\2\2\35\u00ba"+
		"\3\2\2\2\37\u00c0\3\2\2\2!\u00c7\3\2\2\2#\u00cd\3\2\2\2%\u00d6\3\2\2\2"+
		"\'\u00d8\3\2\2\2)\u00da\3\2\2\2+\u00dc\3\2\2\2-\u00de\3\2\2\2/\u00e0\3"+
		"\2\2\2\61\u00e2\3\2\2\2\63\u00e4\3\2\2\2\65\u00e6\3\2\2\2\67\u00e8\3\2"+
		"\2\29\u00ea\3\2\2\2;\u00ec\3\2\2\2=\u00ee\3\2\2\2?\u00f0\3\2\2\2A\u00f2"+
		"\3\2\2\2C\u00f4\3\2\2\2E\u00f6\3\2\2\2G\u00f9\3\2\2\2I\u00fc\3\2\2\2K"+
		"\u00ff\3\2\2\2M\u0102\3\2\2\2O\u0105\3\2\2\2Q\u0108\3\2\2\2S\u010a\3\2"+
		"\2\2U\u010d\3\2\2\2W\u0110\3\2\2\2Y\u0112\3\2\2\2[\u0114\3\2\2\2]\u0116"+
		"\3\2\2\2_\u0118\3\2\2\2a\u011b\3\2\2\2c\u011e\3\2\2\2e\u0126\3\2\2\2g"+
		"\u0128\3\2\2\2i\u0132\3\2\2\2k\u0134\3\2\2\2m\u0136\3\2\2\2o\u013d\3\2"+
		"\2\2q\u0142\3\2\2\2s\u0144\3\2\2\2u\u0148\3\2\2\2w\u0153\3\2\2\2y\u0157"+
		"\3\2\2\2{\u0162\3\2\2\2}~\7=\2\2~\4\3\2\2\2\177\u0080\7d\2\2\u0080\u0081"+
		"\7q\2\2\u0081\u0082\7q\2\2\u0082\u0083\7n\2\2\u0083\6\3\2\2\2\u0084\u0085"+
		"\7k\2\2\u0085\u0086\7p\2\2\u0086\u0087\7v\2\2\u0087\b\3\2\2\2\u0088\u0089"+
		"\7u\2\2\u0089\u008a\7v\2\2\u008a\u008b\7t\2\2\u008b\u008c\7k\2\2\u008c"+
		"\u008d\7p\2\2\u008d\u008e\7i\2\2\u008e\n\3\2\2\2\u008f\u0090\7p\2\2\u0090"+
		"\u0091\7w\2\2\u0091\u0092\7n\2\2\u0092\u0093\7n\2\2\u0093\f\3\2\2\2\u0094"+
		"\u0095\7x\2\2\u0095\u0096\7q\2\2\u0096\u0097\7k\2\2\u0097\u0098\7f\2\2"+
		"\u0098\16\3\2\2\2\u0099\u009a\7v\2\2\u009a\u009b\7t\2\2\u009b\u009c\7"+
		"w\2\2\u009c\u009d\7g\2\2\u009d\20\3\2\2\2\u009e\u009f\7h\2\2\u009f\u00a0"+
		"\7c\2\2\u00a0\u00a1\7n\2\2\u00a1\u00a2\7u\2\2\u00a2\u00a3\7g\2\2\u00a3"+
		"\22\3\2\2\2\u00a4\u00a5\7p\2\2\u00a5\u00a6\7g\2\2\u00a6\u00a7\7y\2\2\u00a7"+
		"\24\3\2\2\2\u00a8\u00a9\7e\2\2\u00a9\u00aa\7n\2\2\u00aa\u00ab\7c\2\2\u00ab"+
		"\u00ac\7u\2\2\u00ac\u00ad\7u\2\2\u00ad\26\3\2\2\2\u00ae\u00af\7k\2\2\u00af"+
		"\u00b0\7h\2\2\u00b0\30\3\2\2\2\u00b1\u00b2\7g\2\2\u00b2\u00b3\7n\2\2\u00b3"+
		"\u00b4\7u\2\2\u00b4\u00b5\7g\2\2\u00b5\32\3\2\2\2\u00b6\u00b7\7h\2\2\u00b7"+
		"\u00b8\7q\2\2\u00b8\u00b9\7t\2\2\u00b9\34\3\2\2\2\u00ba\u00bb\7y\2\2\u00bb"+
		"\u00bc\7j\2\2\u00bc\u00bd\7k\2\2\u00bd\u00be\7n\2\2\u00be\u00bf\7g\2\2"+
		"\u00bf\36\3\2\2\2\u00c0\u00c1\7t\2\2\u00c1\u00c2\7g\2\2\u00c2\u00c3\7"+
		"v\2\2\u00c3\u00c4\7w\2\2\u00c4\u00c5\7t\2\2\u00c5\u00c6\7p\2\2\u00c6 "+
		"\3\2\2\2\u00c7\u00c8\7d\2\2\u00c8\u00c9\7t\2\2\u00c9\u00ca\7g\2\2\u00ca"+
		"\u00cb\7c\2\2\u00cb\u00cc\7m\2\2\u00cc\"\3\2\2\2\u00cd\u00ce\7e\2\2\u00ce"+
		"\u00cf\7q\2\2\u00cf\u00d0\7p\2\2\u00d0\u00d1\7v\2\2\u00d1\u00d2\7k\2\2"+
		"\u00d2\u00d3\7p\2\2\u00d3\u00d4\7w\2\2\u00d4\u00d5\7g\2\2\u00d5$\3\2\2"+
		"\2\u00d6\u00d7\7*\2\2\u00d7&\3\2\2\2\u00d8\u00d9\7+\2\2\u00d9(\3\2\2\2"+
		"\u00da\u00db\7]\2\2\u00db*\3\2\2\2\u00dc\u00dd\7_\2\2\u00dd,\3\2\2\2\u00de"+
		"\u00df\7}\2\2\u00df.\3\2\2\2\u00e0\u00e1\7\177\2\2\u00e1\60\3\2\2\2\u00e2"+
		"\u00e3\7\60\2\2\u00e3\62\3\2\2\2\u00e4\u00e5\7.\2\2\u00e5\64\3\2\2\2\u00e6"+
		"\u00e7\7-\2\2\u00e7\66\3\2\2\2\u00e8\u00e9\7/\2\2\u00e98\3\2\2\2\u00ea"+
		"\u00eb\7,\2\2\u00eb:\3\2\2\2\u00ec\u00ed\7\61\2\2\u00ed<\3\2\2\2\u00ee"+
		"\u00ef\7\'\2\2\u00ef>\3\2\2\2\u00f0\u00f1\7?\2\2\u00f1@\3\2\2\2\u00f2"+
		"\u00f3\7>\2\2\u00f3B\3\2\2\2\u00f4\u00f5\7@\2\2\u00f5D\3\2\2\2\u00f6\u00f7"+
		"\7?\2\2\u00f7\u00f8\7?\2\2\u00f8F\3\2\2\2\u00f9\u00fa\7#\2\2\u00fa\u00fb"+
		"\7?\2\2\u00fbH\3\2\2\2\u00fc\u00fd\7>\2\2\u00fd\u00fe\7?\2\2\u00feJ\3"+
		"\2\2\2\u00ff\u0100\7@\2\2\u0100\u0101\7?\2\2\u0101L\3\2\2\2\u0102\u0103"+
		"\7(\2\2\u0103\u0104\7(\2\2\u0104N\3\2\2\2\u0105\u0106\7~\2\2\u0106\u0107"+
		"\7~\2\2\u0107P\3\2\2\2\u0108\u0109\7#\2\2\u0109R\3\2\2\2\u010a\u010b\7"+
		">\2\2\u010b\u010c\7>\2\2\u010cT\3\2\2\2\u010d\u010e\7@\2\2\u010e\u010f"+
		"\7@\2\2\u010fV\3\2\2\2\u0110\u0111\7\u0080\2\2\u0111X\3\2\2\2\u0112\u0113"+
		"\7~\2\2\u0113Z\3\2\2\2\u0114\u0115\7`\2\2\u0115\\\3\2\2\2\u0116\u0117"+
		"\7(\2\2\u0117^\3\2\2\2\u0118\u0119\7-\2\2\u0119\u011a\7-\2\2\u011a`\3"+
		"\2\2\2\u011b\u011c\7/\2\2\u011c\u011d\7/\2\2\u011db\3\2\2\2\u011e\u0123"+
		"\5e\63\2\u011f\u0122\5g\64\2\u0120\u0122\5e\63\2\u0121\u011f\3\2\2\2\u0121"+
		"\u0120\3\2\2\2\u0122\u0125\3\2\2\2\u0123\u0121\3\2\2\2\u0123\u0124\3\2"+
		"\2\2\u0124d\3\2\2\2\u0125\u0123\3\2\2\2\u0126\u0127\t\2\2\2\u0127f\3\2"+
		"\2\2\u0128\u0129\t\3\2\2\u0129h\3\2\2\2\u012a\u012e\5k\66\2\u012b\u012d"+
		"\5g\64\2\u012c\u012b\3\2\2\2\u012d\u0130\3\2\2\2\u012e\u012c\3\2\2\2\u012e"+
		"\u012f\3\2\2\2\u012f\u0133\3\2\2\2\u0130\u012e\3\2\2\2\u0131\u0133\7\62"+
		"\2\2\u0132\u012a\3\2\2\2\u0132\u0131\3\2\2\2\u0133j\3\2\2\2\u0134\u0135"+
		"\t\4\2\2\u0135l\3\2\2\2\u0136\u0137\7$\2\2\u0137\u0138\5o8\2\u0138\u0139"+
		"\7$\2\2\u0139n\3\2\2\2\u013a\u013c\5q9\2\u013b\u013a\3\2\2\2\u013c\u013f"+
		"\3\2\2\2\u013d\u013b\3\2\2\2\u013d\u013e\3\2\2\2\u013ep\3\2\2\2\u013f"+
		"\u013d\3\2\2\2\u0140\u0143\n\5\2\2\u0141\u0143\5s:\2\u0142\u0140\3\2\2"+
		"\2\u0142\u0141\3\2\2\2\u0143r\3\2\2\2\u0144\u0145\7^\2\2\u0145\u0146\t"+
		"\6\2\2\u0146t\3\2\2\2\u0147\u0149\t\7\2\2\u0148\u0147\3\2\2\2\u0149\u014a"+
		"\3\2\2\2\u014a\u0148\3\2\2\2\u014a\u014b\3\2\2\2\u014b\u014c\3\2\2\2\u014c"+
		"\u014d\b;\2\2\u014dv\3\2\2\2\u014e\u0150\7\17\2\2\u014f\u0151\7\f\2\2"+
		"\u0150\u014f\3\2\2\2\u0150\u0151\3\2\2\2\u0151\u0154\3\2\2\2\u0152\u0154"+
		"\7\f\2\2\u0153\u014e\3\2\2\2\u0153\u0152\3\2\2\2\u0154\u0155\3\2\2\2\u0155"+
		"\u0156\b<\2\2\u0156x\3\2\2\2\u0157\u0158\7\61\2\2\u0158\u0159\7\61\2\2"+
		"\u0159\u015d\3\2\2\2\u015a\u015c\n\b\2\2\u015b\u015a\3\2\2\2\u015c\u015f"+
		"\3\2\2\2\u015d\u015b\3\2\2\2\u015d\u015e\3\2\2\2\u015e\u0160\3\2\2\2\u015f"+
		"\u015d\3\2\2\2\u0160\u0161\b=\2\2\u0161z\3\2\2\2\u0162\u0163\7\61\2\2"+
		"\u0163\u0164\7,\2\2\u0164\u0168\3\2\2\2\u0165\u0167\13\2\2\2\u0166\u0165"+
		"\3\2\2\2\u0167\u016a\3\2\2\2\u0168\u0169\3\2\2\2\u0168\u0166\3\2\2\2\u0169"+
		"\u016b\3\2\2\2\u016a\u0168\3\2\2\2\u016b\u016c\7,\2\2\u016c\u016d\7\61"+
		"\2\2\u016d\u016e\3\2\2\2\u016e\u016f\b>\2\2\u016f|\3\2\2\2\16\2\u0121"+
		"\u0123\u012e\u0132\u013d\u0142\u014a\u0150\u0153\u015d\u0168\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}