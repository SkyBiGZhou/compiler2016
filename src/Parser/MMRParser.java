package Parser;
// Generated from MMR.g4 by ANTLR 4.5.2

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MMRParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, BOOL=2, INT=3, STRING=4, NULL=5, VOID=6, TRUE=7, FALSE=8, NEW=9, 
		CLASS=10, IF=11, ELSE=12, FOR=13, WHILE=14, RETURN=15, BREAK=16, CONTINUE=17, 
		LEFTPA=18, RIGHTPA=19, LEFTBRACKET=20, RIGHTBRACKET=21, LEFTBRACE=22, 
		RIGHTBRACE=23, DOT=24, COMMA=25, PLUS=26, MINUS=27, TIMES=28, DIVISION=29, 
		MOD=30, ASSIGN=31, LESS=32, GREATER=33, EQUAL=34, NOTEQUAL=35, LESSEQUAL=36, 
		GREATEREQUAL=37, ANDAND=38, OROR=39, NOT=40, LEFTSHIFT=41, RIGHTSHIFT=42, 
		TILDE=43, OR=44, XOR=45, AND=46, INCREASE=47, DECREASE=48, IDENTIFIER=49, 
		INT_LITERAL=50, STRING_LITERAL=51, WHITESPACE=52, NEWLINE=53, LINECOMMENT=54, 
		BLOCKCOMMENT=55;
	public static final int
		RULE_program = 0, RULE_declaration = 1, RULE_class_declaration = 2, RULE_function_declaration = 3, 
		RULE_type_name = 4, RULE_global_var_declaration = 5, RULE_statement = 6, 
		RULE_compound_statement = 7, RULE_var_declaration_statement = 8, RULE_var_declaration = 9, 
		RULE_expr_statement = 10, RULE_expression = 11, RULE_assignment_expression = 12, 
		RULE_logical_or_expression = 13, RULE_logical_and_expression = 14, RULE_or_expression = 15, 
		RULE_xor_expression = 16, RULE_and_expression = 17, RULE_equality_expression = 18, 
		RULE_relation_expression = 19, RULE_shift_expression = 20, RULE_add_expression = 21, 
		RULE_multi_expression = 22, RULE_creation_expression = 23, RULE_unary_expression = 24, 
		RULE_postfix_expression = 25, RULE_primary_expression = 26, RULE_constant = 27, 
		RULE_if_statement = 28, RULE_loop_statement = 29, RULE_init = 30, RULE_boundary = 31, 
		RULE_step = 32, RULE_jump_statement = 33;
	public static final String[] ruleNames = {
		"program", "declaration", "class_declaration", "function_declaration", 
		"type_name", "global_var_declaration", "statement", "compound_statement", 
		"var_declaration_statement", "var_declaration", "expr_statement", "expression", 
		"assignment_expression", "logical_or_expression", "logical_and_expression", 
		"or_expression", "xor_expression", "and_expression", "equality_expression", 
		"relation_expression", "shift_expression", "add_expression", "multi_expression", 
		"creation_expression", "unary_expression", "postfix_expression", "primary_expression", 
		"constant", "if_statement", "loop_statement", "init", "boundary", "step", 
		"jump_statement"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "';'", "'bool'", "'int'", "'string'", "'null'", "'void'", "'true'", 
		"'false'", "'new'", "'class'", "'if'", "'else'", "'for'", "'while'", "'return'", 
		"'break'", "'continue'", "'('", "')'", "'['", "']'", "'{'", "'}'", "'.'", 
		"','", "'+'", "'-'", "'*'", "'/'", "'%'", "'='", "'<'", "'>'", "'=='", 
		"'!='", "'<='", "'>='", "'&&'", "'||'", "'!'", "'<<'", "'>>'", "'~'", 
		"'|'", "'^'", "'&'", "'++'", "'--'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, "BOOL", "INT", "STRING", "NULL", "VOID", "TRUE", "FALSE", 
		"NEW", "CLASS", "IF", "ELSE", "FOR", "WHILE", "RETURN", "BREAK", "CONTINUE", 
		"LEFTPA", "RIGHTPA", "LEFTBRACKET", "RIGHTBRACKET", "LEFTBRACE", "RIGHTBRACE", 
		"DOT", "COMMA", "PLUS", "MINUS", "TIMES", "DIVISION", "MOD", "ASSIGN", 
		"LESS", "GREATER", "EQUAL", "NOTEQUAL", "LESSEQUAL", "GREATEREQUAL", "ANDAND", 
		"OROR", "NOT", "LEFTSHIFT", "RIGHTSHIFT", "TILDE", "OR", "XOR", "AND", 
		"INCREASE", "DECREASE", "IDENTIFIER", "INT_LITERAL", "STRING_LITERAL", 
		"WHITESPACE", "NEWLINE", "LINECOMMENT", "BLOCKCOMMENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "MMR.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MMRParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(MMRParser.EOF, 0); }
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOL) | (1L << INT) | (1L << STRING) | (1L << VOID) | (1L << CLASS) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(68);
				declaration();
				}
				}
				setState(73);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(74);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public Class_declarationContext class_declaration() {
			return getRuleContext(Class_declarationContext.class,0);
		}
		public Function_declarationContext function_declaration() {
			return getRuleContext(Function_declarationContext.class,0);
		}
		public Global_var_declarationContext global_var_declaration() {
			return getRuleContext(Global_var_declarationContext.class,0);
		}
		public DeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitDeclaration(this);
		}
	}

	public final DeclarationContext declaration() throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_declaration);
		try {
			setState(79);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(76);
				class_declaration();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(77);
				function_declaration();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(78);
				global_var_declaration();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_declarationContext extends ParserRuleContext {
		public TerminalNode CLASS() { return getToken(MMRParser.CLASS, 0); }
		public TerminalNode IDENTIFIER() { return getToken(MMRParser.IDENTIFIER, 0); }
		public List<Var_declaration_statementContext> var_declaration_statement() {
			return getRuleContexts(Var_declaration_statementContext.class);
		}
		public Var_declaration_statementContext var_declaration_statement(int i) {
			return getRuleContext(Var_declaration_statementContext.class,i);
		}
		public Class_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterClass_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitClass_declaration(this);
		}
	}

	public final Class_declarationContext class_declaration() throws RecognitionException {
		Class_declarationContext _localctx = new Class_declarationContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_class_declaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(81);
			match(CLASS);
			setState(82);
			match(IDENTIFIER);
			setState(83);
			match(LEFTBRACE);
			setState(87);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOL) | (1L << INT) | (1L << STRING) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(84);
				var_declaration_statement();
				}
				}
				setState(89);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(90);
			match(RIGHTBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_declarationContext extends ParserRuleContext {
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(MMRParser.IDENTIFIER, 0); }
		public Compound_statementContext compound_statement() {
			return getRuleContext(Compound_statementContext.class,0);
		}
		public List<Var_declarationContext> var_declaration() {
			return getRuleContexts(Var_declarationContext.class);
		}
		public Var_declarationContext var_declaration(int i) {
			return getRuleContext(Var_declarationContext.class,i);
		}
		public TerminalNode VOID() { return getToken(MMRParser.VOID, 0); }
		public Function_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterFunction_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitFunction_declaration(this);
		}
	}

	public final Function_declarationContext function_declaration() throws RecognitionException {
		Function_declarationContext _localctx = new Function_declarationContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_function_declaration);
		int _la;
		try {
			setState(123);
			switch (_input.LA(1)) {
			case BOOL:
			case INT:
			case STRING:
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(92);
				type_name(0);
				setState(93);
				match(IDENTIFIER);
				setState(94);
				match(LEFTPA);
				setState(103);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOL) | (1L << INT) | (1L << STRING) | (1L << IDENTIFIER))) != 0)) {
					{
					setState(95);
					var_declaration();
					setState(100);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(96);
						match(COMMA);
						setState(97);
						var_declaration();
						}
						}
						setState(102);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(105);
				match(RIGHTPA);
				setState(106);
				compound_statement();
				}
				break;
			case VOID:
				enterOuterAlt(_localctx, 2);
				{
				setState(108);
				match(VOID);
				setState(109);
				match(IDENTIFIER);
				setState(110);
				match(LEFTPA);
				setState(119);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOL) | (1L << INT) | (1L << STRING) | (1L << IDENTIFIER))) != 0)) {
					{
					setState(111);
					var_declaration();
					setState(116);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(112);
						match(COMMA);
						setState(113);
						var_declaration();
						}
						}
						setState(118);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(121);
				match(RIGHTPA);
				setState(122);
				compound_statement();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_nameContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(MMRParser.INT, 0); }
		public TerminalNode BOOL() { return getToken(MMRParser.BOOL, 0); }
		public TerminalNode STRING() { return getToken(MMRParser.STRING, 0); }
		public TerminalNode IDENTIFIER() { return getToken(MMRParser.IDENTIFIER, 0); }
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Type_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterType_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitType_name(this);
		}
	}

	public final Type_nameContext type_name() throws RecognitionException {
		return type_name(0);
	}

	private Type_nameContext type_name(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Type_nameContext _localctx = new Type_nameContext(_ctx, _parentState);
		Type_nameContext _prevctx = _localctx;
		int _startState = 8;
		enterRecursionRule(_localctx, 8, RULE_type_name, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(130);
			switch (_input.LA(1)) {
			case INT:
				{
				setState(126);
				match(INT);
				}
				break;
			case BOOL:
				{
				setState(127);
				match(BOOL);
				}
				break;
			case STRING:
				{
				setState(128);
				match(STRING);
				}
				break;
			case IDENTIFIER:
				{
				setState(129);
				match(IDENTIFIER);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(140);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Type_nameContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_type_name);
					setState(132);
					if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
					{
					setState(133);
					match(LEFTBRACKET);
					setState(135);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NULL) | (1L << TRUE) | (1L << FALSE) | (1L << NEW) | (1L << LEFTPA) | (1L << PLUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << INCREASE) | (1L << DECREASE) | (1L << IDENTIFIER) | (1L << INT_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
						{
						setState(134);
						expression();
						}
					}

					setState(137);
					match(RIGHTBRACKET);
					}
					}
					} 
				}
				setState(142);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Global_var_declarationContext extends ParserRuleContext {
		public Var_declarationContext var_declaration() {
			return getRuleContext(Var_declarationContext.class,0);
		}
		public Global_var_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_global_var_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterGlobal_var_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitGlobal_var_declaration(this);
		}
	}

	public final Global_var_declarationContext global_var_declaration() throws RecognitionException {
		Global_var_declarationContext _localctx = new Global_var_declarationContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_global_var_declaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(143);
			var_declaration();
			setState(144);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public Compound_statementContext compound_statement() {
			return getRuleContext(Compound_statementContext.class,0);
		}
		public If_statementContext if_statement() {
			return getRuleContext(If_statementContext.class,0);
		}
		public Var_declaration_statementContext var_declaration_statement() {
			return getRuleContext(Var_declaration_statementContext.class,0);
		}
		public Expr_statementContext expr_statement() {
			return getRuleContext(Expr_statementContext.class,0);
		}
		public Loop_statementContext loop_statement() {
			return getRuleContext(Loop_statementContext.class,0);
		}
		public Jump_statementContext jump_statement() {
			return getRuleContext(Jump_statementContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_statement);
		try {
			setState(152);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(146);
				compound_statement();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(147);
				if_statement();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(148);
				var_declaration_statement();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(149);
				expr_statement();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(150);
				loop_statement();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(151);
				jump_statement();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Compound_statementContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public Compound_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compound_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterCompound_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitCompound_statement(this);
		}
	}

	public final Compound_statementContext compound_statement() throws RecognitionException {
		Compound_statementContext _localctx = new Compound_statementContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_compound_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(154);
			match(LEFTBRACE);
			setState(158);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << BOOL) | (1L << INT) | (1L << STRING) | (1L << NULL) | (1L << TRUE) | (1L << FALSE) | (1L << NEW) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RETURN) | (1L << BREAK) | (1L << CONTINUE) | (1L << LEFTPA) | (1L << LEFTBRACE) | (1L << PLUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << INCREASE) | (1L << DECREASE) | (1L << IDENTIFIER) | (1L << INT_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
				{
				{
				setState(155);
				statement();
				}
				}
				setState(160);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(161);
			match(RIGHTBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_declaration_statementContext extends ParserRuleContext {
		public Var_declarationContext var_declaration() {
			return getRuleContext(Var_declarationContext.class,0);
		}
		public Var_declaration_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_declaration_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterVar_declaration_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitVar_declaration_statement(this);
		}
	}

	public final Var_declaration_statementContext var_declaration_statement() throws RecognitionException {
		Var_declaration_statementContext _localctx = new Var_declaration_statementContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_var_declaration_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(163);
			var_declaration();
			setState(164);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_declarationContext extends ParserRuleContext {
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(MMRParser.IDENTIFIER, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Var_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterVar_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitVar_declaration(this);
		}
	}

	public final Var_declarationContext var_declaration() throws RecognitionException {
		Var_declarationContext _localctx = new Var_declarationContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_var_declaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(166);
			type_name(0);
			setState(167);
			match(IDENTIFIER);
			setState(170);
			_la = _input.LA(1);
			if (_la==ASSIGN) {
				{
				setState(168);
				match(ASSIGN);
				setState(169);
				expression();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_statementContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Expr_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterExpr_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitExpr_statement(this);
		}
	}

	public final Expr_statementContext expr_statement() throws RecognitionException {
		Expr_statementContext _localctx = new Expr_statementContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_expr_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(173);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NULL) | (1L << TRUE) | (1L << FALSE) | (1L << NEW) | (1L << LEFTPA) | (1L << PLUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << INCREASE) | (1L << DECREASE) | (1L << IDENTIFIER) | (1L << INT_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
				{
				setState(172);
				expression();
				}
			}

			setState(175);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public Assignment_expressionContext assignment_expression() {
			return getRuleContext(Assignment_expressionContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitExpression(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(177);
			assignment_expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_expressionContext extends ParserRuleContext {
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public Assignment_expressionContext assignment_expression() {
			return getRuleContext(Assignment_expressionContext.class,0);
		}
		public Logical_or_expressionContext logical_or_expression() {
			return getRuleContext(Logical_or_expressionContext.class,0);
		}
		public Assignment_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterAssignment_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitAssignment_expression(this);
		}
	}

	public final Assignment_expressionContext assignment_expression() throws RecognitionException {
		Assignment_expressionContext _localctx = new Assignment_expressionContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_assignment_expression);
		try {
			setState(184);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(179);
				unary_expression();
				setState(180);
				match(ASSIGN);
				setState(181);
				assignment_expression();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(183);
				logical_or_expression(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logical_or_expressionContext extends ParserRuleContext {
		public Logical_and_expressionContext logical_and_expression() {
			return getRuleContext(Logical_and_expressionContext.class,0);
		}
		public Logical_or_expressionContext logical_or_expression() {
			return getRuleContext(Logical_or_expressionContext.class,0);
		}
		public Logical_or_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_or_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterLogical_or_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitLogical_or_expression(this);
		}
	}

	public final Logical_or_expressionContext logical_or_expression() throws RecognitionException {
		return logical_or_expression(0);
	}

	private Logical_or_expressionContext logical_or_expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Logical_or_expressionContext _localctx = new Logical_or_expressionContext(_ctx, _parentState);
		Logical_or_expressionContext _prevctx = _localctx;
		int _startState = 26;
		enterRecursionRule(_localctx, 26, RULE_logical_or_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(187);
			logical_and_expression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(194);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Logical_or_expressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_logical_or_expression);
					setState(189);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(190);
					match(OROR);
					setState(191);
					logical_and_expression(0);
					}
					} 
				}
				setState(196);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Logical_and_expressionContext extends ParserRuleContext {
		public Or_expressionContext or_expression() {
			return getRuleContext(Or_expressionContext.class,0);
		}
		public Logical_and_expressionContext logical_and_expression() {
			return getRuleContext(Logical_and_expressionContext.class,0);
		}
		public Logical_and_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_and_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterLogical_and_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitLogical_and_expression(this);
		}
	}

	public final Logical_and_expressionContext logical_and_expression() throws RecognitionException {
		return logical_and_expression(0);
	}

	private Logical_and_expressionContext logical_and_expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Logical_and_expressionContext _localctx = new Logical_and_expressionContext(_ctx, _parentState);
		Logical_and_expressionContext _prevctx = _localctx;
		int _startState = 28;
		enterRecursionRule(_localctx, 28, RULE_logical_and_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(198);
			or_expression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(205);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Logical_and_expressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_logical_and_expression);
					setState(200);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(201);
					match(ANDAND);
					setState(202);
					or_expression(0);
					}
					} 
				}
				setState(207);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Or_expressionContext extends ParserRuleContext {
		public Xor_expressionContext xor_expression() {
			return getRuleContext(Xor_expressionContext.class,0);
		}
		public Or_expressionContext or_expression() {
			return getRuleContext(Or_expressionContext.class,0);
		}
		public Or_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_or_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterOr_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitOr_expression(this);
		}
	}

	public final Or_expressionContext or_expression() throws RecognitionException {
		return or_expression(0);
	}

	private Or_expressionContext or_expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Or_expressionContext _localctx = new Or_expressionContext(_ctx, _parentState);
		Or_expressionContext _prevctx = _localctx;
		int _startState = 30;
		enterRecursionRule(_localctx, 30, RULE_or_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(209);
			xor_expression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(216);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Or_expressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_or_expression);
					setState(211);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(212);
					match(OR);
					setState(213);
					xor_expression(0);
					}
					} 
				}
				setState(218);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Xor_expressionContext extends ParserRuleContext {
		public And_expressionContext and_expression() {
			return getRuleContext(And_expressionContext.class,0);
		}
		public Xor_expressionContext xor_expression() {
			return getRuleContext(Xor_expressionContext.class,0);
		}
		public Xor_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_xor_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterXor_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitXor_expression(this);
		}
	}

	public final Xor_expressionContext xor_expression() throws RecognitionException {
		return xor_expression(0);
	}

	private Xor_expressionContext xor_expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Xor_expressionContext _localctx = new Xor_expressionContext(_ctx, _parentState);
		Xor_expressionContext _prevctx = _localctx;
		int _startState = 32;
		enterRecursionRule(_localctx, 32, RULE_xor_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(220);
			and_expression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(227);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Xor_expressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_xor_expression);
					setState(222);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(223);
					match(XOR);
					setState(224);
					and_expression(0);
					}
					} 
				}
				setState(229);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class And_expressionContext extends ParserRuleContext {
		public Equality_expressionContext equality_expression() {
			return getRuleContext(Equality_expressionContext.class,0);
		}
		public And_expressionContext and_expression() {
			return getRuleContext(And_expressionContext.class,0);
		}
		public And_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_and_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterAnd_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitAnd_expression(this);
		}
	}

	public final And_expressionContext and_expression() throws RecognitionException {
		return and_expression(0);
	}

	private And_expressionContext and_expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		And_expressionContext _localctx = new And_expressionContext(_ctx, _parentState);
		And_expressionContext _prevctx = _localctx;
		int _startState = 34;
		enterRecursionRule(_localctx, 34, RULE_and_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(231);
			equality_expression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(238);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new And_expressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_and_expression);
					setState(233);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(234);
					match(AND);
					setState(235);
					equality_expression(0);
					}
					} 
				}
				setState(240);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Equality_expressionContext extends ParserRuleContext {
		public Relation_expressionContext relation_expression() {
			return getRuleContext(Relation_expressionContext.class,0);
		}
		public Equality_expressionContext equality_expression() {
			return getRuleContext(Equality_expressionContext.class,0);
		}
		public TerminalNode EQUAL() { return getToken(MMRParser.EQUAL, 0); }
		public TerminalNode NOTEQUAL() { return getToken(MMRParser.NOTEQUAL, 0); }
		public Equality_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equality_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterEquality_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitEquality_expression(this);
		}
	}

	public final Equality_expressionContext equality_expression() throws RecognitionException {
		return equality_expression(0);
	}

	private Equality_expressionContext equality_expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Equality_expressionContext _localctx = new Equality_expressionContext(_ctx, _parentState);
		Equality_expressionContext _prevctx = _localctx;
		int _startState = 36;
		enterRecursionRule(_localctx, 36, RULE_equality_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(242);
			relation_expression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(252);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(250);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
					case 1:
						{
						_localctx = new Equality_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_equality_expression);
						setState(244);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(245);
						match(EQUAL);
						setState(246);
						relation_expression(0);
						}
						break;
					case 2:
						{
						_localctx = new Equality_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_equality_expression);
						setState(247);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(248);
						match(NOTEQUAL);
						setState(249);
						relation_expression(0);
						}
						break;
					}
					} 
				}
				setState(254);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Relation_expressionContext extends ParserRuleContext {
		public Shift_expressionContext shift_expression() {
			return getRuleContext(Shift_expressionContext.class,0);
		}
		public Relation_expressionContext relation_expression() {
			return getRuleContext(Relation_expressionContext.class,0);
		}
		public TerminalNode GREATER() { return getToken(MMRParser.GREATER, 0); }
		public TerminalNode LESS() { return getToken(MMRParser.LESS, 0); }
		public TerminalNode GREATEREQUAL() { return getToken(MMRParser.GREATEREQUAL, 0); }
		public TerminalNode LESSEQUAL() { return getToken(MMRParser.LESSEQUAL, 0); }
		public Relation_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relation_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterRelation_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitRelation_expression(this);
		}
	}

	public final Relation_expressionContext relation_expression() throws RecognitionException {
		return relation_expression(0);
	}

	private Relation_expressionContext relation_expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Relation_expressionContext _localctx = new Relation_expressionContext(_ctx, _parentState);
		Relation_expressionContext _prevctx = _localctx;
		int _startState = 38;
		enterRecursionRule(_localctx, 38, RULE_relation_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(256);
			shift_expression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(272);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(270);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,23,_ctx) ) {
					case 1:
						{
						_localctx = new Relation_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_relation_expression);
						setState(258);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(259);
						match(GREATER);
						setState(260);
						shift_expression(0);
						}
						break;
					case 2:
						{
						_localctx = new Relation_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_relation_expression);
						setState(261);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(262);
						match(LESS);
						setState(263);
						shift_expression(0);
						}
						break;
					case 3:
						{
						_localctx = new Relation_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_relation_expression);
						setState(264);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(265);
						match(GREATEREQUAL);
						setState(266);
						shift_expression(0);
						}
						break;
					case 4:
						{
						_localctx = new Relation_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_relation_expression);
						setState(267);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(268);
						match(LESSEQUAL);
						setState(269);
						shift_expression(0);
						}
						break;
					}
					} 
				}
				setState(274);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Shift_expressionContext extends ParserRuleContext {
		public Add_expressionContext add_expression() {
			return getRuleContext(Add_expressionContext.class,0);
		}
		public Shift_expressionContext shift_expression() {
			return getRuleContext(Shift_expressionContext.class,0);
		}
		public TerminalNode RIGHTSHIFT() { return getToken(MMRParser.RIGHTSHIFT, 0); }
		public TerminalNode LEFTSHIFT() { return getToken(MMRParser.LEFTSHIFT, 0); }
		public Shift_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shift_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterShift_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitShift_expression(this);
		}
	}

	public final Shift_expressionContext shift_expression() throws RecognitionException {
		return shift_expression(0);
	}

	private Shift_expressionContext shift_expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Shift_expressionContext _localctx = new Shift_expressionContext(_ctx, _parentState);
		Shift_expressionContext _prevctx = _localctx;
		int _startState = 40;
		enterRecursionRule(_localctx, 40, RULE_shift_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(276);
			add_expression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(286);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(284);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
					case 1:
						{
						_localctx = new Shift_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_shift_expression);
						setState(278);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(279);
						match(RIGHTSHIFT);
						setState(280);
						add_expression(0);
						}
						break;
					case 2:
						{
						_localctx = new Shift_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_shift_expression);
						setState(281);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(282);
						match(LEFTSHIFT);
						setState(283);
						add_expression(0);
						}
						break;
					}
					} 
				}
				setState(288);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Add_expressionContext extends ParserRuleContext {
		public Multi_expressionContext multi_expression() {
			return getRuleContext(Multi_expressionContext.class,0);
		}
		public Add_expressionContext add_expression() {
			return getRuleContext(Add_expressionContext.class,0);
		}
		public TerminalNode PLUS() { return getToken(MMRParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(MMRParser.MINUS, 0); }
		public Add_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_add_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterAdd_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitAdd_expression(this);
		}
	}

	public final Add_expressionContext add_expression() throws RecognitionException {
		return add_expression(0);
	}

	private Add_expressionContext add_expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Add_expressionContext _localctx = new Add_expressionContext(_ctx, _parentState);
		Add_expressionContext _prevctx = _localctx;
		int _startState = 42;
		enterRecursionRule(_localctx, 42, RULE_add_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(290);
			multi_expression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(300);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(298);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
					case 1:
						{
						_localctx = new Add_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_add_expression);
						setState(292);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(293);
						match(PLUS);
						setState(294);
						multi_expression(0);
						}
						break;
					case 2:
						{
						_localctx = new Add_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_add_expression);
						setState(295);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(296);
						match(MINUS);
						setState(297);
						multi_expression(0);
						}
						break;
					}
					} 
				}
				setState(302);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Multi_expressionContext extends ParserRuleContext {
		public Creation_expressionContext creation_expression() {
			return getRuleContext(Creation_expressionContext.class,0);
		}
		public Multi_expressionContext multi_expression() {
			return getRuleContext(Multi_expressionContext.class,0);
		}
		public TerminalNode TIMES() { return getToken(MMRParser.TIMES, 0); }
		public TerminalNode DIVISION() { return getToken(MMRParser.DIVISION, 0); }
		public TerminalNode MOD() { return getToken(MMRParser.MOD, 0); }
		public Multi_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multi_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterMulti_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitMulti_expression(this);
		}
	}

	public final Multi_expressionContext multi_expression() throws RecognitionException {
		return multi_expression(0);
	}

	private Multi_expressionContext multi_expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Multi_expressionContext _localctx = new Multi_expressionContext(_ctx, _parentState);
		Multi_expressionContext _prevctx = _localctx;
		int _startState = 44;
		enterRecursionRule(_localctx, 44, RULE_multi_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(304);
			creation_expression();
			}
			_ctx.stop = _input.LT(-1);
			setState(317);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(315);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,29,_ctx) ) {
					case 1:
						{
						_localctx = new Multi_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_multi_expression);
						setState(306);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(307);
						match(TIMES);
						setState(308);
						creation_expression();
						}
						break;
					case 2:
						{
						_localctx = new Multi_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_multi_expression);
						setState(309);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(310);
						match(DIVISION);
						setState(311);
						creation_expression();
						}
						break;
					case 3:
						{
						_localctx = new Multi_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_multi_expression);
						setState(312);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(313);
						match(MOD);
						setState(314);
						creation_expression();
						}
						break;
					}
					} 
				}
				setState(319);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Creation_expressionContext extends ParserRuleContext {
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public Creation_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_creation_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterCreation_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitCreation_expression(this);
		}
	}

	public final Creation_expressionContext creation_expression() throws RecognitionException {
		Creation_expressionContext _localctx = new Creation_expressionContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_creation_expression);
		try {
			setState(323);
			switch (_input.LA(1)) {
			case NEW:
				enterOuterAlt(_localctx, 1);
				{
				setState(320);
				match(NEW);
				setState(321);
				type_name(0);
				}
				break;
			case NULL:
			case TRUE:
			case FALSE:
			case LEFTPA:
			case PLUS:
			case MINUS:
			case NOT:
			case TILDE:
			case INCREASE:
			case DECREASE:
			case IDENTIFIER:
			case INT_LITERAL:
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(322);
				unary_expression();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Unary_expressionContext extends ParserRuleContext {
		public Postfix_expressionContext postfix_expression() {
			return getRuleContext(Postfix_expressionContext.class,0);
		}
		public TerminalNode INCREASE() { return getToken(MMRParser.INCREASE, 0); }
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public TerminalNode DECREASE() { return getToken(MMRParser.DECREASE, 0); }
		public TerminalNode PLUS() { return getToken(MMRParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(MMRParser.MINUS, 0); }
		public TerminalNode TILDE() { return getToken(MMRParser.TILDE, 0); }
		public TerminalNode NOT() { return getToken(MMRParser.NOT, 0); }
		public Unary_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterUnary_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitUnary_expression(this);
		}
	}

	public final Unary_expressionContext unary_expression() throws RecognitionException {
		Unary_expressionContext _localctx = new Unary_expressionContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_unary_expression);
		try {
			setState(338);
			switch (_input.LA(1)) {
			case NULL:
			case TRUE:
			case FALSE:
			case LEFTPA:
			case IDENTIFIER:
			case INT_LITERAL:
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 1);
				{
				setState(325);
				postfix_expression(0);
				}
				break;
			case INCREASE:
				enterOuterAlt(_localctx, 2);
				{
				setState(326);
				match(INCREASE);
				setState(327);
				unary_expression();
				}
				break;
			case DECREASE:
				enterOuterAlt(_localctx, 3);
				{
				setState(328);
				match(DECREASE);
				setState(329);
				unary_expression();
				}
				break;
			case PLUS:
				enterOuterAlt(_localctx, 4);
				{
				setState(330);
				match(PLUS);
				setState(331);
				unary_expression();
				}
				break;
			case MINUS:
				enterOuterAlt(_localctx, 5);
				{
				setState(332);
				match(MINUS);
				setState(333);
				unary_expression();
				}
				break;
			case TILDE:
				enterOuterAlt(_localctx, 6);
				{
				setState(334);
				match(TILDE);
				setState(335);
				unary_expression();
				}
				break;
			case NOT:
				enterOuterAlt(_localctx, 7);
				{
				setState(336);
				match(NOT);
				setState(337);
				unary_expression();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Postfix_expressionContext extends ParserRuleContext {
		public Primary_expressionContext primary_expression() {
			return getRuleContext(Primary_expressionContext.class,0);
		}
		public Postfix_expressionContext postfix_expression() {
			return getRuleContext(Postfix_expressionContext.class,0);
		}
		public TerminalNode INCREASE() { return getToken(MMRParser.INCREASE, 0); }
		public TerminalNode DECREASE() { return getToken(MMRParser.DECREASE, 0); }
		public TerminalNode LEFTBRACKET() { return getToken(MMRParser.LEFTBRACKET, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RIGHTBRACKET() { return getToken(MMRParser.RIGHTBRACKET, 0); }
		public TerminalNode DOT() { return getToken(MMRParser.DOT, 0); }
		public TerminalNode IDENTIFIER() { return getToken(MMRParser.IDENTIFIER, 0); }
		public TerminalNode LEFTPA() { return getToken(MMRParser.LEFTPA, 0); }
		public TerminalNode RIGHTPA() { return getToken(MMRParser.RIGHTPA, 0); }
		public List<Assignment_expressionContext> assignment_expression() {
			return getRuleContexts(Assignment_expressionContext.class);
		}
		public Assignment_expressionContext assignment_expression(int i) {
			return getRuleContext(Assignment_expressionContext.class,i);
		}
		public Postfix_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postfix_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterPostfix_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitPostfix_expression(this);
		}
	}

	public final Postfix_expressionContext postfix_expression() throws RecognitionException {
		return postfix_expression(0);
	}

	private Postfix_expressionContext postfix_expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Postfix_expressionContext _localctx = new Postfix_expressionContext(_ctx, _parentState);
		Postfix_expressionContext _prevctx = _localctx;
		int _startState = 50;
		enterRecursionRule(_localctx, 50, RULE_postfix_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(341);
			primary_expression();
			}
			_ctx.stop = _input.LT(-1);
			setState(370);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,36,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(368);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
					case 1:
						{
						_localctx = new Postfix_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_postfix_expression);
						setState(343);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(344);
						match(INCREASE);
						}
						break;
					case 2:
						{
						_localctx = new Postfix_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_postfix_expression);
						setState(345);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(346);
						match(DECREASE);
						}
						break;
					case 3:
						{
						_localctx = new Postfix_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_postfix_expression);
						setState(347);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(348);
						match(LEFTBRACKET);
						setState(349);
						expression();
						setState(350);
						match(RIGHTBRACKET);
						}
						break;
					case 4:
						{
						_localctx = new Postfix_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_postfix_expression);
						setState(352);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(353);
						match(DOT);
						setState(354);
						match(IDENTIFIER);
						}
						break;
					case 5:
						{
						_localctx = new Postfix_expressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_postfix_expression);
						setState(355);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(356);
						match(LEFTPA);
						setState(365);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NULL) | (1L << TRUE) | (1L << FALSE) | (1L << NEW) | (1L << LEFTPA) | (1L << PLUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << INCREASE) | (1L << DECREASE) | (1L << IDENTIFIER) | (1L << INT_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
							{
							setState(357);
							assignment_expression();
							setState(362);
							_errHandler.sync(this);
							_la = _input.LA(1);
							while (_la==COMMA) {
								{
								{
								setState(358);
								match(COMMA);
								setState(359);
								assignment_expression();
								}
								}
								setState(364);
								_errHandler.sync(this);
								_la = _input.LA(1);
							}
							}
						}

						setState(367);
						match(RIGHTPA);
						}
						break;
					}
					} 
				}
				setState(372);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,36,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Primary_expressionContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(MMRParser.IDENTIFIER, 0); }
		public ConstantContext constant() {
			return getRuleContext(ConstantContext.class,0);
		}
		public TerminalNode LEFTPA() { return getToken(MMRParser.LEFTPA, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RIGHTPA() { return getToken(MMRParser.RIGHTPA, 0); }
		public Primary_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primary_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterPrimary_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitPrimary_expression(this);
		}
	}

	public final Primary_expressionContext primary_expression() throws RecognitionException {
		Primary_expressionContext _localctx = new Primary_expressionContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_primary_expression);
		try {
			setState(379);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(373);
				match(IDENTIFIER);
				}
				break;
			case NULL:
			case TRUE:
			case FALSE:
			case INT_LITERAL:
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(374);
				constant();
				}
				break;
			case LEFTPA:
				enterOuterAlt(_localctx, 3);
				{
				setState(375);
				match(LEFTPA);
				setState(376);
				expression();
				setState(377);
				match(RIGHTPA);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantContext extends ParserRuleContext {
		public TerminalNode NULL() { return getToken(MMRParser.NULL, 0); }
		public TerminalNode TRUE() { return getToken(MMRParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(MMRParser.FALSE, 0); }
		public TerminalNode INT_LITERAL() { return getToken(MMRParser.INT_LITERAL, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(MMRParser.STRING_LITERAL, 0); }
		public ConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitConstant(this);
		}
	}

	public final ConstantContext constant() throws RecognitionException {
		ConstantContext _localctx = new ConstantContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_constant);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(381);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NULL) | (1L << TRUE) | (1L << FALSE) | (1L << INT_LITERAL) | (1L << STRING_LITERAL))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_statementContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(MMRParser.ELSE, 0); }
		public If_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterIf_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitIf_statement(this);
		}
	}

	public final If_statementContext if_statement() throws RecognitionException {
		If_statementContext _localctx = new If_statementContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_if_statement);
		try {
			setState(397);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,38,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(383);
				match(IF);
				setState(384);
				match(LEFTPA);
				setState(385);
				expression();
				setState(386);
				match(RIGHTPA);
				setState(387);
				statement();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(389);
				match(IF);
				setState(390);
				match(LEFTPA);
				setState(391);
				expression();
				setState(392);
				match(RIGHTPA);
				setState(393);
				statement();
				setState(394);
				match(ELSE);
				setState(395);
				statement();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Loop_statementContext extends ParserRuleContext {
		public TerminalNode FOR() { return getToken(MMRParser.FOR, 0); }
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public InitContext init() {
			return getRuleContext(InitContext.class,0);
		}
		public BoundaryContext boundary() {
			return getRuleContext(BoundaryContext.class,0);
		}
		public StepContext step() {
			return getRuleContext(StepContext.class,0);
		}
		public TerminalNode WHILE() { return getToken(MMRParser.WHILE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Loop_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_loop_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterLoop_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitLoop_statement(this);
		}
	}

	public final Loop_statementContext loop_statement() throws RecognitionException {
		Loop_statementContext _localctx = new Loop_statementContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_loop_statement);
		int _la;
		try {
			setState(420);
			switch (_input.LA(1)) {
			case FOR:
				enterOuterAlt(_localctx, 1);
				{
				setState(399);
				match(FOR);
				setState(400);
				match(LEFTPA);
				setState(402);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NULL) | (1L << TRUE) | (1L << FALSE) | (1L << NEW) | (1L << LEFTPA) | (1L << PLUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << INCREASE) | (1L << DECREASE) | (1L << IDENTIFIER) | (1L << INT_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
					{
					setState(401);
					init();
					}
				}

				setState(404);
				match(T__0);
				setState(406);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NULL) | (1L << TRUE) | (1L << FALSE) | (1L << NEW) | (1L << LEFTPA) | (1L << PLUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << INCREASE) | (1L << DECREASE) | (1L << IDENTIFIER) | (1L << INT_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
					{
					setState(405);
					boundary();
					}
				}

				setState(408);
				match(T__0);
				setState(410);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NULL) | (1L << TRUE) | (1L << FALSE) | (1L << NEW) | (1L << LEFTPA) | (1L << PLUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << INCREASE) | (1L << DECREASE) | (1L << IDENTIFIER) | (1L << INT_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
					{
					setState(409);
					step();
					}
				}

				setState(412);
				match(RIGHTPA);
				setState(413);
				statement();
				}
				break;
			case WHILE:
				enterOuterAlt(_localctx, 2);
				{
				setState(414);
				match(WHILE);
				setState(415);
				match(LEFTPA);
				setState(416);
				expression();
				setState(417);
				match(RIGHTPA);
				setState(418);
				statement();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public InitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterInit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitInit(this);
		}
	}

	public final InitContext init() throws RecognitionException {
		InitContext _localctx = new InitContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_init);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(422);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoundaryContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BoundaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boundary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterBoundary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitBoundary(this);
		}
	}

	public final BoundaryContext boundary() throws RecognitionException {
		BoundaryContext _localctx = new BoundaryContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_boundary);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(424);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StepContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public StepContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_step; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterStep(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitStep(this);
		}
	}

	public final StepContext step() throws RecognitionException {
		StepContext _localctx = new StepContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_step);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(426);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Jump_statementContext extends ParserRuleContext {
		public TerminalNode RETURN() { return getToken(MMRParser.RETURN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode BREAK() { return getToken(MMRParser.BREAK, 0); }
		public TerminalNode CONTINUE() { return getToken(MMRParser.CONTINUE, 0); }
		public Jump_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_jump_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).enterJump_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MMRListener ) ((MMRListener)listener).exitJump_statement(this);
		}
	}

	public final Jump_statementContext jump_statement() throws RecognitionException {
		Jump_statementContext _localctx = new Jump_statementContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_jump_statement);
		int _la;
		try {
			setState(437);
			switch (_input.LA(1)) {
			case RETURN:
				enterOuterAlt(_localctx, 1);
				{
				setState(428);
				match(RETURN);
				setState(430);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NULL) | (1L << TRUE) | (1L << FALSE) | (1L << NEW) | (1L << LEFTPA) | (1L << PLUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << INCREASE) | (1L << DECREASE) | (1L << IDENTIFIER) | (1L << INT_LITERAL) | (1L << STRING_LITERAL))) != 0)) {
					{
					setState(429);
					expression();
					}
				}

				setState(432);
				match(T__0);
				}
				break;
			case BREAK:
				enterOuterAlt(_localctx, 2);
				{
				setState(433);
				match(BREAK);
				setState(434);
				match(T__0);
				}
				break;
			case CONTINUE:
				enterOuterAlt(_localctx, 3);
				{
				setState(435);
				match(CONTINUE);
				setState(436);
				match(T__0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 4:
			return type_name_sempred((Type_nameContext)_localctx, predIndex);
		case 13:
			return logical_or_expression_sempred((Logical_or_expressionContext)_localctx, predIndex);
		case 14:
			return logical_and_expression_sempred((Logical_and_expressionContext)_localctx, predIndex);
		case 15:
			return or_expression_sempred((Or_expressionContext)_localctx, predIndex);
		case 16:
			return xor_expression_sempred((Xor_expressionContext)_localctx, predIndex);
		case 17:
			return and_expression_sempred((And_expressionContext)_localctx, predIndex);
		case 18:
			return equality_expression_sempred((Equality_expressionContext)_localctx, predIndex);
		case 19:
			return relation_expression_sempred((Relation_expressionContext)_localctx, predIndex);
		case 20:
			return shift_expression_sempred((Shift_expressionContext)_localctx, predIndex);
		case 21:
			return add_expression_sempred((Add_expressionContext)_localctx, predIndex);
		case 22:
			return multi_expression_sempred((Multi_expressionContext)_localctx, predIndex);
		case 25:
			return postfix_expression_sempred((Postfix_expressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean type_name_sempred(Type_nameContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 5);
		}
		return true;
	}
	private boolean logical_or_expression_sempred(Logical_or_expressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean logical_and_expression_sempred(Logical_and_expressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean or_expression_sempred(Or_expressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 3:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean xor_expression_sempred(Xor_expressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 4:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean and_expression_sempred(And_expressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 5:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean equality_expression_sempred(Equality_expressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 6:
			return precpred(_ctx, 3);
		case 7:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean relation_expression_sempred(Relation_expressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 8:
			return precpred(_ctx, 5);
		case 9:
			return precpred(_ctx, 4);
		case 10:
			return precpred(_ctx, 3);
		case 11:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean shift_expression_sempred(Shift_expressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 12:
			return precpred(_ctx, 3);
		case 13:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean add_expression_sempred(Add_expressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 14:
			return precpred(_ctx, 3);
		case 15:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean multi_expression_sempred(Multi_expressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 16:
			return precpred(_ctx, 4);
		case 17:
			return precpred(_ctx, 3);
		case 18:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean postfix_expression_sempred(Postfix_expressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 19:
			return precpred(_ctx, 5);
		case 20:
			return precpred(_ctx, 4);
		case 21:
			return precpred(_ctx, 3);
		case 22:
			return precpred(_ctx, 2);
		case 23:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\39\u01ba\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\3\2\7\2H\n\2\f\2\16\2K\13\2\3\2\3\2\3\3\3\3\3\3\5\3"+
		"R\n\3\3\4\3\4\3\4\3\4\7\4X\n\4\f\4\16\4[\13\4\3\4\3\4\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\7\5e\n\5\f\5\16\5h\13\5\5\5j\n\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\7\5u\n\5\f\5\16\5x\13\5\5\5z\n\5\3\5\3\5\5\5~\n\5\3\6\3\6\3\6"+
		"\3\6\3\6\5\6\u0085\n\6\3\6\3\6\3\6\5\6\u008a\n\6\3\6\7\6\u008d\n\6\f\6"+
		"\16\6\u0090\13\6\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u009b\n\b\3\t"+
		"\3\t\7\t\u009f\n\t\f\t\16\t\u00a2\13\t\3\t\3\t\3\n\3\n\3\n\3\13\3\13\3"+
		"\13\3\13\5\13\u00ad\n\13\3\f\5\f\u00b0\n\f\3\f\3\f\3\r\3\r\3\16\3\16\3"+
		"\16\3\16\3\16\5\16\u00bb\n\16\3\17\3\17\3\17\3\17\3\17\3\17\7\17\u00c3"+
		"\n\17\f\17\16\17\u00c6\13\17\3\20\3\20\3\20\3\20\3\20\3\20\7\20\u00ce"+
		"\n\20\f\20\16\20\u00d1\13\20\3\21\3\21\3\21\3\21\3\21\3\21\7\21\u00d9"+
		"\n\21\f\21\16\21\u00dc\13\21\3\22\3\22\3\22\3\22\3\22\3\22\7\22\u00e4"+
		"\n\22\f\22\16\22\u00e7\13\22\3\23\3\23\3\23\3\23\3\23\3\23\7\23\u00ef"+
		"\n\23\f\23\16\23\u00f2\13\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3"+
		"\24\7\24\u00fd\n\24\f\24\16\24\u0100\13\24\3\25\3\25\3\25\3\25\3\25\3"+
		"\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\7\25\u0111\n\25\f\25"+
		"\16\25\u0114\13\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\7\26\u011f"+
		"\n\26\f\26\16\26\u0122\13\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3"+
		"\27\7\27\u012d\n\27\f\27\16\27\u0130\13\27\3\30\3\30\3\30\3\30\3\30\3"+
		"\30\3\30\3\30\3\30\3\30\3\30\3\30\7\30\u013e\n\30\f\30\16\30\u0141\13"+
		"\30\3\31\3\31\3\31\5\31\u0146\n\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32"+
		"\3\32\3\32\3\32\3\32\3\32\3\32\5\32\u0155\n\32\3\33\3\33\3\33\3\33\3\33"+
		"\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33"+
		"\3\33\7\33\u016b\n\33\f\33\16\33\u016e\13\33\5\33\u0170\n\33\3\33\7\33"+
		"\u0173\n\33\f\33\16\33\u0176\13\33\3\34\3\34\3\34\3\34\3\34\3\34\5\34"+
		"\u017e\n\34\3\35\3\35\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36"+
		"\3\36\3\36\3\36\3\36\5\36\u0190\n\36\3\37\3\37\3\37\5\37\u0195\n\37\3"+
		"\37\3\37\5\37\u0199\n\37\3\37\3\37\5\37\u019d\n\37\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\5\37\u01a7\n\37\3 \3 \3!\3!\3\"\3\"\3#\3#\5#\u01b1"+
		"\n#\3#\3#\3#\3#\3#\5#\u01b8\n#\3#\2\16\n\34\36 \"$&(*,.\64$\2\4\6\b\n"+
		"\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BD\2\3\5\2\7\7"+
		"\t\n\64\65\u01d8\2I\3\2\2\2\4Q\3\2\2\2\6S\3\2\2\2\b}\3\2\2\2\n\u0084\3"+
		"\2\2\2\f\u0091\3\2\2\2\16\u009a\3\2\2\2\20\u009c\3\2\2\2\22\u00a5\3\2"+
		"\2\2\24\u00a8\3\2\2\2\26\u00af\3\2\2\2\30\u00b3\3\2\2\2\32\u00ba\3\2\2"+
		"\2\34\u00bc\3\2\2\2\36\u00c7\3\2\2\2 \u00d2\3\2\2\2\"\u00dd\3\2\2\2$\u00e8"+
		"\3\2\2\2&\u00f3\3\2\2\2(\u0101\3\2\2\2*\u0115\3\2\2\2,\u0123\3\2\2\2."+
		"\u0131\3\2\2\2\60\u0145\3\2\2\2\62\u0154\3\2\2\2\64\u0156\3\2\2\2\66\u017d"+
		"\3\2\2\28\u017f\3\2\2\2:\u018f\3\2\2\2<\u01a6\3\2\2\2>\u01a8\3\2\2\2@"+
		"\u01aa\3\2\2\2B\u01ac\3\2\2\2D\u01b7\3\2\2\2FH\5\4\3\2GF\3\2\2\2HK\3\2"+
		"\2\2IG\3\2\2\2IJ\3\2\2\2JL\3\2\2\2KI\3\2\2\2LM\7\2\2\3M\3\3\2\2\2NR\5"+
		"\6\4\2OR\5\b\5\2PR\5\f\7\2QN\3\2\2\2QO\3\2\2\2QP\3\2\2\2R\5\3\2\2\2ST"+
		"\7\f\2\2TU\7\63\2\2UY\7\30\2\2VX\5\22\n\2WV\3\2\2\2X[\3\2\2\2YW\3\2\2"+
		"\2YZ\3\2\2\2Z\\\3\2\2\2[Y\3\2\2\2\\]\7\31\2\2]\7\3\2\2\2^_\5\n\6\2_`\7"+
		"\63\2\2`i\7\24\2\2af\5\24\13\2bc\7\33\2\2ce\5\24\13\2db\3\2\2\2eh\3\2"+
		"\2\2fd\3\2\2\2fg\3\2\2\2gj\3\2\2\2hf\3\2\2\2ia\3\2\2\2ij\3\2\2\2jk\3\2"+
		"\2\2kl\7\25\2\2lm\5\20\t\2m~\3\2\2\2no\7\b\2\2op\7\63\2\2py\7\24\2\2q"+
		"v\5\24\13\2rs\7\33\2\2su\5\24\13\2tr\3\2\2\2ux\3\2\2\2vt\3\2\2\2vw\3\2"+
		"\2\2wz\3\2\2\2xv\3\2\2\2yq\3\2\2\2yz\3\2\2\2z{\3\2\2\2{|\7\25\2\2|~\5"+
		"\20\t\2}^\3\2\2\2}n\3\2\2\2~\t\3\2\2\2\177\u0080\b\6\1\2\u0080\u0085\7"+
		"\5\2\2\u0081\u0085\7\4\2\2\u0082\u0085\7\6\2\2\u0083\u0085\7\63\2\2\u0084"+
		"\177\3\2\2\2\u0084\u0081\3\2\2\2\u0084\u0082\3\2\2\2\u0084\u0083\3\2\2"+
		"\2\u0085\u008e\3\2\2\2\u0086\u0087\f\7\2\2\u0087\u0089\7\26\2\2\u0088"+
		"\u008a\5\30\r\2\u0089\u0088\3\2\2\2\u0089\u008a\3\2\2\2\u008a\u008b\3"+
		"\2\2\2\u008b\u008d\7\27\2\2\u008c\u0086\3\2\2\2\u008d\u0090\3\2\2\2\u008e"+
		"\u008c\3\2\2\2\u008e\u008f\3\2\2\2\u008f\13\3\2\2\2\u0090\u008e\3\2\2"+
		"\2\u0091\u0092\5\24\13\2\u0092\u0093\7\3\2\2\u0093\r\3\2\2\2\u0094\u009b"+
		"\5\20\t\2\u0095\u009b\5:\36\2\u0096\u009b\5\22\n\2\u0097\u009b\5\26\f"+
		"\2\u0098\u009b\5<\37\2\u0099\u009b\5D#\2\u009a\u0094\3\2\2\2\u009a\u0095"+
		"\3\2\2\2\u009a\u0096\3\2\2\2\u009a\u0097\3\2\2\2\u009a\u0098\3\2\2\2\u009a"+
		"\u0099\3\2\2\2\u009b\17\3\2\2\2\u009c\u00a0\7\30\2\2\u009d\u009f\5\16"+
		"\b\2\u009e\u009d\3\2\2\2\u009f\u00a2\3\2\2\2\u00a0\u009e\3\2\2\2\u00a0"+
		"\u00a1\3\2\2\2\u00a1\u00a3\3\2\2\2\u00a2\u00a0\3\2\2\2\u00a3\u00a4\7\31"+
		"\2\2\u00a4\21\3\2\2\2\u00a5\u00a6\5\24\13\2\u00a6\u00a7\7\3\2\2\u00a7"+
		"\23\3\2\2\2\u00a8\u00a9\5\n\6\2\u00a9\u00ac\7\63\2\2\u00aa\u00ab\7!\2"+
		"\2\u00ab\u00ad\5\30\r\2\u00ac\u00aa\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad"+
		"\25\3\2\2\2\u00ae\u00b0\5\30\r\2\u00af\u00ae\3\2\2\2\u00af\u00b0\3\2\2"+
		"\2\u00b0\u00b1\3\2\2\2\u00b1\u00b2\7\3\2\2\u00b2\27\3\2\2\2\u00b3\u00b4"+
		"\5\32\16\2\u00b4\31\3\2\2\2\u00b5\u00b6\5\62\32\2\u00b6\u00b7\7!\2\2\u00b7"+
		"\u00b8\5\32\16\2\u00b8\u00bb\3\2\2\2\u00b9\u00bb\5\34\17\2\u00ba\u00b5"+
		"\3\2\2\2\u00ba\u00b9\3\2\2\2\u00bb\33\3\2\2\2\u00bc\u00bd\b\17\1\2\u00bd"+
		"\u00be\5\36\20\2\u00be\u00c4\3\2\2\2\u00bf\u00c0\f\4\2\2\u00c0\u00c1\7"+
		")\2\2\u00c1\u00c3\5\36\20\2\u00c2\u00bf\3\2\2\2\u00c3\u00c6\3\2\2\2\u00c4"+
		"\u00c2\3\2\2\2\u00c4\u00c5\3\2\2\2\u00c5\35\3\2\2\2\u00c6\u00c4\3\2\2"+
		"\2\u00c7\u00c8\b\20\1\2\u00c8\u00c9\5 \21\2\u00c9\u00cf\3\2\2\2\u00ca"+
		"\u00cb\f\4\2\2\u00cb\u00cc\7(\2\2\u00cc\u00ce\5 \21\2\u00cd\u00ca\3\2"+
		"\2\2\u00ce\u00d1\3\2\2\2\u00cf\u00cd\3\2\2\2\u00cf\u00d0\3\2\2\2\u00d0"+
		"\37\3\2\2\2\u00d1\u00cf\3\2\2\2\u00d2\u00d3\b\21\1\2\u00d3\u00d4\5\"\22"+
		"\2\u00d4\u00da\3\2\2\2\u00d5\u00d6\f\4\2\2\u00d6\u00d7\7.\2\2\u00d7\u00d9"+
		"\5\"\22\2\u00d8\u00d5\3\2\2\2\u00d9\u00dc\3\2\2\2\u00da\u00d8\3\2\2\2"+
		"\u00da\u00db\3\2\2\2\u00db!\3\2\2\2\u00dc\u00da\3\2\2\2\u00dd\u00de\b"+
		"\22\1\2\u00de\u00df\5$\23\2\u00df\u00e5\3\2\2\2\u00e0\u00e1\f\4\2\2\u00e1"+
		"\u00e2\7/\2\2\u00e2\u00e4\5$\23\2\u00e3\u00e0\3\2\2\2\u00e4\u00e7\3\2"+
		"\2\2\u00e5\u00e3\3\2\2\2\u00e5\u00e6\3\2\2\2\u00e6#\3\2\2\2\u00e7\u00e5"+
		"\3\2\2\2\u00e8\u00e9\b\23\1\2\u00e9\u00ea\5&\24\2\u00ea\u00f0\3\2\2\2"+
		"\u00eb\u00ec\f\4\2\2\u00ec\u00ed\7\60\2\2\u00ed\u00ef\5&\24\2\u00ee\u00eb"+
		"\3\2\2\2\u00ef\u00f2\3\2\2\2\u00f0\u00ee\3\2\2\2\u00f0\u00f1\3\2\2\2\u00f1"+
		"%\3\2\2\2\u00f2\u00f0\3\2\2\2\u00f3\u00f4\b\24\1\2\u00f4\u00f5\5(\25\2"+
		"\u00f5\u00fe\3\2\2\2\u00f6\u00f7\f\5\2\2\u00f7\u00f8\7$\2\2\u00f8\u00fd"+
		"\5(\25\2\u00f9\u00fa\f\4\2\2\u00fa\u00fb\7%\2\2\u00fb\u00fd\5(\25\2\u00fc"+
		"\u00f6\3\2\2\2\u00fc\u00f9\3\2\2\2\u00fd\u0100\3\2\2\2\u00fe\u00fc\3\2"+
		"\2\2\u00fe\u00ff\3\2\2\2\u00ff\'\3\2\2\2\u0100\u00fe\3\2\2\2\u0101\u0102"+
		"\b\25\1\2\u0102\u0103\5*\26\2\u0103\u0112\3\2\2\2\u0104\u0105\f\7\2\2"+
		"\u0105\u0106\7#\2\2\u0106\u0111\5*\26\2\u0107\u0108\f\6\2\2\u0108\u0109"+
		"\7\"\2\2\u0109\u0111\5*\26\2\u010a\u010b\f\5\2\2\u010b\u010c\7\'\2\2\u010c"+
		"\u0111\5*\26\2\u010d\u010e\f\4\2\2\u010e\u010f\7&\2\2\u010f\u0111\5*\26"+
		"\2\u0110\u0104\3\2\2\2\u0110\u0107\3\2\2\2\u0110\u010a\3\2\2\2\u0110\u010d"+
		"\3\2\2\2\u0111\u0114\3\2\2\2\u0112\u0110\3\2\2\2\u0112\u0113\3\2\2\2\u0113"+
		")\3\2\2\2\u0114\u0112\3\2\2\2\u0115\u0116\b\26\1\2\u0116\u0117\5,\27\2"+
		"\u0117\u0120\3\2\2\2\u0118\u0119\f\5\2\2\u0119\u011a\7,\2\2\u011a\u011f"+
		"\5,\27\2\u011b\u011c\f\4\2\2\u011c\u011d\7+\2\2\u011d\u011f\5,\27\2\u011e"+
		"\u0118\3\2\2\2\u011e\u011b\3\2\2\2\u011f\u0122\3\2\2\2\u0120\u011e\3\2"+
		"\2\2\u0120\u0121\3\2\2\2\u0121+\3\2\2\2\u0122\u0120\3\2\2\2\u0123\u0124"+
		"\b\27\1\2\u0124\u0125\5.\30\2\u0125\u012e\3\2\2\2\u0126\u0127\f\5\2\2"+
		"\u0127\u0128\7\34\2\2\u0128\u012d\5.\30\2\u0129\u012a\f\4\2\2\u012a\u012b"+
		"\7\35\2\2\u012b\u012d\5.\30\2\u012c\u0126\3\2\2\2\u012c\u0129\3\2\2\2"+
		"\u012d\u0130\3\2\2\2\u012e\u012c\3\2\2\2\u012e\u012f\3\2\2\2\u012f-\3"+
		"\2\2\2\u0130\u012e\3\2\2\2\u0131\u0132\b\30\1\2\u0132\u0133\5\60\31\2"+
		"\u0133\u013f\3\2\2\2\u0134\u0135\f\6\2\2\u0135\u0136\7\36\2\2\u0136\u013e"+
		"\5\60\31\2\u0137\u0138\f\5\2\2\u0138\u0139\7\37\2\2\u0139\u013e\5\60\31"+
		"\2\u013a\u013b\f\4\2\2\u013b\u013c\7 \2\2\u013c\u013e\5\60\31\2\u013d"+
		"\u0134\3\2\2\2\u013d\u0137\3\2\2\2\u013d\u013a\3\2\2\2\u013e\u0141\3\2"+
		"\2\2\u013f\u013d\3\2\2\2\u013f\u0140\3\2\2\2\u0140/\3\2\2\2\u0141\u013f"+
		"\3\2\2\2\u0142\u0143\7\13\2\2\u0143\u0146\5\n\6\2\u0144\u0146\5\62\32"+
		"\2\u0145\u0142\3\2\2\2\u0145\u0144\3\2\2\2\u0146\61\3\2\2\2\u0147\u0155"+
		"\5\64\33\2\u0148\u0149\7\61\2\2\u0149\u0155\5\62\32\2\u014a\u014b\7\62"+
		"\2\2\u014b\u0155\5\62\32\2\u014c\u014d\7\34\2\2\u014d\u0155\5\62\32\2"+
		"\u014e\u014f\7\35\2\2\u014f\u0155\5\62\32\2\u0150\u0151\7-\2\2\u0151\u0155"+
		"\5\62\32\2\u0152\u0153\7*\2\2\u0153\u0155\5\62\32\2\u0154\u0147\3\2\2"+
		"\2\u0154\u0148\3\2\2\2\u0154\u014a\3\2\2\2\u0154\u014c\3\2\2\2\u0154\u014e"+
		"\3\2\2\2\u0154\u0150\3\2\2\2\u0154\u0152\3\2\2\2\u0155\63\3\2\2\2\u0156"+
		"\u0157\b\33\1\2\u0157\u0158\5\66\34\2\u0158\u0174\3\2\2\2\u0159\u015a"+
		"\f\7\2\2\u015a\u0173\7\61\2\2\u015b\u015c\f\6\2\2\u015c\u0173\7\62\2\2"+
		"\u015d\u015e\f\5\2\2\u015e\u015f\7\26\2\2\u015f\u0160\5\30\r\2\u0160\u0161"+
		"\7\27\2\2\u0161\u0173\3\2\2\2\u0162\u0163\f\4\2\2\u0163\u0164\7\32\2\2"+
		"\u0164\u0173\7\63\2\2\u0165\u0166\f\3\2\2\u0166\u016f\7\24\2\2\u0167\u016c"+
		"\5\32\16\2\u0168\u0169\7\33\2\2\u0169\u016b\5\32\16\2\u016a\u0168\3\2"+
		"\2\2\u016b\u016e\3\2\2\2\u016c\u016a\3\2\2\2\u016c\u016d\3\2\2\2\u016d"+
		"\u0170\3\2\2\2\u016e\u016c\3\2\2\2\u016f\u0167\3\2\2\2\u016f\u0170\3\2"+
		"\2\2\u0170\u0171\3\2\2\2\u0171\u0173\7\25\2\2\u0172\u0159\3\2\2\2\u0172"+
		"\u015b\3\2\2\2\u0172\u015d\3\2\2\2\u0172\u0162\3\2\2\2\u0172\u0165\3\2"+
		"\2\2\u0173\u0176\3\2\2\2\u0174\u0172\3\2\2\2\u0174\u0175\3\2\2\2\u0175"+
		"\65\3\2\2\2\u0176\u0174\3\2\2\2\u0177\u017e\7\63\2\2\u0178\u017e\58\35"+
		"\2\u0179\u017a\7\24\2\2\u017a\u017b\5\30\r\2\u017b\u017c\7\25\2\2\u017c"+
		"\u017e\3\2\2\2\u017d\u0177\3\2\2\2\u017d\u0178\3\2\2\2\u017d\u0179\3\2"+
		"\2\2\u017e\67\3\2\2\2\u017f\u0180\t\2\2\2\u01809\3\2\2\2\u0181\u0182\7"+
		"\r\2\2\u0182\u0183\7\24\2\2\u0183\u0184\5\30\r\2\u0184\u0185\7\25\2\2"+
		"\u0185\u0186\5\16\b\2\u0186\u0190\3\2\2\2\u0187\u0188\7\r\2\2\u0188\u0189"+
		"\7\24\2\2\u0189\u018a\5\30\r\2\u018a\u018b\7\25\2\2\u018b\u018c\5\16\b"+
		"\2\u018c\u018d\7\16\2\2\u018d\u018e\5\16\b\2\u018e\u0190\3\2\2\2\u018f"+
		"\u0181\3\2\2\2\u018f\u0187\3\2\2\2\u0190;\3\2\2\2\u0191\u0192\7\17\2\2"+
		"\u0192\u0194\7\24\2\2\u0193\u0195\5> \2\u0194\u0193\3\2\2\2\u0194\u0195"+
		"\3\2\2\2\u0195\u0196\3\2\2\2\u0196\u0198\7\3\2\2\u0197\u0199\5@!\2\u0198"+
		"\u0197\3\2\2\2\u0198\u0199\3\2\2\2\u0199\u019a\3\2\2\2\u019a\u019c\7\3"+
		"\2\2\u019b\u019d\5B\"\2\u019c\u019b\3\2\2\2\u019c\u019d\3\2\2\2\u019d"+
		"\u019e\3\2\2\2\u019e\u019f\7\25\2\2\u019f\u01a7\5\16\b\2\u01a0\u01a1\7"+
		"\20\2\2\u01a1\u01a2\7\24\2\2\u01a2\u01a3\5\30\r\2\u01a3\u01a4\7\25\2\2"+
		"\u01a4\u01a5\5\16\b\2\u01a5\u01a7\3\2\2\2\u01a6\u0191\3\2\2\2\u01a6\u01a0"+
		"\3\2\2\2\u01a7=\3\2\2\2\u01a8\u01a9\5\30\r\2\u01a9?\3\2\2\2\u01aa\u01ab"+
		"\5\30\r\2\u01abA\3\2\2\2\u01ac\u01ad\5\30\r\2\u01adC\3\2\2\2\u01ae\u01b0"+
		"\7\21\2\2\u01af\u01b1\5\30\r\2\u01b0\u01af\3\2\2\2\u01b0\u01b1\3\2\2\2"+
		"\u01b1\u01b2\3\2\2\2\u01b2\u01b8\7\3\2\2\u01b3\u01b4\7\22\2\2\u01b4\u01b8"+
		"\7\3\2\2\u01b5\u01b6\7\23\2\2\u01b6\u01b8\7\3\2\2\u01b7\u01ae\3\2\2\2"+
		"\u01b7\u01b3\3\2\2\2\u01b7\u01b5\3\2\2\2\u01b8E\3\2\2\2/IQYfivy}\u0084"+
		"\u0089\u008e\u009a\u00a0\u00ac\u00af\u00ba\u00c4\u00cf\u00da\u00e5\u00f0"+
		"\u00fc\u00fe\u0110\u0112\u011e\u0120\u012c\u012e\u013d\u013f\u0145\u0154"+
		"\u016c\u016f\u0172\u0174\u017d\u018f\u0194\u0198\u019c\u01a6\u01b0\u01b7";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}