package AbstractSyntaxTree;

import AST.*;
import Parser.MMRBaseListener;
import Parser.MMRParser;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

/**
 * Created by zhouyuhao on 2016/4/3.
 */
public class AbstractSyntaxTree extends MMRBaseListener{
    public MMRParser parser;
    public static ParseTreeProperty<AST> tree = new ParseTreeProperty<AST>();

    public AbstractSyntaxTree(MMRParser parser) {
        this.parser = parser;
    }

    @Override public void exitProgram(MMRParser.ProgramContext ctx) {
        super.exitProgram(ctx);
        Program node = new Program();
        tree.put(ctx, node);
        if (ctx.declaration() != null) {
            for (MMRParser.DeclarationContext decl : ctx.declaration()) {
                Declaration cnt = (Declaration) tree.get(decl);
                node.decl.add(cnt);
            }
        }
        tree.get(ctx).row = ctx.getStart().getLine();
    }

    @Override public void exitDeclaration(MMRParser.DeclarationContext ctx) {
        super.exitDeclaration(ctx);
        if (ctx.class_declaration() != null) {
            tree.put(ctx, tree.get(ctx.class_declaration()));
        } else if (ctx.function_declaration() != null) {
            tree.put(ctx, tree.get(ctx.function_declaration()));
        } else if (ctx.global_var_declaration() != null){
            tree.put(ctx, tree.get(ctx.global_var_declaration()));
        }
    }

    @Override public void exitClass_declaration(MMRParser.Class_declarationContext ctx) {
        super.exitClass_declaration(ctx);
        ClassDecl node = new ClassDecl();
        tree.put(ctx, node);
        node.id = Symbol.get(ctx.IDENTIFIER().getText());
        if (ctx.var_declaration_statement() != null) {
            for (MMRParser.Var_declaration_statementContext stmt : ctx.var_declaration_statement()) {
                VarDeclStmt cnt = (VarDeclStmt) tree.get(stmt);
                node.decls.add(cnt.vardecl);
            }
        }
        tree.get(ctx).row = ctx.getStart().getLine();
    }

    @Override public void exitFunction_declaration(MMRParser.Function_declarationContext ctx) {
        super.exitFunction_declaration(ctx);
        if (ctx.type_name() != null) {
            FunctionDecl node = new FunctionDecl();
            tree.put(ctx, node);
            node.type = (Type) tree.get(ctx.type_name());
            node.id = Symbol.get(ctx.IDENTIFIER().getText());
            if (ctx.var_declaration() != null) {
                for (MMRParser.Var_declarationContext decl : ctx.var_declaration()) {
                    VarDecl cnt = (VarDecl) tree.get(decl);
                    node.parameter.add(cnt);
                }
            }
            node.stmts = (CompoundStmt) tree.get(ctx.compound_statement());
        } else {
            FunctionDecl node = new FunctionDecl();
            tree.put(ctx, node);
            node.type = new VoidType();
            node.id = Symbol.get(ctx.IDENTIFIER().getText());
            if (ctx.var_declaration() != null) {
                for (MMRParser.Var_declarationContext decl : ctx.var_declaration()) {
                    VarDecl cnt = (VarDecl) tree.get(decl);
                    node.parameter.add(cnt);
                }
            }
            node.stmts = (CompoundStmt) tree.get(ctx.compound_statement());
        }
        tree.get(ctx).row = ctx.getStart().getLine();
    }

    @Override public void exitType_name(MMRParser.Type_nameContext ctx) {
        super.exitType_name(ctx);
        if (ctx.INT() != null) {
            IntType node = new IntType();
            tree.put(ctx, node);
        } else if (ctx.STRING() != null) {
            StringType node = new StringType();
            tree.put(ctx, node);
        } else if (ctx.BOOL() != null) {
            BoolType node = new BoolType();
            tree.put(ctx, node);
        } else if (ctx.IDENTIFIER() != null) {
            ClassType node = new ClassType();
            tree.put(ctx, node);
            node.id = Symbol.get(ctx.IDENTIFIER().getText());
        } else {
            ArrayType node = new ArrayType();
            tree.put(ctx, node);
            node.basetype = (Type) tree.get(ctx.type_name());
            if (ctx.expression() != null) {
                node.arraysize = (Expression) tree.get(ctx.expression());
            }
        }
        tree.get(ctx).row = ctx.getStart().getLine();
    }

    @Override public void exitGlobal_var_declaration(MMRParser.Global_var_declarationContext ctx) {
        super.exitGlobal_var_declaration(ctx);
        tree.put(ctx, tree.get(ctx.var_declaration()));
    }

    @Override public void exitStatement(MMRParser.StatementContext ctx) {
        super.exitStatement(ctx);
        if (ctx.compound_statement() != null) {
            tree.put(ctx, tree.get(ctx.compound_statement()));
        } else
        if (ctx.if_statement() != null) {
            tree.put(ctx, tree.get(ctx.if_statement()));
        } else
        if (ctx.var_declaration_statement() != null) {
            tree.put(ctx, tree.get(ctx.var_declaration_statement()));
        } else
        if (ctx.expr_statement() != null) {
            tree.put(ctx, tree.get(ctx.expr_statement()));
        } else
        if (ctx.loop_statement() != null) {
            tree.put(ctx, tree.get(ctx.loop_statement()));
        } else
        if (ctx.jump_statement() != null) {
            tree.put(ctx, tree.get(ctx.jump_statement()));
        }
    }

    @Override public void exitCompound_statement(MMRParser.Compound_statementContext ctx) {
        super.exitCompound_statement(ctx);
        CompoundStmt node = new CompoundStmt();
        tree.put(ctx, node);
        if (ctx.statement() != null) {
            for (MMRParser.StatementContext stmt : ctx.statement()) {
                Statement cnt = (Statement) tree.get(stmt);
                node.stmt.add(cnt);
            }
        }
        tree.get(ctx).row = ctx.getStart().getLine();
    }

    @Override public void exitVar_declaration_statement(MMRParser.Var_declaration_statementContext ctx) {
        super.exitVar_declaration_statement(ctx);
        VarDeclStmt node = new VarDeclStmt();
        node.vardecl = (VarDecl) tree.get(ctx.var_declaration());
        tree.put(ctx, node);
        tree.get(ctx).row = ctx.getStart().getLine();
    }

    @Override public void exitVar_declaration(MMRParser.Var_declarationContext ctx) {
        super.exitVar_declaration(ctx);
        VarDecl node = new VarDecl();
        tree.put(ctx, node);
        node.type = (Type) tree.get(ctx.type_name());
        node.id = Symbol.get(ctx.IDENTIFIER().getText());
        //if (node.id == null) System.out.print("1\n");
        if (ctx.expression() != null) {
            node.expr = (Expression) tree.get(ctx.expression());
        }
        tree.get(ctx).row = ctx.getStart().getLine();
    }

    @Override public void exitExpr_statement(MMRParser.Expr_statementContext ctx) {
        super.exitExpr_statement(ctx);
        if (ctx.expression() != null) {
            tree.put(ctx, tree.get(ctx.expression()));
        }
    }

    @Override public void exitExpression(MMRParser.ExpressionContext ctx) {
        super.exitExpression(ctx);
        /*
        if (ctx.expression() != null) {
            BinaryExpr node = new BinaryExpr();
            tree.put(ctx, node);
            node.left = (Expression) tree.get(ctx.expression());
            node.op = BinaryOp.COMMA;
            node.right = (Expression) tree.get(ctx.assignment_expression());
        } else {
        */
        tree.put(ctx, tree.get(ctx.assignment_expression()));
    }

    @Override public void exitAssignment_expression(MMRParser.Assignment_expressionContext ctx) {
        super.exitAssignment_expression(ctx);
        if (ctx.assignment_expression() != null) {
            BinaryExpr node = new BinaryExpr();
            tree.put(ctx, node);
            node.left = (Expression) tree.get(ctx.unary_expression());
            node.op = BinaryOp.ASSIGN;
            node.right = (Expression) tree.get(ctx.assignment_expression());
            tree.get(ctx).row = ctx.getStart().getLine();
        } else {
            tree.put(ctx, tree.get(ctx.logical_or_expression()));
        }
    }

    @Override public void exitLogical_or_expression(MMRParser.Logical_or_expressionContext ctx) {
        super.exitLogical_or_expression(ctx);
        if (ctx.logical_or_expression() != null) {
            BinaryExpr node = new BinaryExpr();
            tree.put(ctx, node);
            node.left = (Expression) tree.get(ctx.logical_or_expression());
            node.op = BinaryOp.OROR;
            node.right = (Expression) tree.get(ctx.logical_and_expression());
            tree.get(ctx).row = ctx.getStart().getLine();
        } else {
            tree.put(ctx, tree.get(ctx.logical_and_expression()));
        }
    }

    @Override public void exitLogical_and_expression(MMRParser.Logical_and_expressionContext ctx) {
        super.exitLogical_and_expression(ctx);
        if (ctx.logical_and_expression() != null) {
            BinaryExpr node = new BinaryExpr();
            tree.put(ctx, node);
            node.left = (Expression) tree.get(ctx.logical_and_expression());
            node.op = BinaryOp.ANDAND;
            node.right = (Expression) tree.get(ctx.or_expression());
            tree.get(ctx).row = ctx.getStart().getLine();
        } else {
            tree.put(ctx, tree.get(ctx.or_expression()));
        }
    }

    @Override public void exitOr_expression(MMRParser.Or_expressionContext ctx) {
        super.exitOr_expression(ctx);
        if (ctx.or_expression() != null) {
            BinaryExpr node = new BinaryExpr();
            tree.put(ctx, node);
            node.left = (Expression) tree.get(ctx.or_expression());
            node.op = BinaryOp.OR;
            node.right = (Expression) tree.get(ctx.xor_expression());
            tree.get(ctx).row = ctx.getStart().getLine();
        } else {
            tree.put(ctx, tree.get(ctx.xor_expression()));
        }
    }

    @Override public void exitXor_expression(MMRParser.Xor_expressionContext ctx) {
        super.exitXor_expression(ctx);
        if (ctx.xor_expression() != null) {
            BinaryExpr node = new BinaryExpr();
            tree.put(ctx, node);
            node.left = (Expression) tree.get(ctx.xor_expression());
            node.op = BinaryOp.XOR;
            node.right = (Expression) tree.get(ctx.and_expression());
            tree.get(ctx).row = ctx.getStart().getLine();
        } else {
            tree.put(ctx, tree.get(ctx.and_expression()));
        }
    }

    @Override public void exitAnd_expression(MMRParser.And_expressionContext ctx) {
        super.exitAnd_expression(ctx);
        if (ctx.and_expression() != null) {
            BinaryExpr node = new BinaryExpr();
            tree.put(ctx, node);
            node.left = (Expression) tree.get(ctx.and_expression());
            node.op = BinaryOp.AND;
            node.right = (Expression) tree.get(ctx.equality_expression());
            tree.get(ctx).row = ctx.getStart().getLine();
        } else {
            tree.put(ctx, tree.get(ctx.equality_expression()));
        }
    }

    @Override public void exitEquality_expression(MMRParser.Equality_expressionContext ctx) {
        super.exitEquality_expression(ctx);
        if (ctx.equality_expression() != null) {
            BinaryExpr node = new BinaryExpr();
            tree.put(ctx, node);
            node.left = (Expression) tree.get(ctx.equality_expression());
            node.right = (Expression) tree.get(ctx.relation_expression());
            if (ctx.EQUAL() != null) {
                node.op = BinaryOp.EQUAL;
            } else {
                node.op = BinaryOp.NOTEQUAL;
            }
            tree.get(ctx).row = ctx.getStart().getLine();
        } else {
            tree.put(ctx, tree.get(ctx.relation_expression()));
        }
    }

    @Override public void exitRelation_expression(MMRParser.Relation_expressionContext ctx) {
        super.exitRelation_expression(ctx);
        if (ctx.relation_expression() != null) {
            BinaryExpr node = new BinaryExpr();
            tree.put(ctx, node);
            node.left = (Expression) tree.get(ctx.relation_expression());
            node.right = (Expression) tree.get(ctx.shift_expression());
            if (ctx.GREATER() != null) {
                node.op = BinaryOp.GREATER;
            }
            if (ctx.LESS() != null) {
                node.op = BinaryOp.LESS;
            }
            if (ctx.GREATEREQUAL() != null) {
                node.op = BinaryOp.GREATEREQUAL;
            }
            if (ctx.LESSEQUAL() != null) {
                node.op = BinaryOp.LESSEQUAL;
            }
            tree.get(ctx).row = ctx.getStart().getLine();
        } else {
            tree.put(ctx, tree.get(ctx.shift_expression()));
        }
    }

    @Override public void exitShift_expression(MMRParser.Shift_expressionContext ctx) {
        super.exitShift_expression(ctx);
        if (ctx.shift_expression() != null) {
            BinaryExpr node = new BinaryExpr();
            tree.put(ctx, node);
            node.left = (Expression) tree.get(ctx.shift_expression());
            node.right = (Expression) tree.get(ctx.add_expression());
            if (ctx.RIGHTSHIFT() != null) {
                node.op = BinaryOp.RIGHTSHIFT;
            } else {
                node.op = BinaryOp.LEFTSHIFT;
            }
            tree.get(ctx).row = ctx.getStart().getLine();
        } else {
            tree.put(ctx, tree.get(ctx.add_expression()));
        }
    }

    @Override public void exitAdd_expression(MMRParser.Add_expressionContext ctx) {
        super.exitAdd_expression(ctx);
        if (ctx.add_expression() != null) {
            BinaryExpr node = new BinaryExpr();
            tree.put(ctx, node);
            node.left = (Expression) tree.get(ctx.add_expression());
            node.right = (Expression) tree.get(ctx.multi_expression());
            if (ctx.PLUS() != null) {
                node.op = BinaryOp.ADD;
            } else {
                node.op = BinaryOp.SUB;
            }
            tree.get(ctx).row = ctx.getStart().getLine();
        } else {
            tree.put(ctx, tree.get(ctx.multi_expression()));
        }
    }

    @Override public void exitMulti_expression(MMRParser.Multi_expressionContext ctx) {
        super.exitMulti_expression(ctx);
        if (ctx.multi_expression() != null) {
            BinaryExpr node = new BinaryExpr();
            tree.put(ctx, node);
            node.left = (Expression) tree.get(ctx.multi_expression());
            node.right = (Expression) tree.get(ctx.creation_expression());
            if (ctx.TIMES() != null) {
                node.op = BinaryOp.TIMES;
            } else if (ctx.DIVISION() != null) {
                node.op = BinaryOp.DIVISION;
            } else {
                node.op = BinaryOp.MOD;
            }
            tree.get(ctx).row = ctx.getStart().getLine();
        } else {
            tree.put(ctx, tree.get(ctx.creation_expression()));
        }
    }

    @Override public void exitCreation_expression(MMRParser.Creation_expressionContext ctx) {
        super.exitCreation_expression(ctx);
        if (ctx.type_name() != null) {
            CreationExpr node = new CreationExpr();
            tree.put(ctx, node);
            node.type = (Type) tree.get(ctx.type_name());
            tree.get(ctx).row = ctx.getStart().getLine();
        } else {
            tree.put(ctx, tree.get(ctx.unary_expression()));
        }
    }

    @Override public void exitUnary_expression(MMRParser.Unary_expressionContext ctx) {
        super.exitUnary_expression(ctx);
        if (ctx.unary_expression() != null) {
            UnaryExpr node = new UnaryExpr();
            tree.put(ctx, node);
            node.unary = (Expression) tree.get(ctx.unary_expression());
            if (ctx.INCREASE() != null) {
                node.op = UnaryOp.INCREASE;
            } else if (ctx.DECREASE() != null) {
                node.op = UnaryOp.DECREASE;
            } else if (ctx.PLUS() != null) {
                node.op = UnaryOp.POS;
            } else if (ctx.MINUS() != null) {
                node.op = UnaryOp.NEG;
            } else if (ctx.TILDE() != null) {
                node.op = UnaryOp.TILDE;
            } else if (ctx.NOT() != null) {
                node.op = UnaryOp.NOT;
            }
            tree.get(ctx).row = ctx.getStart().getLine();
        } else {
            tree.put(ctx, tree.get(ctx.postfix_expression()));
        }
    }

   // @Override public void exitParameter_list(MMRParser.Parameter_listContext ctx) { }

    @Override public void exitPostfix_expression(MMRParser.Postfix_expressionContext ctx) {
        super.exitPostfix_expression(ctx);
        if (ctx.postfix_expression() != null) {
            if (ctx.INCREASE() != null) {
                Increment node = new Increment();
                tree.put(ctx, node);
                node.expr = (Expression) tree.get(ctx.postfix_expression());
            } else if (ctx.DECREASE() != null) {
                Decrement node = new Decrement();
                tree.put(ctx, node);
                node.expr = (Expression) tree.get(ctx.postfix_expression());
            } else if (ctx.LEFTBRACKET() != null) {
                ArrayAccess node = new ArrayAccess();
                tree.put(ctx, node);
                node.expr = (Expression) tree.get(ctx.postfix_expression());
                node.position = (Expression) tree.get(ctx.expression());
            } else if (ctx.DOT() != null) {
                ClassMem node = new ClassMem();
                tree.put(ctx, node);
                node.classname = (Expression) tree.get(ctx.postfix_expression());
                node.id = Symbol.get(ctx.IDENTIFIER().getText());
            } else if (ctx.LEFTPA() != null) {
                if (tree.get(ctx.postfix_expression()) instanceof Symbol) {
                    //FunctionCall
                    FunctionCall node = new FunctionCall();
                    tree.put(ctx, node);
                    node.id = (Symbol) tree.get(ctx.postfix_expression());
                    if (ctx.assignment_expression() != null) {
                        for (MMRParser.Assignment_expressionContext expr : ctx.assignment_expression()) {
                            Expression cnt = (Expression) tree.get(expr);
                            node.parameter.add(cnt);
                        }
                    }
                } else {
                    //EmbeddedCall
                    EmbeddedCall node = new EmbeddedCall();
                    tree.put(ctx, node);
                    node.expr = ((ClassMem) tree.get(ctx.postfix_expression())).classname;
                    node.id = ((ClassMem) tree.get(ctx.postfix_expression())).id;
                    if (ctx.assignment_expression() != null) {
                        for (MMRParser.Assignment_expressionContext expr : ctx.assignment_expression()) {
                            Expression cnt = (Expression) tree.get(expr);
                            node.parameter.add(cnt);
                        }
                    }
                }
            }
            tree.get(ctx).row = ctx.getStart().getLine();
        } else {
            tree.put(ctx, tree.get(ctx.primary_expression()));
        }
    }

    @Override public void exitPrimary_expression(MMRParser.Primary_expressionContext ctx) {
        super.exitPrimary_expression(ctx);
        if (ctx.IDENTIFIER() != null) {
            Symbol node = Symbol.get(ctx.IDENTIFIER().getText());
            //System.out.print("2\n");
            tree.put(ctx, node);
            tree.get(ctx).row = ctx.getStart().getLine();
        } else if (ctx.constant() != null) {
            tree.put(ctx, tree.get(ctx.constant()));
        } else {
            tree.put(ctx, tree.get(ctx.expression()));
        }
    }

    @Override public void exitConstant(MMRParser.ConstantContext ctx) {
        super.exitConstant(ctx);
        if (ctx.NULL() != null) {
            Null node = new Null();
            tree.put(ctx, node);
        } else if (ctx.TRUE() != null) {
            BoolLiteral node = new BoolLiteral();
            node.value = Boolean.TRUE;
            tree.put(ctx, node);
        } else if (ctx.FALSE() != null) {
            BoolLiteral node = new BoolLiteral();
            node.value = Boolean.FALSE;
            tree.put(ctx, node);
        } else if (ctx.INT_LITERAL() != null) {
            IntLiteral node = new IntLiteral();
            node.value = Integer.valueOf(ctx.INT_LITERAL().getText());
            tree.put(ctx, node);
        } else if (ctx.STRING_LITERAL() != null) {
            StringLiteral node = new StringLiteral();
            node.value = ctx.STRING_LITERAL().getText();
            tree.put(ctx, node);
        }
        tree.get(ctx).row = ctx.getStart().getLine();
    }

    @Override public void exitIf_statement(MMRParser.If_statementContext ctx) {
        super.exitIf_statement(ctx);
        if (ctx.ELSE() != null) {
            IfStmt node = new IfStmt();
            tree.put(ctx, node);
            node.condition = (Expression) tree.get(ctx.expression());
            node.branch_true = (Statement) tree.get(ctx.statement(0));
            node.branch_false = (Statement) tree.get(ctx.statement(1));
        } else {
            IfStmt node = new IfStmt();
            tree.put(ctx, node);
            node.condition = (Expression) tree.get(ctx.expression());
            node.branch_true = (Statement) tree.get(ctx.statement(0));
        }
        tree.get(ctx).row = ctx.getStart().getLine();
    }

    @Override public void exitLoop_statement(MMRParser.Loop_statementContext ctx) {
        super.exitLoop_statement(ctx);
        if (ctx.WHILE() != null) {
            WhileStmt node = new WhileStmt();
            tree.put(ctx, node);
            node.condition = (Expression) tree.get(ctx.expression());
            node.stmt = (Statement) tree.get(ctx.statement());
        } else if (ctx.FOR() != null) {
            ForStmt node = new ForStmt();
            tree.put(ctx, node);
            if (ctx.init() != null) {
                node.init = (Expression) tree.get(ctx.init());
            }
            if (ctx.boundary() != null) {
                node.boundary = (Expression) tree.get(ctx.boundary());
            }
            if (ctx.step() != null) {
                node.step = (Expression) tree.get(ctx.step());
            }
            node.operation = (Statement) tree.get(ctx.statement());
        }
        tree.get(ctx).row = ctx.getStart().getLine();
    }

    @Override public void exitInit(MMRParser.InitContext ctx) {
        super.exitInit(ctx);
        tree.put(ctx, tree.get(ctx.expression()));
    }

    @Override public void exitBoundary(MMRParser.BoundaryContext ctx) {
        super.exitBoundary(ctx);
        tree.put(ctx, tree.get(ctx.expression()));
    }

    @Override public void exitStep(MMRParser.StepContext ctx) {
        super.exitStep(ctx);
        tree.put(ctx, tree.get(ctx.expression()));
    }

    @Override public void exitJump_statement(MMRParser.Jump_statementContext ctx) {
        super.exitJump_statement(ctx);
        if (ctx.RETURN() != null) {
            ReturnStmt node = new ReturnStmt();
            if (ctx.expression() != null) {
                node.expr = (Expression) tree.get(ctx.expression());
                tree.put(ctx, node);
            }
        } else if (ctx.BREAK() != null) {
            BreakStmt node = new BreakStmt();
            tree.put(ctx, node);
        } else if (ctx.CONTINUE() != null) {
            ContinueStmt node = new ContinueStmt();
            tree.put(ctx, node);
        }
        //tree.get(ctx).row = ctx.getStart().getLine();
    }

    //@Override public void visitTerminal(TerminalNode node) { }
}
