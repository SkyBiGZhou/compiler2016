package AbstractSyntaxTree;

import AST.Program;
import AST.Table;
import IR.IR;
import Parser.MMRLexer;
import Parser.MMRParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.InputStream;
import java.util.Stack;

/**
 * Created by zhouyuhao on 2016/4/5.
 */
public class ExtractInterfaceTool {

    public static void main(String[] args) throws Exception {
        //String inputFile = null;
        //if (args.length > 0) inputFile = args[0];
        try {

            //System.out.println(filename);

            //String s = "/Users/bluesnap/Downloads/Compiler/testfile/test.mx";
            InputStream is = System.in;//new FileInputStream(s);

            ANTLRInputStream input = new ANTLRInputStream(is);

            MMRLexer lexer = new MMRLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            MMRParser parser = new MMRParser(tokens);
            ParseTree tree = parser.program();

            ParseTreeWalker walker = new ParseTreeWalker();
            AbstractSyntaxTree ASTBuilder = new AbstractSyntaxTree(parser);
            walker.walk(ASTBuilder, tree);

            //AbstractSyntaxTree.tree.get(tree).print(-1);
            boolean ret = true;
            Table table = new Table();
            ret = ret & AbstractSyntaxTree.tree.get(tree).firstTraverse(table, 0);
            ret = ret & AbstractSyntaxTree.tree.get(tree).secondTraverse(table, 0);
            ret = ret & AbstractSyntaxTree.tree.get(tree).thirdTraverse(table, 0, new Stack<>(), null);
            //System.out.println(ret);
            //System.out.print("Accepted\n");
            IR ir = ((Program) AbstractSyntaxTree.tree.get(tree)).IRtranslate(null, table, 0, new Stack<>());
            //AbstractSyntaxTree.tree.get(tree).print(-1);
            //System.out.println(ir.toString());
            System.out.print(ir.MIPSForCISC());
        /*
        File file = new File();
        if (!file.exists()) {
            file.createNewFile();
        }
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(content);
        bw.close();
        */
/*
        if (shouldPass != ret) {
            fail("Failed!");
        }
        */
            if (ret) System.exit(0);
            else System.exit(1);
        } catch (Exception e) {
            System.exit(1);
        }
    }
}
